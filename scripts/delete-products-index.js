const {Client} = require("@elastic/elasticsearch");
const path = require("path");

require('dotenv').config({ path: path.resolve(process.cwd(), '../.env') })

const esClient = new Client({
    node: process.env.ELASTIC_SEARCH_HOST,
    auth: {
        username: process.env.ELASTIC_SEARCH_USER,
        password: process.env.ELASTIC_SEARCH_PASSWORD
    }
})

esClient.indices.delete({
    index: 'movies'
}).then(console.log).catch(console.error)
