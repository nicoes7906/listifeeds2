const {Client} = require("@elastic/elasticsearch");
const path = require("path");

require('dotenv').config({ path: path.resolve(process.cwd(), '../.env') })

const esClient = new Client({
  node: process.env.ELASTIC_SEARCH_HOST,
  auth: {
    username: process.env.ELASTIC_SEARCH_USER,
    password: process.env.ELASTIC_SEARCH_PASSWORD
  }
})

const isoDate = new Date(2021,8,18).toISOString();

const query = {
  "bool": {
    "filter": [{ "range": { "modifiedDate": { "lt": isoDate }}}]
  }
}

const MAX_TO_BE_DELETED = 500;

checkDeleteTotal()
  .then(deleteProducts)
  .then(console.log)
  .catch(console.error)

function checkDeleteTotal() {
  return new Promise((resolve, reject) => {
    let totalToBeDeleted = -1;
    esClient.search({
      index: process.env.ELASTIC_SEARCH_INDEX,
      body: {
        query
      }
    }).then((result) => {
      totalToBeDeleted = result.body.hits.total

      if(totalToBeDeleted.value > 0 && totalToBeDeleted.value < MAX_TO_BE_DELETED) {
        resolve(true)
      } else if(totalToBeDeleted > MAX_TO_BE_DELETED) {
        throw new Error('Too many products seem to be deleted')
      }
      reject('Nothing will be deleted')
    }).catch(console.error)
  })
}

function deleteProducts() {
  return new Promise((resolve, reject) => {
    esClient.deleteByQuery({
      index: process.env.ELASTIC_SEARCH_INDEX,
      body: {
        query
      }
    }).then((result) => {
      resolve(result)
    }).catch(reject)
  })
}
