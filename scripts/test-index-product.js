const {Client} = require("@elastic/elasticsearch");
const path = require('path');

require('dotenv').config({ path: path.resolve(process.cwd(), '../.env') })

const esClient = new Client({
    node: process.env.ELASTIC_SEARCH_HOST,
    auth: {
        username: process.env.ELASTIC_SEARCH_USER,
        password: process.env.ELASTIC_SEARCH_PASSWORD
    }
});

const body = [
    {
        "index": {
            "_index": "products2",
            "_id": "820650808784"
        }
    },
    {
        "objectID": "820650808784",
        "ean": "820650808784",
        "title": "Pokémon Sword & Shield Evolving Skies Sleeved Booster - Pokémon Kaarten",
        "shortDescription": "Een epische storm wacht op jou met Pokémon TCG Sword & Shield Evolving Skies! De machtige Dragon-...",
        "longDescription": "Een epische storm wacht op jou met Pokémon TCG Sword & Shield Evolving Skies! De machtige Dragon-type Pokémon zijn terug: deze set bevat onder andere Rayquaza VMAX en Duraludon VMAX (in Gigantamax vorm), maar ook Dragonite V, Noivern V, en nog veel meer. De wolken openen zich en daar onthullen Eevee's evoluties zich in een volle regenboog van Pokémon V en Pokémon VMAX waarmee een nieuwe heldere dag aanbreekt in Pokémon Sword & Shield Evolving Skies.<br /><br />De Pokémon Sword & Shield Evolving Skies Sleeved Booster bevat 10 willekeurige Pokémon kaarten.<br /><br /><strong>Let op: </strong>Er kan geen keuze worden gemaakt tussen de vier boosters. De inhoud van de boosters is gelijk, het is enkel de verpakking die verschilt. Je ontvangt een van de afgebeelde boosters.",
        "ranking": 1,
        "facetPrice": 5.99,
        "categories": [
            "SPELLEN",
            "POPULAR"
        ],
        "vendors": [
            {
                "price": 5.99,
                "name": "Bol.com",
                "mobileUrl": "http://partner.bol.com/click/click?p=1&t=url&s=47511&url=undefined&f=TXL",
                "desktopUrl": "http://partner.bol.com/click/click?p=1&t=url&s=47511&url=undefined&f=TXL"
            }
        ],
        "media": [
            "https://media.s-bol.com/qAQJLWx92m8D/r0Gm48K/465x840.jpg",
            "https://media.s-bol.com/m7qBN9xrmN7A/r0Gm48K/465x840.jpg",
            "https://media.s-bol.com/m7qBNWYE2WZO/r0Gm48K/465x840.jpg",
            "https://media.s-bol.com/m7qBNWYrEjR0/r0Gm48K/465x840.jpg"
        ],
        "price": {
            "actual": "5.99",
            "euros": "5",
            "cents": "99"
        },
        "fromAge": -1,
        "toAge": -1,
        "modifiedDate": "2021-09-08T06:16:34.000Z",
        "createdDate": "2021-09-02T13:30:44.000Z"
    }
]

esClient.bulk({body}).then(console.log).catch(console.error)
