ROOT_DIR=~/WebstormProjects/listifeeds2

SRC_DIR="$ROOT_DIR/src/feed-downloader"
DIST_SRC_DIR="$ROOT_DIR/deploy/functions/feed-downloader"

rm -rf ${DIST_SRC_DIR:?}/*

${SRC_DIR}/node_modules/typescript/bin/tsc -p ${SRC_DIR} --outDir ${DIST_SRC_DIR}
rsync ${SRC_DIR}/package.json ${DIST_SRC_DIR}/package.json
rsync -r ${SRC_DIR}/node_modules/ ${DIST_SRC_DIR}/node_modules
rsync ${SRC_DIR}/package-lock.json ${DIST_SRC_DIR}/package-lock.json

echo "Finished building lambda src"
