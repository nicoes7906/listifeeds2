const {Client} = require("@elastic/elasticsearch");
const path = require("path");

require('dotenv').config({ path: path.resolve(process.cwd(), '../.env') })

const esClient = new Client({
    node: process.env.ELASTIC_SEARCH_HOST,
    auth: {
        username: process.env.ELASTIC_SEARCH_USER,
        password: process.env.ELASTIC_SEARCH_PASSWORD
    }
})

esClient.indices.create({
    index: 'products',
    body: {
        mappings: {
            properties: {
              objectID: { type: 'keyword' },
              ean: { type: 'keyword' },
              shortDescription: { type: 'text' },
              longDescription: { type: 'text' },
              ranking: { type: 'integer' },
              facetPrice: { type: 'float' },
              categories: {
                  type: "keyword"
                },
              vendors: {
                  type: "object"
              },
              fromAge: { type: 'integer' },
              toAge: { type: 'integer' },
              modifiedDate: { type: 'date' },
              createdDate: { type: 'date' },
            }
        }
    }
}, { ignore: [400] }).then(console.log).catch(console.error)
