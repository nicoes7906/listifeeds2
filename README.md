# ListiFeeds 2

## Goal
Process affiliate feeds to a single data model that can be used on Listi.nl for creating toy wishlists

## Technical Approach
Created a single repo with processors for different feeds. Some have XML feeds, some have JSON feeds, some have CSV feeds.
There are different executors that can be targeted through an env variable.
This is run on AWS Lambda in combination with Step Functions for orchestrating the whole process.
The idea is that all executors are run in parallel with Step Functions and when all the executors have written their data to the MySQL database,
It will extract these products and create a product items with data from different vendors and put that into the search service (Algolia).

![Alt text](./images/step-functions-screenshot.png "Step Functions overview")

## Update 2020
### Improvements
Most of this code was written in 2018, some of it rewritten in 2019.
Nowadays I would do some things differently. Also working in teams brings extra requirements to the code.

- The MySQL database is still hosted outside of AWS. The URL is publically available. Not as secure as can be.
- The infrastructure was created by hand when I was well on my AWS journey. Nowadays I would put everything in CloudFormation.
- I've grouped some feed processing in a single step. These could all be parallelised.
- In a few places there were no return types defined. When working in teams I would recommend to be more strict in this area.
