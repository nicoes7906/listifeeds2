/*
  Install PM2 to run this: npm install pm2 -g
*/

const http = require('http'),
  exec = require('child_process').exec,
  port = 3000;

const statusKeeper = {};

function initializeScriptExecutionWithId(id) {
  statusKeeper[id] = 'PENDING';
  exec('sh download-upload-script.sh',
    function(error, stdout, stderr){
      if(error) {
        console.log(stderr);
        statusKeeper[id] = 'ERROR';
        setTimeout(() => delete statusKeeper[id], 60000);
      } else {
        console.log(stdout);
        statusKeeper[id] = 'SUCCESS';
        setTimeout(() => delete statusKeeper[id], 60000);
      }
    })
}

const server = http.createServer(function(req, res) {
  const matchExecute = req.url.match(/^\/execute\?id=([0-9a-z]*)$/);
  const matchStatus = req.url.match(/^\/status\?id=([0-9a-z]*)$/);
  if (matchExecute) {
    console.log(req.url, matchExecute[1]);
    initializeScriptExecutionWithId(matchExecute[1])
    res.writeHead(202, {'Content-Type': 'text/plain'});
    res.write('Operation pending');
  } else if (matchStatus) {
    if(statusKeeper[matchStatus[1]] === 'SUCCESS') {
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.write('Done');
    } else if (statusKeeper[matchStatus[1]] === 'ERROR') {
      res.writeHead(500, {'Content-Type': 'text/plain'});
      res.write('Error');
    } else if(statusKeeper[matchStatus[1]] === 'PENDING') {
      res.writeHead(202, {'Content-Type': 'text/plain'});
      res.write('Operation pending');
    } else {
      res.writeHead(404, {'Content-Type': 'text/plain'});
      res.write('Not found');
    }
  } else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.write('Not found');
  }
  res.end();
}).listen(port);

server.timeout = 20000;
