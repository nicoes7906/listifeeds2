# Download csv feeds
curl ftp://bol_pricecompare:d%21ChjlcZFt@ftp.bol.com/product-feed_toys-v2.csv.gz -o ~/downloads/bol-toys-feed.csv.gz
echo "Downloaded feeds"

# Only keep products offered by Bol.com
gunzip -f ~/downloads/bol-toys-feed.csv.gz > ~/downloads/bol-toys-feed.csv
echo "Gunzipped feeds"
egrep '\|"B"\|' ~/downloads/bol-toys-feed.csv > ~/downloads/bol-toys-feed-filtered.csv
## Get columns from original file and append filtered
echo -e "$(head -1 ~/downloads/bol-toys-feed.csv)\n$(cat ~/downloads/bol-toys-feed-filtered.csv)" > ~/downloads/bol-toys-feed-filtered-with-head.csv
echo "Filtered feeds"
gzip -f -c ~/downloads/bol-toys-feed-filtered-with-head.csv > ~/downloads/bol-toys-feed.csv.gzip
echo "Gzipped feeds"

# Upload result to S3
aws s3 cp ~/downloads/bol-toys-feed.csv.gzip s3://listifeeds-bucket
echo "Uploaded filtered feeds"
