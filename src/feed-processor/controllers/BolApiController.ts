import {Promise} from 'core-js';
import {Vendor} from "../enums/vendor";
import {BolJsonFetcherStream} from "../classes/Bol/BolJsonFetcherStream";
import {BaseDocumentController} from "../classes/BaseDocumentController";

export class BolApiController extends BaseDocumentController {
  run(opts) {
    const {target} = opts;
    return new Promise((resolve, reject) => {
      try {
        const bolJsonFetcher = BolJsonFetcherStream.createJsonFetcher(
          target,
          Vendor.BOLOPENAPI,
          'Bol api',
          this.sqlClient,
          this.s3Client,
          this.logger
        );

        Promise.resolve()
          .then(() => bolJsonFetcher.run())
          .then(() => resolve('Ok'))
          .catch(e => reject('Error: ' + e));
      } catch (e) {
        this.logger.error(e);
        reject('Error');
      }

    })
  }
}
