import {BaseDocumentController} from "../classes/BaseDocumentController";
import {IcecatPcfFetcherStream} from "../classes/Icecat/IcecatPcfFetcherStream";
import {S3FileNames} from "../constants/S3";
import {Promise} from "core-js";

export class IceCatController extends BaseDocumentController {
  run(): Promise<String> {
    const {logger} = this;
    return new Promise<String>((resolve, reject) => {
      const fetcher = IcecatPcfFetcherStream.createCsvFetcher('icecat', this.sqlClient, this.s3Client, this.logger, S3FileNames.ICECAT)

      try {
        Promise.resolve()
          .then(() => fetcher.run())
          .then(() => resolve('Ok'))
          .catch(e => reject('Error: ' + e));
      } catch (e) {
        onError(e);
      }

      function onError(e) {
        logger.error(e);
        reject('Error');
      }
    })
  }
}
