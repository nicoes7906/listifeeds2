import {BaseDocumentController} from "../classes/BaseDocumentController";
import {Promise} from "core-js";
import {CustomProductFetcherStream} from "../classes/CustomProduct/CustomProductFetcherStream";
import {S3FileNames} from "../constants/S3";

export class CustomProductController extends BaseDocumentController {
  run(): Promise<String> {
    const {logger} = this;
    return new Promise<String>((resolve, reject) => {
      const fetcher = CustomProductFetcherStream.createCsvFetcher('custom product', this.sqlClient, this.s3Client, this.logger, S3FileNames.CUSTOM)

      try {
        Promise.resolve()
          .then(() => fetcher.run())
          .then(() => resolve('Ok'))
          .catch(e => reject('Error: ' + e));
      } catch (e) {
        onError(e);
      }

      function onError(e) {
        logger.error(e);
        reject('Error');
      }
    })
  }
}
