import {Promise} from 'core-js';
import {BaseDocumentController} from "../classes/BaseDocumentController";
import {BolFeedsTargetFile} from "../enums/BolFeedsTargetFile";
import {BolFeedCsvFetcherStream} from "../classes/Bol/BolCsvFetcherStream";

require('dotenv').load();

export class BolFeedsController extends BaseDocumentController {
  run(opts) {
    const {logger} = this;
    const {target} = opts;
    return new Promise((resolve, reject) => {

      let fetcher;

      if(target === BolFeedsTargetFile.TOYS) {
        fetcher = BolFeedCsvFetcherStream.createCsvFetcher('Bol toys', this.sqlClient, this.s3Client, this.logger, target);
      } else {
        fetcher = BolFeedCsvFetcherStream.createCsvFetcher('Bol computer & games', this.sqlClient, this.s3Client, this.logger, target);
      }

      try {
        Promise.resolve()
          .then(() => fetcher.run())
          .then(() => resolve('Ok'))
          .catch(e => reject('Error: ' + e));
      } catch (e) {
        onError(e);
      }

      function onError(e) {
        logger.error(e);
        reject('Error');
      }
    });
  }
}
