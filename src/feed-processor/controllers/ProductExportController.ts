import {BaseDocumentController} from "../classes/BaseDocumentController";
import * as s3StreamFactory from 's3-upload-stream';
import * as Stream from "stream";
import * as pump from 'pump';

export class ProductExportController extends BaseDocumentController {
  run(): Promise<String> {
    return new Promise((resolve, reject) => {
      this.logger.info('Start product csv export...');

      return this.uploadAsCsv()
        .then(() => resolve('OK'))
        .catch(reject)
    });
  }

  uploadAsCsv() {
    const s3Stream = s3StreamFactory(this.s3Client);
    return new Promise((resolve, reject) => {
      const productStream = this.sqlClient.query('SELECT * FROM AggregatedProducts ORDER BY ranking ASC').stream();
      const transformToCsv = createSqlToCsvStream();
      const upload = s3Stream.upload({
        "Bucket": "listifeeds-bucket",
        "Key": `productfeed/user/icecat/11b446b9-e949-4490-bc44-0ac4efda0b92/listi-products.csv`
      });

      // Handle errors.
      upload.on('error', function (error) {
        console.log(error);
        reject(error)
      });

      // Handle progress.
      upload.on('part', function (details) {
        console.log(details);
      });

      // Handle upload completion.
      upload.on('uploaded', function (details) {
        console.log(details);
        resolve("Success");
      });

      pump(
        productStream,
        transformToCsv,
        upload,
        (err) => {
          if (err) {
            if (err.toString().indexOf('premature close') > -1) {
              this.logger.info('Pipeline ended prematurely');
              resolve('Closed');
            } else {
              this.logger.error(`Pipeline product export failed.`);
              reject(err);
            }
          } else {
            this.logger.info(`Pipeline product export succeeded.`);
            resolve('Ok');
          }
        }
      )
    })
  }
}

function createSqlToCsvStream() {
  let count = 0;
  return new Stream.Transform({
      objectMode: true,
      transform(chunk, encoding, callback) {
        if(count === 0) {
          this.push('ean\n')
        }
        this.push(chunk.ean + '\n')
        count++;
        callback();
      }
    }
  )
}
