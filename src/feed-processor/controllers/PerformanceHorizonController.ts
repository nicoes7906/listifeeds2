import {Promise} from 'core-js';
import {Vendor} from "../enums/vendor";
import {PerformanceHorizonCsvFetcherStream} from "../classes/PerformanceHorizon/PerformanceHorizonCsvFetcherStream";
import {Runnable} from "../interfaces/runnable";
import {PerformanceHorizonXmlFetcherStream} from "../classes/PerformanceHorizon/PerformanceHorizonXmlFetcherStream";
import {BaseDocumentController} from "../classes/BaseDocumentController";
import {PerformanceHorizonTargetFile} from "../enums/PerformanceHorizonTargetFile";

export class PerformanceHorizonController extends BaseDocumentController {

  run(opts): Promise<String> {
    const {target} = opts;

    let fetcher: Runnable;
    if(target === PerformanceHorizonTargetFile.WEHKAMP_TOYS) {
      fetcher = PerformanceHorizonCsvFetcherStream.createCsvFetcher(
        Vendor.WEHKAMP,
        'Wehkamp',
        this.sqlClient,
        this.s3Client,
        this.logger,
        target
      );
    } else if ( target === PerformanceHorizonTargetFile.COOOLBLUE_COMPUTER_GAMES) {
      fetcher = PerformanceHorizonXmlFetcherStream.createPhXmlFetcher(
        Vendor.COOLBLUE,
        'Coolblue',
        this.sqlClient,
        this.s3Client,
        this.logger,
        target
      );
    }

    return Promise.resolve('Start')
      .then(() => fetcher.run())
      .then(() => this.logger.info(PerformanceHorizonController._formatLogSucces(target)))
      .then(() => 'Ok')
      .catch(e => {
        this.logger.error(e);
        return 'Error';
      })
  }

  static _formatLogSucces(tag) {
    return `Saved products from ${tag}`;
  }
}
