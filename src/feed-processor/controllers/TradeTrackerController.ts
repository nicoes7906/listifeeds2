import {TradeTrackerConstants} from '../constants/tradetracker';
import {Vendor} from '../enums/vendor';
import {Promise} from 'core-js';
import {TradeTrackerFetcherStream} from "../classes/TradeTracker/TradeTrackerFetcherStream";
import {BaseDocumentController} from "../classes/BaseDocumentController";

export class TradeTrackerController extends BaseDocumentController {

  public run(): Promise<String> {
    return new Promise((resolve, reject) => {
      const ittFetcher = TradeTrackerFetcherStream.createTradeTrackerFetcher(
        Vendor.INTERTOYS,
        TradeTrackerConstants.INTER_FEED,
        'Intertoys',
        this.sqlClient,
        this.logger
      );
      Promise.resolve()
        .then(() => ittFetcher.run())
        .then(() => resolve('Ok'))
        .catch(e => {
          console.trace(e);
          reject(e)
        });
    });
  }
}
