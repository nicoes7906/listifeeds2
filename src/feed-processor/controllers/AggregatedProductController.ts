import {Promise} from 'core-js';
import {ProductBatchWriterMysql} from "../classes/Product/ProductBatchWriterMysql";
import {Timer} from "../classes/Timer";
import * as pump from "pump";
import {AggregatedProductTransformer} from "../classes/AggregatedProduct/AggregatedProductTransformer";
import {BaseDocumentController} from "../classes/BaseDocumentController";

export class AggregatedProductController extends BaseDocumentController {

  run(): Promise<void> {
    return new Promise((resolve, reject) => {
      const { logger, sqlClient } = this;
      const productsTable = process.env.MYSQL_PRODUCTS_TABLE;

      if(!productsTable) throw new Error('No products table from env')

      const logTag = 'aggregated product';
      const timer = new Timer(logTag, logger.info.bind(logger));

      timer.start();

      const productStream = sqlClient.query(`SELECT * FROM ${productsTable} WHERE ean > 0 AND modified_date >= CURDATE() - INTERVAL 30 DAY ORDER BY ean`).stream();
      const aggregatedProductTransformer = AggregatedProductTransformer.createTransformStream();
      const aggregatedProductWriteStream = ProductBatchWriterMysql.createAggregatedProductWriteStream(sqlClient, logTag, logger, timer);

      pump(
        productStream,
        aggregatedProductTransformer,
        aggregatedProductWriteStream,
        (err) => {
          if (err) {
            if (err.toString().indexOf('premature close') > -1) {
              logger.info('Pipeline ended prematurely');
              resolve();
            } else {
              logger.error(`Pipeline ${logTag} failed.`);
              reject(err);
            }
          } else {
            logger.info(`Pipeline ${logTag} succeeded.`);
            resolve();
          }
        }
      );
    })

  }
}
