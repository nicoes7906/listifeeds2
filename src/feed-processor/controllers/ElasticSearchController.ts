import {BaseDocumentController} from "../classes/BaseDocumentController";
import {ElasticSearchUploader} from "../classes/ElasticSearch/ElasticSearchUploader";
import {Client} from "@elastic/elasticsearch";

const MAX_UPLOADS = 25000;
require('dotenv').load();

const {ELASTIC_SEARCH_HOST, ELASTIC_SEARCH_USER, ELASTIC_SEARCH_PASSWORD, ELASTIC_SEARCH_INDEX} = process.env;

export class ElasticSearchController extends BaseDocumentController {
  run(): Promise<String> {
    return new Promise((resolve, reject) => {
      this.logger.info('Start ES upload...');

      return this.getNumberOfProductsEligibleForUpload()
        .then((numberOfResults: number) => this.checkIfUploadAllowed(numberOfResults, reject))
        .then(() => this.uploadToES())
        .then(() => resolve('OK'))
        .catch(reject)
    });
  }

  checkIfUploadAllowed(numberOfResults, reject) {
    this.logger.info(`${numberOfResults} products are eligible for uploading...`);
    if (numberOfResults > MAX_UPLOADS) {
      this.logger.error(`Too many products eligible for upload. Exiting...`);
      reject(`Too many products eligible for uploading: ${numberOfResults}`);
    }
  }

  uploadToES() {
    const esClient = new Client({
      node: ELASTIC_SEARCH_HOST,
      auth: {
        username: ELASTIC_SEARCH_USER,
        password: ELASTIC_SEARCH_PASSWORD
      }
    })

    return ElasticSearchUploader.run(
      this.sqlClient.query('SELECT * FROM AggregatedProducts ORDER BY ranking ASC').stream(),
      500,
      esClient,
      ELASTIC_SEARCH_INDEX,
      this.logger.info,
      this.logger.error
    )
  }

  getNumberOfProductsEligibleForUpload(): Promise<number> {
    return new Promise((resolve, reject) => {
      const query = `SELECT COUNT(*) FROM AggregatedProducts`;
      this.sqlClient.query(query, (err, result) => {
        if(err) reject(err);
        try {
          const count = +result[0]['COUNT(*)'];
          resolve(count);
        } catch (e) {
          reject(e);
        }
      })
    })
  }
}
