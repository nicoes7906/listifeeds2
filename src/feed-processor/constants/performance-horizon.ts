import {CategoryAliasMap} from "../types/category-alias-map";
import {Category} from "../enums/category";

const COOLBLUE_ELEKTRONICA_ALIASES = [
  'Drones',
  'Tekentablets',
]

const COOLBLUE_GAMES_CONSOLE_ALIASES = ['Gaming muizen', 'Controllers', 'racepedalen', "Gaming headsets", 'Games', 'Game captures', 'VR brillen', 'Gaming stoelen', 'Flightsticks', 'flightstick pedalen'];

export namespace PerformanceHorizon {
  export const HOST = 'feeds.performancehorizon.com';
  export const COOLBLUE_CATEGORY_ALIAS_MAP: CategoryAliasMap = [
    {category: Category.GAMES_CONSOLES, aliases: COOLBLUE_GAMES_CONSOLE_ALIASES},
    {category: Category.ELEKTRONICA, aliases: COOLBLUE_ELEKTRONICA_ALIASES},
  ]
}
