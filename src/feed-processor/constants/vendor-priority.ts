import {Vendor} from "../enums/vendor";

export const VENDOR_PRIORITY = [
  Vendor.LISTI,
  Vendor.BOLOPENAPI,
  Vendor.BOLFEED,
  Vendor.COOLBLUE,
  Vendor.WEHKAMP,
  Vendor.ICECAT,
  Vendor.INTERTOYS,
  Vendor.BARTSMIT,
  Vendor.MEDIAMARKT,
  Vendor.FONQ,
  Vendor.COOLSHOP,
  Vendor.GROTE_SPEELGOED_WINKEL,
  Vendor.FF_SHOPPEN,
  Vendor.SPEELGOED_DIRECT,
  Vendor.FUN_EN_FEEST,
  Vendor.BELLATIO
];
