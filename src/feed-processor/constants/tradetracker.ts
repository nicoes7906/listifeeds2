import {CategoryAliasMap} from "../types/category-alias-map";
import {Category} from "../enums/category";

export namespace TradeTrackerConstants {
  export const HOST: string = 'pf.tradetracker.net';
  // Intertoys
  export const INTER_FEED: string = '/?aid=223954&encoding=utf-8&type=xml-v2&fid=797117&categoryType=2&additionalType=2';
  // Bart smit
  export const BARTS_FEED: string = '/?aid=223954&encoding=utf-8&type=xml-v2&fid=797120&categoryType=2&additionalType=2';
  // Grote speelgoed winkel
  export const GSW_FEED: string = '/?aid=223954&encoding=utf-8&type=xml-v2&fid=252262&categoryType=2&additionalType=2';
  // Ff shoppen
  export const FFSHO_FEED: string = '/?aid=223954&encoding=utf-8&type=xml-v2&fid=333386&categoryType=2&additionalType=2';
  // Speelgoed post order
  export const SPOST_FEED: string = '/?aid=223954&encoding=utf-8&type=xml-v2&fid=254035&categoryType=2&additionalType=2';
  // Coolshop
  export const COOLS_FEED: string = '/?aid=223954&encoding=utf-8&type=xml-v2&fid=985283&categoryType=2&additionalType=2';
  // Fonq
  export const FONQ_FEED: string = '/?aid=287017&encoding=utf-8&type=xml-v2&fid=584064&categoryType=2&additionalType=2';
  // Fellatio
  export const FELLA_FEED: string = '/?aid=287017&encoding=utf-8&type=xml-v2&fid=254047&categoryType=2&additionalType=2';
  // Fun en feest
  export const FUNEN_FEED: string = '/?aid=287017&encoding=utf-8&type=xml-v2&fid=1046154&categoryType=2&additionalType=2';
  // Speelgoed direct
  export const SDIRE_FEED: string = '/?aid=287017&encoding=utf-8&type=xml-v2&fid=1098280&categoryType=2&additionalType=2';
  // Knuffelparadijs
  export const KNUFP_FEED: string = '/?aid=287017&encoding=utf-8&type=xml-v2&fid=254011&categoryType=2&additionalType=2';
  // Bellatio
  export const BELLA_FEED: string = '/?aid=287017&encoding=utf-8&type=xml-v2&fid=254047&categoryType=2&additionalType=2';

  export const CATEGORY_ALIAS_MAP: CategoryAliasMap = [
    {category: Category.BOUWEN_CONSTRUCTIE, aliases: ['Bouwen & Constructie']},
    {category: Category.POPPEN_KNUFFELS, aliases: ['Poppen & Knuffels']},
    {category: Category.BUITENSPEELGOED, aliases: ['Buiten- speelgoed']},
    {category: Category.ELEKTRONICA, aliases: ['Elektronica']},
    {category: Category.SPELLEN, aliases: ['Spellen']},
    {category: Category.ROLLENSPEL_VERKLEDEN, aliases: ['Rollenspel & Verkleden']},
    {category: Category.SPORT_SCHOOL, aliases: ['School & Vrije tijd']},
    {category: Category.SPEELFIGUREN, aliases: ['Speelfiguren & Speelsets']},
    {category: Category.PUZZELS, aliases: ['Puzzels']},
    {category: Category.VOERTUIGEN, aliases: ['Speelgoed- voertuigen']},
    {category: Category.EDUCATIEF_SPEELGOED, aliases: ['Boeken']},
    {category: Category.GAMES_CONSOLES, aliases: ['Games']},
    {category: Category.BABY_PEUTER, aliases: ['Baby & Peuter']}
  ]
}
