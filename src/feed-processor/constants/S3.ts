export enum S3FileNames {
  BOL_TOYS_API = 'bol-api-toys.json',
  BOL_GAMES_API = 'bol-api-games.json',
  BOL_TOYS_FEED = 'bol-toys-feed.csv',
  BOL_COMPUTER_GAMES_FEED = 'bol-computer-games-feed.csv',
  COOLBLUE = 'coolblue.xml',
  WEHKAMP = 'wehkamp.csv',
  ICECAT = 'icecat-pcf.csv',
  CUSTOM = 'listi-custom.csv',
}

export const bucketName = 'listifeeds-bucket';
