import {Category} from "../enums/category";
import {CategoryAliasMap} from "../types/category-alias-map";;

export namespace BolConstants {
  export const PATH_COMPUTER_GAMES_XML = '/tmp/bol-computer-games-feed.xml';
  export const SITE_ID = 47511; //To be found at partnerprogramma.bol.com
  export const API_KEY = '18834F82BD67430BBB9B8FF4CCF369C3';
  export const CATEGORY_ALIAS_MAP: CategoryAliasMap = [
    {category: Category.BUITENSPEELGOED, aliases: ['Buitenspeelgoed', 'Fietsen en Fietsaccessoires', 'Speeltoestellen en Zwembaden', 'Zwembaden', 'Speeltoestellen', 'Speltafels', 'Vliegers', 'Outdoor Toys', 'Waterspeelgoed']},
    {category: Category.BOUWEN_CONSTRUCTIE, aliases: ['Bouwen & Constructie', 'Modelbouw']},
    {category: Category.SPELLEN, aliases: ['Spellen', 'Bordspellen', 'Kaartspellen']},
    {category: Category.PUZZELS, aliases: ['Puzzels']},
    {category: Category.BABY_PEUTER, aliases: ['Baby & Peuter', 'Babyspeelgoed', 'Blokken en Vormen']},
    {category: Category.SPEELFIGUREN, aliases: ['Speelfiguren', 'Speelfiguren & Sets', 'Houten speelgoed']},
    {category: Category.POPPEN_KNUFFELS, aliases: ['Poppen', 'Knuffels & Knuffeldoekjes', 'Speelgoedkleden', 'Poppenkasten']},
    {category: Category.KNUTSELEN, aliases: ['Knutselen', 'Knutselen voor kinderen', 'Boetseren en Vormgeven']},
    {category: Category.EDUCATIEF_SPEELGOED, aliases: ['Educatief speelgoed', 'Leren en Experimenteren', 'Koken en Bakken Speelgoed']},
    {category: Category.ROLLENSPEL_VERKLEDEN, aliases: ['Rollenspellen', 'Verkleden & Rollenspel', 'Verkleden en Rollenspel', 'Verkleedkleding', 'Feestartikelen']},
    {category: Category.VOERTUIGEN, aliases: ['Voertuigen', 'Speelgoedvoertuigen']},
    {category: Category.ZINGEN_MUZIEK, aliases: ['Zingen & Muziek', 'Zingen en Muziek Maken']},
    {category: Category.HOBBY_CREATIEF, aliases: ['Hobby & Creatief']},
    {category: Category.SPORT_SCHOOL, aliases: ['Sport & Vrije tijd']},
    {category: Category.GAMES_CONSOLES, aliases: ['Games', 'Games Software', 'Games Hardware - Accessoires']},
    {category: Category.ELEKTRONICA, aliases: ['Elektronisch Speelgoed']},
  ]
}
