import {Vendor} from "../enums/vendor";
import {VENDOR_PRIORITY} from "./vendor-priority";
import 'mocha';
import {expect} from 'chai';

describe('Vendor priority', () => {

  it('should contain all vendors', () => {

    Object.keys(Vendor).forEach(vendorKey => {
      expect(VENDOR_PRIORITY).to.include(Vendor[vendorKey]);
    })

  })
});
