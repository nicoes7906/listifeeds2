import {Category} from "../enums/category";

export const BoostableCategories = [Category.POPULAR, Category.SPELLEN, Category.BOUWEN_CONSTRUCTIE]
