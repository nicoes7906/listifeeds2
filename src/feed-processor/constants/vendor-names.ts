import {Vendor} from "../enums/vendor";

export const VENDOR_DISPLAY_NAMES = {
  [Vendor.BOLOPENAPI]: 'Bol.com',
  [Vendor.BOLFEED]: 'Bol.com',
  [Vendor.BARTSMIT]: 'Bartsmit',
  [Vendor.INTERTOYS]: 'Intertoys',
  [Vendor.COOLBLUE]: 'Coolblue',
  [Vendor.MEDIAMARKT]: 'Mediamarkt',
  [Vendor.COOLSHOP]: 'Coolshop',
  [Vendor.GROTE_SPEELGOED_WINKEL]: 'Gr. Speelg. winkel',
  [Vendor.SPEELGOED_DIRECT]: 'Speelg. direct',
  [Vendor.FUN_EN_FEEST]: 'Fun en feest',
  [Vendor.FONQ]: 'Fonq',
  [Vendor.FF_SHOPPEN]: 'FF Shoppen',
  [Vendor.WEHKAMP]: 'Wehkamp',
  [Vendor.BELLATIO]: 'Bellatio'
};
