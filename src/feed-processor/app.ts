import {BolFeedsController} from "./controllers/BolFeedsController";
import {ScriptExecutor} from "./classes/ScriptExecutor";
import {BolApiController} from "./controllers/BolApiController";
import {BolFeedsTargetFile} from "./enums/BolFeedsTargetFile";
import {PerformanceHorizonController} from "./controllers/PerformanceHorizonController";
import {PerformanceHorizonTargetFile} from "./enums/PerformanceHorizonTargetFile";
import {AggregatedProductController} from "./controllers/AggregatedProductController";
import {BolApiTargetFile} from "./enums/BolApiTargetFile";
import {ElasticSearchController} from "./controllers/ElasticSearchController";
import {IceCatController} from "./controllers/IcecatController";
import {ProductExportController} from "./controllers/ProductExportController";
import {CustomProductController} from "./controllers/CustomProductController";

exports.lambdaHandler = function runner(event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = true;
  if(!event.Executor) throw new Error('Executor not defined');
  console.log("Starting process for executor", event.Executor);
  switch (event.Executor) {
    case 'BOLTOYSFEED':
      createExecutor('boltoyfeeds', BolFeedsController, callback, {target: BolFeedsTargetFile.TOYS });
      break;
    case 'BOLTOYSAPI':
      createExecutor('bolapi', BolApiController, callback, {target: BolApiTargetFile.TOYS });
      break;
    case 'BOLGAMESAPI':
      createExecutor('bolapi', BolApiController, callback, {target: BolApiTargetFile.GAMES });
      break;
    case 'WEHKAMP':
      createExecutor('wehkamp', PerformanceHorizonController, callback, {target: PerformanceHorizonTargetFile.WEHKAMP_TOYS });
      break;
    case 'COOLBLUE':
      createExecutor('coolblue', PerformanceHorizonController, callback, {target: PerformanceHorizonTargetFile.COOOLBLUE_COMPUTER_GAMES });
      break;
    case 'AGGREGATOR':
      createExecutor('aggregator', AggregatedProductController, callback);
      break;
    case 'ICECAT':
      createExecutor('icecat', IceCatController, callback);
      break;
    case 'CUSTOM':
      createExecutor('custom', CustomProductController, callback);
      break;
    case 'ELASTICSEARCH':
      createExecutor('elasticsearch', ElasticSearchController, callback);
      break;
    case 'PRODUCTEXPORT':
      createExecutor('productexport', ProductExportController, callback);
      break;
  }
};

function createExecutor(name, controller, callback, runOpts?: any) {
  ScriptExecutor.executeRunner(name, (sqlClient, s3Client, logger) => new controller(logger, sqlClient, s3Client).run(runOpts))
    .then(() => {
      callback(null);
    })
    .catch(callback);
}
