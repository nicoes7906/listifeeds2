import {AggregatedProduct} from "../classes/AggregatedProduct/AggregatedProduct";

export interface AggregatedProductRecord extends AggregatedProduct {
  modified_date: Date
  created_date: Date
}
