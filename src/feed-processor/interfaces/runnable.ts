export interface Runnable {
    run: (...args: any[]) => Promise<any>;
}
