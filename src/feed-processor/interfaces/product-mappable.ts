import { Product } from '../classes/Product/Product';

export interface ProductMappable {
  mapProduct: (any, Function?, ...args: any[]) => Product | null;
}
