export interface ProductInfo {
  ean: number;
  images: Array<String>;
  description: String;
  price: number;
}