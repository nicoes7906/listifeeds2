import EventEmitter = NodeJS.EventEmitter;

export interface FeedStream extends EventEmitter {
  pause: () => void;
  resume: () => void;
  start: () => void;
  abort: () => void;
}
