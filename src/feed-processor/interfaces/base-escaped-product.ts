export interface BaseEscapedProduct {
  ean: String,
  vendor?: String
}
