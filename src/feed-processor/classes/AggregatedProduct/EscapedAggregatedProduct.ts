import { escape } from 'mysql2';
import { AggregatedProduct } from './AggregatedProduct';
import { BaseEscapedProduct } from '../../interfaces/base-escaped-product';

export class EscapedAggregatedProduct implements BaseEscapedProduct {
  public ean: String;
  public title: String;
  public images: String;
  public description: String;
  public price: String;
  public vendors: String;
  public number_of_prices: String;
  public ranking: String;
  public categories: String;
  public fromAge: String;
  public toAge: String;
  public video: String;
  public boostCategories: String;

  constructor(product: AggregatedProduct) {
    this.ean = escape(product.ean);
    this.title = escape(product.title);
    this.images = escape(product.images.join('#'));
    this.description = escape(product.description);
    this.price = escape(product.price);
    this.vendors = escape(JSON.stringify(product.vendors));
    this.number_of_prices = escape(product.numberOfPrices);
    this.ranking = escape(product.ranking);
    this.categories = escape(product.categories.join('#'));
    this.fromAge = escape(product.fromAge);
    this.toAge = escape(product.toAge);
    this.video = escape(product.video);
    this.boostCategories = escape(product.boostCategories);
  }
}
