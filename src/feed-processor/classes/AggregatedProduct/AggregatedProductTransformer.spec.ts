import 'mocha';
import { expect } from 'chai';

import {AggregatedProductTransformer} from "./AggregatedProductTransformer";
import {Vendor} from "../../enums/vendor";
import {isObject} from "lodash";
import * as Stream from "stream";

describe('AggregatedProductTransformer', () => {
  it('convert a single product to an aggregated one', () => {
    const mockProducts = [
      createMockProduct("123", {})
    ];

    const aggregatedProductWriter: Stream.Transform = AggregatedProductTransformer.createTransformStream();

    mockProducts.forEach(mockProduct => aggregatedProductWriter.write(mockProduct));

    aggregatedProductWriter.end();

    const firstRead = aggregatedProductWriter.read();
    const secondRead = aggregatedProductWriter.read();

    expect(isObject(firstRead)).to.equal(true);
    expect(firstRead.ean).to.equal(123);
    expect(secondRead).to.equal(null);
  });

  it('combine two products with the same ean as one aggregated product', () => {
    const mockProducts = [
      createMockProduct("123", {}),
      createMockProduct("123", {})
    ];

    const aggregatedProductWriter: Stream.Transform = AggregatedProductTransformer.createTransformStream();

    mockProducts.forEach(mockProduct => aggregatedProductWriter.write(mockProduct));

    aggregatedProductWriter.end();

    const firstRead = aggregatedProductWriter.read();
    const secondRead = aggregatedProductWriter.read();

    expect(isObject(firstRead)).to.equal(true);
    expect(firstRead.ean).to.equal(123);
    expect(secondRead).to.equal(null);
  });

  it('save two products with the same ean as one and a single one separately', () => {
    const mockProducts = [
      createMockProduct("123", {}),
      createMockProduct("123", {vendor: Vendor.INTERTOYS}),
      createMockProduct("456", {})
    ];

    const aggregatedProductWriter: Stream.Transform = AggregatedProductTransformer.createTransformStream();

    mockProducts.forEach(mockProduct => aggregatedProductWriter.write(mockProduct));

    aggregatedProductWriter.end();

    const firstRead = aggregatedProductWriter.read();
    const secondRead = aggregatedProductWriter.read();
    const thirdRead = aggregatedProductWriter.read();

    expect(isObject(firstRead)).to.equal(true);
    expect(firstRead.ean).to.equal(123);
    expect(isObject(secondRead)).to.equal(true);
    expect(secondRead.ean).to.equal(456);
    expect(thirdRead).to.equal(null);
  });
});

function createMockProduct(ean, fieldOverrides) {
  return {
    ean,
    title: 'productTitle',
    images: 'img1#img2#img3',
    price: '1.10',
    description: 'moi',
    vendor: Vendor.BOLFEED,
    vendor_rank: null,
    link: 'http://google.com',
    categories: null,
    ...fieldOverrides
  }
}
