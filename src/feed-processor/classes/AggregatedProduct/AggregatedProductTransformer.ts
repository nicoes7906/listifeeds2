/**
 * This transform stream assumes all the products come in sorted by EAN
 */

import * as Stream from "stream";
import {AggregatedProduct} from "./AggregatedProduct";
import {isString} from "lodash";
import {Product} from "../Product/Product";
import {Vendor} from "../../enums/vendor";

export class AggregatedProductTransformer {
  static createTransformStream(): Stream.Transform {

    let buffer: Object = {};
    let lastEan = null;

    const transformStream = new Stream.Transform({
      objectMode: true,
      transform(chunk, encoding, callback) {

        // If ean is defined
        if (isString(chunk.ean)) {
          const productFromChunk = createProductFromChunk(chunk);
          // If ean not is not last handled ean
          if (productFromChunk.ean !== lastEan) {
            // If the last ean exists in the buffer
            if (buffer[lastEan]) {
              pushAggregatedProduct();
            }
            // Create a new buffer entry for the chunk ean
            buffer[productFromChunk.ean] = [productFromChunk];
            lastEan = productFromChunk.ean;

            // Else if the chunk ean is the same as the last ean
          } else {
            // Push a new product to the ean entry in the buffer
            buffer[lastEan].push(productFromChunk);
          }
        } else {
          callback(new Error('Chunk has no ean'));
        }

        callback();
      },
      flush(callback) {
        pushAggregatedProduct();
        callback();
      }
    });

    function pushAggregatedProduct() {
      try {
        // Create the aggregated product
        const agProduct = new AggregatedProduct(buffer[lastEan], [Vendor.ICECAT, Vendor.LISTI]);

        // Push to the read stack if there is a product with prices
        if(agProduct.numberOfPrices > 0) {
          transformStream.push(agProduct);
        }
      } catch (e) {
        console.error(e);
      }

      // Remove the buffer entries for the products at lastEan
      delete buffer[lastEan];
    }

    return transformStream;
  }
}

function createProductFromChunk(chunk) {
  const categories = isString(chunk.categories) ? chunk.categories.split('#') : [];
  return new Product(chunk.ean, chunk.title, chunk.images.split('#'), chunk.description, chunk.price, chunk.vendor, chunk.vendor_rank, chunk?.link, chunk.links?.length > 1 ? JSON.parse(chunk.links) : {}, categories, chunk.fromAge, chunk.toAge, chunk.feedIndex, chunk.video, chunk.boostCategories);
}

