import { Product } from "../Product/Product";
import { ProductInfo } from '../../interfaces/product-info';
import {maxBy, minBy, isEmpty, find} from 'lodash';
import {VENDOR_PRIORITY} from "../../constants/vendor-priority";
import {VendorGroup} from "../../types/vendor-group";
import {RankingHelper} from "../RankingHelper/RankingHelper";
import {Category} from "../../enums/category";
import {Vendor} from "../../enums/vendor";
import {checkForForbiddenWords} from "../../lib/ForbiddenWordsChecker";

export class AggregatedProduct implements ProductInfo {
  public ean: number;
  public title: string;
  public images: Array<String>;
  public description: String;
  public price: number;
  public vendors: Array<VendorGroup>;
  public numberOfPrices: number;
  public ranking: number;
  public categories: Array<Category>;
  public fromAge: number;
  public toAge: number;
  public video: string;
  public boostCategories: string;

  constructor(
    products: Array<Product>,
    infoOnlyVendors: Array<Vendor> = []
  ) {

    const sellingVendors = products.filter(product => product.vendor.length > 0 && infoOnlyVendors.indexOf(product.vendor) < 0);

    const description = AggregatedProduct.reduceToFirstSetValueWithPriority('description', products);
    const title = AggregatedProduct.reduceToFirstSetValueWithPriority('title', products);

    checkForForbiddenWords(description)
    checkForForbiddenWords(title)

    this.ean = +AggregatedProduct.reduceToFirstSetValueWithPriority('ean', products);
    this.title = title;
    this.images = AggregatedProduct.reduceToMostImages(products);
    this.description = description;

    // Selling vendors only
    if(sellingVendors.length > 0) {
      this.price = +AggregatedProduct.reduceToLowestPrice(sellingVendors);
      this.vendors = AggregatedProduct.mapToVendorGroup(sellingVendors);
      this.numberOfPrices = sellingVendors.length;
    } else {
      this.price = -1;
      this.vendors = [];
      this.numberOfPrices = 0;
    }

    const bolOaProduct = find(products, product => product.vendor === Vendor.BOLOPENAPI);
    this.ranking = +RankingHelper.calculateRanking(
      this.images.length,
      this.vendors.length,
      !isEmpty(this.description),
      bolOaProduct ? +bolOaProduct.feedIndex : -1
    );

    this.categories = AggregatedProduct.reduceToUniqueNonEmptyCategories(products);
    this.fromAge = +AggregatedProduct.reduceToFirstSetValueWithPriority('fromAge', products);
    this.toAge = +AggregatedProduct.reduceToFirstSetValueWithPriority('toAge', products);
    this.video = AggregatedProduct.reduceToFirstSetValueWithPriority('video', products);
    this.boostCategories = AggregatedProduct.reduceToFirstSetValueWithPriority('boostCategories', products);
  }

  static getProductsByVendorPriority(products: Array<Product>) {
    return products
      // Filter if the vendor is not on the vendor priority list
      .filter(product => VENDOR_PRIORITY.indexOf(product.vendor) > -1)
      // Sort based on vendor priority
      .sort((productA, productB) =>
        VENDOR_PRIORITY.indexOf(productA.vendor) < VENDOR_PRIORITY.indexOf(productB.vendor) ? -1 : 1);
  }

  static reduceToUniqueNonEmptyCategories(products: Array<Product>) {
    return products.reduce((acc, product) => {
      return acc.concat(product.categories
        .filter(category => !isEmpty(category))
        .filter(category => acc.indexOf(category) === -1))

    }, [])
  }

  static reduceToLowestPrice(products: Array<Product>) {
    return minBy(products, product => product.price).price;
  }

  static reduceToMostImages(products: Array<Product>) {
    return maxBy(products, product => product.images.length).images;
  }

  static mapToVendorGroup(products: Array<Product>): Array<VendorGroup> {
    return products.map(product => ({
        price: product.price,
        name: product.vendor,
        mobileUrl: product.links?.mobileUrl || product.link,
        desktopUrl: product.links?.desktopUrl  || product.link
    }));
  }

  static reduceToFirstSetValueWithPriority(propName: String, products: Array<Product>) {
    const productsByVendorPriority = AggregatedProduct.getProductsByVendorPriority(products);
    return productsByVendorPriority.reduce((acc, product) => {
        return isEmpty(acc) ? product.getValueOf(propName) : acc;
    }, null)
  }
}
