import 'mocha';
import {expect} from 'chai';

import {AggregatedProduct} from "./AggregatedProduct";
import {Product} from "../Product/Product";
import {Vendor} from "../../enums/vendor";
import {Category} from "../../enums/category";

describe('AggregatedProduct', () => {
  const mockProduct1 = new Product(1000000032997, 'Calabaz Jazz Tafellamp', ['https://mb.fcdn.nl/square340/543684/calabaz-jazz-tafellamp.jpg' ], 'hoi', 46.03, Vendor.FONQ, null,'http://coolblue.com', {desktopUrl: 'http://coolblue.com', mobileUrl: 'http://coolblue.com'}, [Category.LEGO], -1, -1, -1, null, null);
  const mockProduct2 = new Product(123, 'product2',['img1','img2'], 'hoi', 45.03, Vendor.BARTSMIT, null,'http://bartsmit.com' ,{desktopUrl: 'http://coolblue.com', mobileUrl: 'http://coolblue.com'},[Category.LEGO], -1, -1, -1, null, null);
  const mockProduct3 = new Product(123, 'product3',[], '', 44.03, Vendor.BOLFEED, null,'http://bol.com', {desktopUrl: 'http://coolblue.com', mobileUrl: 'http://coolblue.com'},[Category.KNUTSELEN], -1, -1, -1, null, null);
  const mockProduct4 = new Product(123, 'product4',[], 'hoi intertoys', 44.03, Vendor.INTERTOYS, null,'http://intertoys.com', {desktopUrl: 'http://coolblue.com', mobileUrl: 'http://coolblue.com'},[Category.EMPTY], -1, -1, -1, null, null);
  const mockProduct5 = new Product(123, 'product3',[], '', 44.03, Vendor.BOLOPENAPI, 45,'http://bol.com', {desktopUrl: 'http://coolblue.com', mobileUrl: 'http://coolblue.com'},[Category.KNUTSELEN], -1, -1, 300, null, null);
  const mockProduct6 = new Product(123, 'product6',[], '', 0, Vendor.ICECAT, 45,'http://bol.com', {desktopUrl: 'http://coolblue.com', mobileUrl: 'http://coolblue.com'},[Category.KNUTSELEN], -1, -1, 300, 'http://videootje.mp4', null);

  it('should get the images from the product with the most images', () => {
    const mockProducts = [
      mockProduct1,
      mockProduct2
    ];
    const agProduct = new AggregatedProduct(mockProducts);
    expect(agProduct.ean).to.equal(1000000032997);
    expect(agProduct.images.length).to.equal(2);
    expect(agProduct.images[1]).to.equal('img2');
  });

  it('should get lowest prices from the products', () => {
    const mockProducts = [
      mockProduct2,
      mockProduct3
    ];

    const agProduct = new AggregatedProduct(mockProducts);
    expect(agProduct.price).to.equal(44.03);
  });

  it('should get the title from a non empty value based on vendor priority', () => {
    const mockProducts = [
      mockProduct1,
      mockProduct3,
      mockProduct4,
      mockProduct5
    ];

    const agProduct = new AggregatedProduct(mockProducts);
    expect(agProduct.title).to.equal('product3');
  });

  it('should get the title from a non empty value based on vendor priority', () => {
    const mockProducts = [
      mockProduct1
    ];

    const agProduct = new AggregatedProduct(mockProducts);
    expect(agProduct.ean).to.equal(1000000032997);
  });

  it('should description from the non empty value based on vendor priority', () => {
    const mockProducts = [
      mockProduct1,
      mockProduct3,
      mockProduct4
    ];

    const agProduct = new AggregatedProduct(mockProducts);
    expect(agProduct.description).to.equal('hoi intertoys');
  });

  it('should have unique categories', () => {
    const mockProducts = [
      mockProduct1,
      mockProduct2,
      mockProduct3,
      mockProduct4
    ];

    const agProduct = new AggregatedProduct(mockProducts);
    expect(agProduct.categories.length).to.equal(3);
    expect(agProduct.categories[0]).to.equal(Category.LEGO);
    expect(agProduct.categories[1]).to.equal(Category.KNUTSELEN);
    expect(agProduct.categories[2]).to.equal(Category.EMPTY);
    expect(agProduct.categories[2]).to.equal(Category.EMPTY);
  });

  describe('ranking', () => {
    it('should have a ranking below 1200 if one of the products is from the Bol Open API and has a feed index', () => {
      const mockProducts = [
        mockProduct1,
        mockProduct2,
        mockProduct3,
        mockProduct4,
        mockProduct5
      ];

      const agProduct = new AggregatedProduct(mockProducts);
      expect(agProduct.ranking).to.be.below(1200);
    });
    it('should have a ranking above 1200 if none of the products is from the Bol Open API', () => {
      const mockProducts = [
        mockProduct1,
        mockProduct2,
        mockProduct3,
        mockProduct4
      ];

      const agProduct = new AggregatedProduct(mockProducts);
      expect(agProduct.ranking).to.be.above(1200);
    });
  });

  describe('Info only vendors', () => {
    it('should return null if the only vendor is info only', () => {
      const mockProducts = [mockProduct6];

      const infoOnlyVendors = [Vendor.ICECAT];

      const agProduct = new AggregatedProduct(mockProducts, infoOnlyVendors);
      expect(agProduct.numberOfPrices).to.equal(0)
    });
  });
});
