import {ProductMappable} from "../../interfaces/product-mappable";
import {Product} from "../Product/Product";
import {Vendor} from "../../enums/vendor";

const matchMp4 = /^https?.*?\.mp4$/

export class IcecatPcfCsvMapper implements ProductMappable {
  mapProduct(csvObject: any, feedIndex:number, logError: Function): Product | null {
    const {ProductTitle, ProductGallery, LongSummaryDescription, ErrorMessage} = csvObject;

    if(ErrorMessage && ErrorMessage.length > 0) {
      return null;
    }

    let video = null;
    if(csvObject['Video/mp4'] && csvObject['Video/mp4'].length > 0) {
      video = csvObject['Video/mp4'].split('|').find(video => video.match(matchMp4))
    }

    return new Product(
      csvObject['Requested_GTIN(EAN/UPC)'],
      ProductTitle,
      ProductGallery.split('|'),
      LongSummaryDescription,
      null,
      Vendor.ICECAT,
      null,
      null,
      null,
      [],
      -1,
      -1,
      1,
      video,
      null
    )
  }
}
