import {IcecatPcfCsvMapper} from "./IcecatMapper";
import {expect} from "chai";
import {Vendor} from "../../enums/vendor";

describe('IcecatMapper', () => {

  it('should correctly map icecat products', () => {
    const mapper = new IcecatPcfCsvMapper();
    const result = mapper.mapProduct(getMockCsvProduct(), 1, console.error)
    expect(result.ean).to.equal(606449140491);
    expect(result.title).to.equal('Netgear RAX200 draadloze router Gigabit Ethernet Tri-band (2.4 GHz / 5 GHz / 5 GHz) Zwart');
    expect(result.images).to.deep.equal([
      "http://images.icecat.biz/img/gallery/71325309_9677181512.jpg",
      "http://images.icecat.biz/img/gallery/71325309_2231453710.jpg",
      "http://images.icecat.biz/img/gallery/71325309_1028722655.jpg",
      "http://images.icecat.biz/img/gallery/71325309_7596636636.jpg",
      "http://images.icecat.biz/img/gallery/71325309_6272636411.jpg"
    ]);
    expect(result.description).to.equal('Netgear RAX200 long description.');
    expect(result.categories.length).to.equal(0);
    expect(result.price).to.equal(0);
    expect(result.vendor).to.equal(Vendor.ICECAT);
    expect(result.link).to.equal(null);
    expect(result.fromAge).to.equal(-1);
    expect(result.toAge).to.equal(-1);
    expect(result.video).to.equal("https://hello.nl/withvideo.mp4");
  })

  it('should only include mp4 videos', () => {
    const mapper = new IcecatPcfCsvMapper();
    const mockProduct = getMockCsvProduct();
    mockProduct['Video/mp4'] = 'http://www.youtube.com/watch?v=gG-q6u6DMFg|http://www.youtube.com/watch?v=IFrnzMTHdXU|https://icecat.biz/videosdfsdfsd.mp4' //TODO: Mock youtube
    const result = mapper.mapProduct(mockProduct, 1, console.error)
    expect(result.video).to.equal('https://icecat.biz/videosdfsdfsd.mp4')
  })

  it('should return null if the product has an ErrorMessage', () => {
    const mapper = new IcecatPcfCsvMapper();
    const mockProduct = getMockCsvProduct();
    mockProduct.ErrorMessage = 'Product data-sheet is not public, please contact your account manager at Icecat'
    const result = mapper.mapProduct(mockProduct, 1, console.error)
    expect(result).to.equal(null)
  })
})


function getMockCsvProduct() {
  return {
    "360": "",
    "Requested_prod_id": "",
    "Requested_GTIN(EAN/UPC)": "606449140491",
    "Requested_Icecat_id": "71325309",
    "ErrorMessage": "",
    "Supplier": "Netgear",
    "Prod_id": "RAX200-100EUS",
    "Icecat_id": "71325309",
    "GTIN(EAN/UPC)": "0606449140491|10606449140498",
    "Category": "Draadloze routers",
    "CatId": "3982",
    "ProductFamily": "",
    "ProductSeries": "",
    "Model": "RAX200",
    "Updated": "20201223215157",
    "Quality": "ICECAT",
    "On_Market": "",
    "Product_Views": "23867",
    "HighPic": "http://images.icecat.biz/img/gallery/71325309_9677181512.jpg",
    "HighPic Resolution": "1500x1245",
    "LowPic": "http://images.icecat.biz/img/gallery_lows/71325309_9677181512.jpg",
    "Pic500x500": "http://images.icecat.biz/img/gallery_mediums/71325309_9677181512.jpg",
    "ThumbPic": "http://images.icecat.biz/img/gallery_thumbs/71325309_9677181512.jpg",
    "Folder_PDF": "",
    "Folder_Manual_PDF": "",
    "ProductTitle": "Netgear RAX200 draadloze router Gigabit Ethernet Tri-band (2.4 GHz / 5 GHz / 5 GHz) Zwart",
    "ShortDesc": "",
    "ShortSummaryDescription": "Netgear RAX200, Wi-Fi 6 (802.11ax), Tri-band (2.4 GHz / 5 GHz / 5 GHz), Ethernet LAN, Zwart, Router om neer te zetten",
    "LongSummaryDescription": "Netgear RAX200 long description.",
    "LongDesc": "",
    "Short Marketing Text": "",
    "SEO Title": "",
    "SEO Description": "",
    "SEO Keywords": "",
    "ProductGallery": "http://images.icecat.biz/img/gallery/71325309_9677181512.jpg|http://images.icecat.biz/img/gallery/71325309_2231453710.jpg|http://images.icecat.biz/img/gallery/71325309_1028722655.jpg|http://images.icecat.biz/img/gallery/71325309_7596636636.jpg|http://images.icecat.biz/img/gallery/71325309_6272636411.jpg",
    "ProductGallery Resolution": "1500x1245|1350x1245|1350x669|1350x932|1402x1019",
    "ProductGallery ExpirationDate": "||||",
    "EU Energy Label": "",
    "EU Product Fiche": "",
    "PDF": "",
    "Video/mp4": "https://hello.nl/withvideo.mp4",
    "Other Multimedia": "",
    "ProductMultimediaObject ExpirationDate": "",
    "ReasonsToBuy": "",
    "Bullet Points": "",
    "Variant_prod_id": "",
    "Variant_GTIN(EAN/UPC)": "",
    "Your product ID": "",
    "Specs 1": "WAN-verbinding",
    "Specs 2": "Ethernet WAN: Ja",
    "Specs 3": "Type WAN-aansluiting: RJ-45",
    "Specs 4": "Draadloos LAN",
    "Specs 5": "Wifi-band: Tri-band (2.4 GHz / 5 GHz / 5 GHz)",
    "Specs 6": "Wifi-standaard: Wi-Fi 6 (802.11ax)",
    "Specs 7": "WLAN gegevensoverdrachtsnelheid (max): 2500 Mbit/s",
    "Specs 8": "Wi-Fi-standaarden: 802.11a,802.11b,802.11g,Wi-Fi 4 (802.11n),Wi-Fi 5 (802.11ac)",
    "Specs 9": "WLAN gegevensoverdrachtsnelheid (eerste band): 1000 Mbit/s",
    "Specs 10": "WLAN gegevensoverdrachtsnelheid (tweede band): 1000 Mbit/s",
    "Specs 11": "WLAN-gegevensoverdrachtsnelheid (derde band): 1000 Mbit/s",
    "Specs 12": "Hoog vermogen: Ja",
    "Specs 13": "Netwerk",
    "Specs 14": "Ethernet LAN: Ja",
    "Specs 15": "Interfacetype Ethernet LAN: Gigabit Ethernet",
    "Specs 16": "Ethernet LAN, data-overdrachtsnelheden: 10,100,1000 Mbit/s",
    "Specs 17": "Bekabelingstechnologie: 10/100/1000Base-T(X)",
    "Specs 18": "Netwerkstandaard: IEEE 802.11ac,IEEE 802.11b,IEEE 802.11g,IEEE 802.11n",
    "Specs 19": "VPN eigenschappen: Secure access to your home network away from home",
    "Specs 20": "Poorten & interfaces",
    "Specs 21": "Aantal Ethernet LAN (RJ-45)-poorten: 4",
    "Specs 22": "USB-poort: Ja",
    "Specs 23": "Aantal poorten USB 3.2 Gen 1 (3.1 Gen 1) Type A: 2",
    "Specs 24": "RJ-11 ports kwantiteit: 1",
    "Specs 25": "Geheugenkaart slot(s): Ja",
    "Specs 26": "Aansluiting voor netstroomadapter: Ja",
    "Specs 27": "Beheerfuncties",
    "Specs 28": "Reset button: Ja",
    "Specs 29": "Protocollen",
    "Specs 30": "Universal Plug and Play ( UPnP ): Ja",
    "Specs 31": "Design",
    "Specs 32": "Type product: Router om neer te zetten",
    "Specs 33": "Kleur van het product: Zwart",
    "Specs 34": "Indicatielampje: Ja",
    "Specs 35": "LED-indicatoren: Stroom",
    "Specs 36": "Aan/uitschakelaar: Ja",
    "Specs 37": "Antenne",
    "Specs 38": "Soort antenne: Extern",
    "Specs 39": "Kenmerken",
    "Specs 40": "Type processor: Ja",
    "Specs 41": "Frequentie van processor: 1,8 MHz",
    "Specs 42": "Aantal processorkernen: 4",
    "Specs 43": "Intern geheugen: 1000 MB",
    "Specs 44": "Flash memory: 512 MB",
    "Specs 45": "Certificering: FCC, CE, RoHS",
    "Specs 46": "Energie",
    "Specs 47": "Type stroombron: DC",
    "Specs 48": "Minimale systeemeisen",
    "Specs 49": "Ondersteunde browser: Internet Explorer, Microsoft Edge, Google Chrome, Firefox, Safari",
    "Specs 50": "Ondersteunt Windows: Windows 10,Windows 2000,Windows 7,Windows 8,Windows Vista,Windows XP",
    "Specs 51": "Ondersteunt Linux: Ja",
    "Specs 52": "Ondersteunt Mac-besturingssysteem: Ja",
    "Specs 53": "Gewicht en omvang",
    "Specs 54": "Breedte: 298 mm",
    "Specs 55": "Diepte: 78 mm",
    "Specs 56": "Hoogte: 11 mm",
    "Specs 57": "Gewicht: 1,43 kg",
    "Specs 58": "Verpakking",
    "Specs 59": "Meegeleverde kabels: DC, LAN (RJ-45)",
    "Specs 60": "Inclusief AC-adapter: Ja",
    "Specs 61": "Gebruikershandleiding: Ja",
    "Specs 62": "Snelle installatiehandleiding: Ja",
    "Specs 63": "Breedte verpakking: 250 mm",
    "Specs 64": "Diepte verpakking: 331 mm",
    "Specs 65": "Hoogte verpakking: 141 mm",
    "Specs 66": "Gewicht verpakking: 2,18 kg",
    "Specs 67": "Aantal producten inbegrepen: 1 stuk(s)",
    "Specs 68": "Logistieke gegevens",
    "Specs 69": "(Buitenste) hoofdverpakking breedte: 28 cm",
    "Specs 70": "(Buitenste) hoofdverpakking lengte: 36,3 cm",
    "Specs 71": "(Buitenste) hoofdverpakking hoogte: 37 cm",
    "Specs 72": "(Buitenste) hoofdverpakking brutogewicht: 5,61 kg",
    "Specs 73": "Hoeveelheid per (buitenste) hoofdverpakking: 2 stuk(s)"
  }
}
