import {isEmpty, isString, isNumber} from 'lodash';

export class ProductValidator {

  static isValidEAN(ean: string) {
    return !isEmpty(ean) && ean.match(/^[0-9]{6,13}$/);
  }

  static isValidTitle(title: any) {
    return !isEmpty(title) && isString(title) && title.length > 2;
  }

  static isValidAge(age: any) {
    return isNumber(age) && age > 0;
  }

}
