export class AgeHelper {

  static getFromAndToAge(fromAge: number, toAge: number) {
    return {
      fromAge: fromAge > 0 ? fromAge : 0,
      toAge: toAge < 18 ? toAge : 17
    };
  }
}
