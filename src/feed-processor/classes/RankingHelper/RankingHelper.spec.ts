import 'mocha';
import { expect } from 'chai';

import {RANKING_IMPORTANCE_VECTORS, RankingHelper} from "./RankingHelper";

describe('RankingHelper', () => {

  describe('calculateRanking', () => {
    const defaultvendorRating = 0;

    it('should give a higher ranking to products that have more images', () => {
        const highestRanking = RankingHelper.calculateRanking(3, 1, true, defaultvendorRating);
        const averageRanking = RankingHelper.calculateRanking(2, 1, true, defaultvendorRating);
        const lowestRanking = RankingHelper.calculateRanking(1, 1, true, defaultvendorRating);
        expect(highestRanking).to.be.below(averageRanking);
        expect(averageRanking).to.be.below(lowestRanking);
    });

    it('should give a higher ranking number to products that have multiple vendors', () => {
      const highestRanking = RankingHelper.calculateRanking(1, 3, true, defaultvendorRating);
      const averageRanking = RankingHelper.calculateRanking(1, 2, true, defaultvendorRating);
      const lowestRanking = RankingHelper.calculateRanking(1, 1, true, defaultvendorRating);
      expect(highestRanking).to.be.below(averageRanking);
      expect(averageRanking).to.be.below(lowestRanking);
    });

    it('should give a higher ranking number to products that have more vendors than images', () => {
      const highestRanking = RankingHelper.calculateRanking(1, 3, true, defaultvendorRating);
      const averageRanking = RankingHelper.calculateRanking(2, 2, true, defaultvendorRating);
      const lowestRanking = RankingHelper.calculateRanking(3, 1, true, defaultvendorRating);
      expect(highestRanking).to.be.below(averageRanking);
      expect(averageRanking).to.be.below(lowestRanking);
    });

    it('should give a higher ranking number to products that have a description', () => {
      const highestRanking = RankingHelper.calculateRanking(0, 0, true, defaultvendorRating);
      const lowestRanking = RankingHelper.calculateRanking(0, 0, false, defaultvendorRating);
      expect(highestRanking).to.be.below(lowestRanking);
    });
  });

  describe('Ranking importance vectors', () => {
    it('should never be higher then a total of 100 points', () => {
      const totalRankingImportancePoints = Object
        .keys(RANKING_IMPORTANCE_VECTORS)
        .reduce((acc, key) => acc + RANKING_IMPORTANCE_VECTORS[key], 0);

      expect(totalRankingImportancePoints).to.equal(100);
    });
  });
});
