export const RANKING_IMPORTANCE_VECTORS = {
  images: 3,
  vendors: 36,
  description: 3,
  title: 18,
  vendorRank: 40
};

const noBolOpenApiIncrement = 50000;

export class RankingHelper {
  static calculateRanking(numberOfImages: number, numberOfVendors: number, hasDescription: boolean, bolOpenApiIndex: number) {
    const factors = {
      images: numberOfImages > 0 ? RANKING_IMPORTANCE_VECTORS.images / numberOfImages : RANKING_IMPORTANCE_VECTORS.images,
      vendors: numberOfVendors > 0 ? RANKING_IMPORTANCE_VECTORS.vendors / numberOfVendors : RANKING_IMPORTANCE_VECTORS.vendors,
      description: hasDescription ? 0 : RANKING_IMPORTANCE_VECTORS.description
    };

    return bolOpenApiIndex > 0 ? bolOpenApiIndex : Object.keys(factors).reduce((acc, key) => {
        return acc + factors[key];
    }, noBolOpenApiIncrement);
  }
}
