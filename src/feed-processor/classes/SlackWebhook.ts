import * as Slack from 'slack-node';
import {SlackConstants} from "../constants/slack";
import {Logger} from "winston";

export class SlackWebhook {
  static createSlackLogger(logger: Logger) {
    let slack = new Slack();
    slack.setWebhook(SlackConstants.LISTI_WEBHOOK_URL);

    return {log};

    function log(msg): Promise<void> {
      return new Promise((resolve, reject) => {
        slack.webhook({
          channel: "#general",
          username: "slackbot",
          text: msg
        }, function(err, response) {
          if(err) {
            logger.error('Slack message failed with error: ' + err );
            reject(err);
          }

          slack = null;
          resolve();
        });
      })
    }

  }
}
