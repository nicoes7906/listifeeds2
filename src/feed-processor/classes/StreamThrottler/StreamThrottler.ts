export class StreamThrottler {
  maxOperationsInProgress: number;
  maxPause: number;
  pendingOperations: number = 0;
  pauseFn: Function;
  resumeFn: Function;

  constructor(pauseFn: Function, resumeFn: Function, maxOperationsInProgress?: number, maxPause?:number) {
    this.maxOperationsInProgress = maxOperationsInProgress || 20;
    this.maxPause = maxPause || 10;
    this.pauseFn = pauseFn;
    this.resumeFn = resumeFn;
    this.incrementOperationCount = this.incrementOperationCount.bind(this);
    this.decrementOperationCount = this.decrementOperationCount.bind(this);
  }

  throttle(callback) {
    callback(this.incrementOperationCount, this.decrementOperationCount);
    if(this.pendingOperations > this.maxOperationsInProgress) {
      this.pauseFn();
      const resumeInterval = setInterval(() => {
        if(this.pendingOperations <= this.maxOperationsInProgress) {
          this.resumeFn();
          clearInterval(resumeInterval);
        }
      }, this.maxPause);
    }
  }

  incrementOperationCount(){
    this.pendingOperations++;
  }

  decrementOperationCount() {
    this.pendingOperations--;
  }

}
