import 'mocha';
import * as sinon from 'sinon';
import {StreamThrottler} from "./StreamThrottler";
import {SinonSpy} from "sinon";

describe('StreamThrottler', () => {
  let clock;

  beforeEach(() => {
    clock = sinon.useFakeTimers();
  });

  it('should pause when the max number of operations has been reached', () => {
    const pauseSpy: SinonSpy = sinon.spy();
    const resumeSpy: SinonSpy = sinon.spy();
    const st = new StreamThrottler(pauseSpy, resumeSpy, 1, 1);
    st.throttle((incrementCb, decrementCb) => incrementCb());
    sinon.assert.notCalled(pauseSpy);
    st.throttle((incrementCb, decrementCb) => incrementCb());
    clock.tick(2);
    sinon.assert.called(pauseSpy);
    sinon.assert.notCalled(resumeSpy);
    st.throttle((incrementCb, decrementCb) => decrementCb());
    clock.tick(2);
    sinon.assert.called(resumeSpy);
  });

  afterEach(() => {
    clock.restore();
  })

});
