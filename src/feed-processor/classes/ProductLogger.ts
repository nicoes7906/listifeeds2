import {Timer} from "./Timer";
import * as stream from "stream";

export class ProductLogger {
  static create() {
    let logCount: number = 0;

    return new stream.Writable({
      objectMode: true,
      write(chunk, enc, next) {
        logCount++;
        console.log(`${logCount} | ${JSON.stringify(chunk)}`);
        next();
      }
    })
  }
}
