import {expect} from "chai";
import {Vendor} from "../../enums/vendor";
import {CustomProductCsvMapper} from "./CustomProductCsvMapper";

describe('Custom products', () => {

  it('should correctly map custom products', () => {
    const mapper = new CustomProductCsvMapper();
    const result = mapper.mapProduct(getMockCustomProduct())
    expect(result.ean).to.equal(123);
    expect(result.title).to.equal('the title');
    expect(result.images).to.deep.equal([
      "http://image1.jpg",
      "http://image2.jpg"
    ]);
    expect(result.description).to.equal('the description');
    expect(result.categories.length).to.equal(0);
    expect(result.price).to.equal(0);
    expect(result.vendor).to.equal(Vendor.LISTI);
    expect(result.link).to.equal(null);
    expect(result.fromAge).to.equal(3);
    expect(result.toAge).to.equal(12);
    expect(result.video).to.equal("http://thevideo.mp4");
  })
})


function getMockCustomProduct() {
  return {
    ean: '123',
    title: 'the title',
    description: 'the description',
    video: 'http://thevideo.mp4',
    images: 'http://image1.jpg|http://image2.jpg',
    feedIndex: 1,
    fromAge: 3,
    toAge: 12
  }
}
