import {ProductMappable} from "../../interfaces/product-mappable";
import {Product} from "../Product/Product";
import {Vendor} from "../../enums/vendor";

export class CustomProductCsvMapper implements ProductMappable {
  mapProduct(csvObject: any): Product | null {
    const {ean, title, images, description, fromAge, toAge, video, boostCategories} = csvObject;

    return new Product(
      ean,
      title,
      images.split('|'),
      description,
      null,
      Vendor.LISTI,
      null,
      null,
      null,
      [],
      fromAge,
      toAge,
      1,
      video,
      boostCategories
    )
  }
}
