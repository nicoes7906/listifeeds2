import {Logger} from 'winston';
import {Timer} from "../Timer";
import {ProductStreamHelpers} from "../Product/ProductStreamHelpers";
import {Promise} from "core-js";
import * as csv from 'csv';
import {Runnable} from "../../interfaces/runnable";
import {RunLimitHandler} from "../RunLimitHandler";
import {ProductBatchWriterMysql} from "../Product/ProductBatchWriterMysql";
import {S3} from "aws-sdk";
import {Pool} from "mysql";
import {createS3DownloadStream} from "../../lib/S3Download";
import {S3FileNames} from "../../constants/S3";
import {CustomProductCsvMapper} from "./CustomProductCsvMapper";

export class CustomProductFetcherStream {
  static createCsvFetcher(logTag: string, sqlClient: Pool, s3Client: S3, logger: Logger, fileName: S3FileNames): Runnable {
    return {
      run
    };

    function run(): Promise<string> {
      return new Promise((resolve, reject) => {
        const productMapper = new CustomProductCsvMapper();
        const customProductMapper = ProductStreamHelpers.createProductMapStream(
          productMapper.mapProduct.bind(productMapper),
          logger,
          reject
        );
        const timer = new Timer(logTag, logger.info.bind(logger));

        timer.start();

        const productWriter = ProductBatchWriterMysql.createProductWriteStream(
          sqlClient,
          logTag,
          logger,
          timer
        );

        const requestStream = createS3DownloadStream(s3Client, fileName, logger, false);

        const runLimitHandler = RunLimitHandler.createRunLimitHandlerStream(
          requestStream.destroy.bind(requestStream),
          logger.error.bind(logger)
        );

        requestStream
          .pipe(csv.parse({columns: true, delimiter: ",", relax: true}))
          .pipe(runLimitHandler)
          .pipe(customProductMapper)
          .pipe(productWriter)
          .on('finish', () => resolve('Ok'))
      });
    }
  }
}
