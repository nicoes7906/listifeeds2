import * as Stream from "stream";

export class MemoryStore {
  static createDuplexStream() {
    const readArr: Buffer[] = [];

    return new Stream.Duplex({
      write(chunk, encoding, callback) {
        var buffer = (Buffer.isBuffer(chunk)) ?
          chunk :  // already is Buffer use it
          new Buffer(chunk, encoding);  // else string: convert

        readArr.push(buffer);
        callback();
      },
      read() {
        const chunk = readArr.shift();
        if(readArr.length) {
          this.push(chunk);
        } else {
          this.push(null);
        }
      }
    })
  }
}
