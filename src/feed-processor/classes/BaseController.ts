import { Logger } from "winston";
import {Pool} from 'mysql2';

export abstract class BaseController {
  logger: Logger;
  connectionPool: Pool;

  constructor(logger: Logger, connectionPool: Pool) {
    this.logger = logger;
    this.connectionPool = connectionPool;
  }

  abstract run() : Promise<any>;
}
