import { Logger } from "winston";
import {S3} from "aws-sdk";
import {Pool} from "mysql";

export abstract class BaseDocumentController {
  logger: Logger;
  sqlClient: Pool;
  s3Client: S3;

  constructor(logger: Logger, sqlClient: Pool, s3Client: S3) {
    this.logger = logger;
    this.sqlClient = sqlClient;
    this.s3Client = s3Client;
  }

  abstract run(opts?: any) : Promise<any>;
}
