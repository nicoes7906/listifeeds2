import {Transform} from "stream";
import {isBoolean} from "lodash";

export class RunLimitHandler {

  static createRunLimitHandler(abortFn) {
    let count = 0;
    const runLimit = this.getRunLimit();
    return fn => {
      count++;
      if (runLimit) {
        if (count <= runLimit) {
          fn();
        } else {
          abortFn();
        }
      } else {
        fn();
      }
    }
  }

  static createRunLimitHandlerStream(abortFn, onError: Function, objectMode?: boolean) {
    let count = 0;
    const runLimit = this.getRunLimit();
    const finalObjectMode = isBoolean(objectMode) ? objectMode : true;
    return new Transform({
      objectMode: finalObjectMode,
      transform(chunk: any, enc, callback) {
        count++;
        if (runLimit && count > runLimit) {
          abortFn();
        } else {
          this.push(chunk);
        }
        callback();
      }
    }).once('error', () => onError())
  }

  static getRunLimit() {
    return process.env.RUN_LIMIT && parseInt(process.env.RUN_LIMIT);
  }

}
