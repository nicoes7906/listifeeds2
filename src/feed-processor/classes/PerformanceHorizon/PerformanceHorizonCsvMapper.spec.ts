import 'mocha';
import { mock } from 'sinon';
import { expect } from 'chai';
import { Vendor } from '../../enums/vendor';
import { Product } from '../Product/Product';
import { PerformanceHorizonXmlMapper } from './PerformanceHorizonXmlMapper';
import {Category} from "../../enums/category";
import {PerformanceHorizonCsvMapper} from "./PerformanceHorizonCsvMapper";

describe('PerformanceHorizonMapper', () => {

  it('should correctly map an csv product to a listi product', () => {
    const mockCsvProduct = getMockCsvProduct();
    const mapper = new PerformanceHorizonCsvMapper(Vendor.WEHKAMP);
    const result = mapper.mapProduct(mockCsvProduct, () => {});
    expect(result.ean).to.equal(9789461260888);
    expect(result.title).to.equal('Zakelijk twitteren voor gevorderden in 60 minuten - Maaike Gulden');
    expect(result.images).to.deep.equal(['https://assets.wehkamp.com/i/wehkamp/16019218_pb_01.jpg?w=500&h=500', 'https://assets.wehkamp.com/i/wehkamp/16019218_eb_02.jpg?w=500&h=500', 'https://assets.wehkamp.com/i/wehkamp/16019218_eb_03.jpg?w=500&h=500']);
    expect(result.description.indexOf('Je maakt al ')).to.equal(0);
    expect(result.categories.length).to.equal(0);
    expect(result.price).to.equal(12.50);
    expect(result.vendor).to.equal(Vendor.WEHKAMP);
    expect(result.link).to.equal('');
    expect(result.links.desktopUrl).to.equal('https://prf.hn/click/camref:1101l3crA/creativeref:1100l15610/destination:https://www.wehkamp.nl/Winkelen/KenmerkAdviseurArtikel.aspx?CC=C31&SC=PA3&KAC=Q09&artikelNummer=16019218&MaatCode=0000&utm_campaign=affiliates_productfeed_Managementboeken_Boeken,+films+&+muziek=&dfw_tracker=22972-16019218');
    expect(result.links.mobileUrl).to.equal('https://prf.hn/click/camref:1101l3crA/creativeref:1100l15610/destination:https://www.wehkamp.nl/Winkelen/KenmerkAdviseurArtikel.aspx?CC=C31&SC=PA3&KAC=Q09&artikelNummer=16019218&MaatCode=0000&utm_campaign=affiliates_productfeed_Managementboeken_Boeken,+films+&+muziek=&dfw_tracker=22972-16019218');
    expect(result.fromAge).to.equal(-1);
    expect(result.toAge).to.equal(-1);
  });

});

function getMockCsvProduct() {
  return {
    "Product_Name":"Zakelijk twitteren voor gevorderden in 60 minuten - Maaike Gulden",
    "Product_Image_URL":"https://assets.wehkamp.com/i/wehkamp/16019218_pb_01.jpg?w=500&h=500",
    "Product Value":"12.50",
    "Product Category":"Boeken, films & muziek",
    "Product_ID":"16019218-0000",
    "Product_URL":"https://prf.hn/click/camref:1101l3crA/creativeref:1100l15610/destination:https://www.wehkamp.nl/Winkelen/KenmerkAdviseurArtikel.aspx?CC=C31&SC=PA3&KAC=Q09&artikelNummer=16019218&MaatCode=0000&utm_campaign=affiliates_productfeed_Managementboeken_Boeken,+films+&+muziek=&dfw_tracker=22972-16019218",
    "brand":"",
    "deliveryTime":"Voor 23:00 besteld, volgende dag in huis",
    "EAN_Code":"9789461260888",
    "Price":"12.50",
    "shippingCost":"2.95",
    "size":"000",
    "sku":"16019218",
    "availability":"0",
    "kleur":"",
    "Product_Description":"Je maakt al gebruik van Twittter, maar je hebt het gevoel dat je er niet uithaalt wat erin zit? Zet de volgende stap en ontdek in dit boek hoe je een trouwe Twittercommunity opbouwt en volgers bindt met boeiende content. Maaike Gulden beschrijft hoe je Twitter kunt inzetten voor acquisitie, marketing, klantenservice en personal branding. Weinig tijd, maar veel ambities? Informeer jezelf snel en grondig met de boeken in de serie Digitale trends en tools in 60 minuten. De serie is een initiatief van Uitgeverij Haystack in samenwerking met Frankwatching.com, het toonaangevende platform over online trends, tips & tricks.",
    "promoText":"Zakelijk twitteren voor gevorderden in 60 minuten - Maaike Gulden",
    "shortDescription":"Zakelijk twitteren voor gevorderden in 60 minuten - Maaike Gulden",
    "weight":"",
    "SubCategory":"Managementboeken",
    "SubSubCategory":"Bedrijfskunde- en marketingboeken",
    "DeliveryCost":"2.95",
    "Category":"Boeken, films & muziek",
    "Model":"Boek",
    "Bijverkoop1":"",
    "Upsell":"",
    "AantalReviews":"0",
    "GemiddeldeRating":"",
    "Extra_Image1":"https://assets.wehkamp.com/i/wehkamp/16019218_eb_02.jpg?w=500&h=500",
    "Extra_Image2":"https://assets.wehkamp.com/i/wehkamp/16019218_eb_03.jpg?w=500&h=500",
    "Bijverkoop2":"",
    "Bijverkoop3":"",
    "Bijverkoop4":"",
    "Typenummer":""
  };
}

