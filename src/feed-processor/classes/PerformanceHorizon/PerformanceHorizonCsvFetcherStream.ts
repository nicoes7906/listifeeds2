import {Logger} from 'winston';
import {Timer} from "../Timer";
import {PerformanceHorizon} from "../../constants/performance-horizon";
import {PerformanceHorizonCsvMapper} from "./PerformanceHorizonCsvMapper";
import {Vendor} from "../../enums/vendor";
import {ProductStreamHelpers} from "../Product/ProductStreamHelpers";
import * as request from "request";
import {Promise} from "core-js";
import * as csv from 'csv';
import {Runnable} from "../../interfaces/runnable";
import {RunLimitHandler} from "../RunLimitHandler";
import {ProductBatchWriterMysql} from "../Product/ProductBatchWriterMysql";
import {DynamoDB, S3} from "aws-sdk";
import {createCsvReaderTransform} from "../CsvReaderTransform/CsvReaderTransform";
import {Pool} from "mysql";
import {PerformanceHorizonTargetFile} from "../../enums/PerformanceHorizonTargetFile";
import {createS3DownloadStream} from "../../lib/S3Download";

export class PerformanceHorizonCsvFetcherStream {
  static createCsvFetcher(vendor: Vendor, logTag: string, sqlClient: Pool, s3Client: S3, logger: Logger, fileName: PerformanceHorizonTargetFile): Runnable {
    return {
      run
    };

    function run(): Promise<string> {
      return new Promise((resolve, reject) => {
        const productMapper = new PerformanceHorizonCsvMapper(Vendor.WEHKAMP);
        const wkMapper = ProductStreamHelpers.createProductMapStream(
          productMapper.mapProduct.bind(productMapper),
          logger,
          reject
        );
        const timer = new Timer(logTag, logger.info.bind(logger));

        timer.start();

        const productWriter = ProductBatchWriterMysql.createProductWriteStream(
          sqlClient,
          logTag,
          logger,
          timer
        );

        const requestStream = createS3DownloadStream(s3Client, fileName, logger);

        const runLimitHandler = RunLimitHandler.createRunLimitHandlerStream(
          requestStream.destroy.bind(requestStream),
          logger.error.bind(logger)
        );

        requestStream
          .pipe(csv.parse({columns: true, delimiter: "\t", relax: true}))
          .pipe(runLimitHandler)
          .pipe(wkMapper)
          .pipe(productWriter)
          .on('finish', () => resolve('Ok'))
      });
    }
  }
}
