import { ProductMappable } from "../../interfaces/product-mappable";
import { Product } from "../Product/Product";
import { Vendor } from "../../enums/vendor";
import { isEmpty, uniq } from 'lodash';
import {ProductValidator} from "../ProductValidator/ProductValidator";
import {sanitizePhImage} from "../../lib/SanitizePhImage";
import {ProductLinks} from "../../types/link";

export class PerformanceHorizonCsvMapper implements ProductMappable {
  vendor: Vendor;
  categories: Array<string>;

  constructor(vendor) {
    this.vendor = vendor;
    this.categories = [];
  }

  mapProduct(phCsvObject: any, logError: Function): Product | null {
    if(PerformanceHorizonCsvMapper.isValidProduct(phCsvObject)) {
      const {EAN_Code, Price, Product_Name, Product_URL, Product_Image_URL, Product_Description, Product_Sub_Category} = phCsvObject;
      const images = [sanitizePhImage(Product_Image_URL)].concat(PerformanceHorizonCsvMapper.getExtraImages(phCsvObject));

      this.categories.push(Product_Sub_Category);

      const links: ProductLinks = {
        mobileUrl: Product_URL,
        desktopUrl: Product_URL
      }

      try {
        return new Product(
          EAN_Code,
          Product_Name,
          images,
          Product_Description,
          Price,
          this.vendor,
          null,
          '',
          links,
          [],
          -1,
          -1,
          -1,
          null,
          null
        );
      } catch (e) {
        logError(e);
      }
    }
    return null;
  }

  getUniqueCategories() {
    return uniq(this.categories);
  }

  static getExtraImages(phCsvObject) {
    return Object.keys(phCsvObject).reduce((acc, key) => {
      if(key.indexOf('Extra_Image') > -1 && !isEmpty(phCsvObject[key])) {
        acc.push(sanitizePhImage(phCsvObject[key]));
      }
      return acc;
    }, [])
  }

  static isValidProduct(phCsvObject) {
    return !!phCsvObject && ProductValidator.isValidEAN(phCsvObject.EAN_Code) && ProductValidator.isValidTitle(phCsvObject["Product_Name"]);
  }
}
