import * as XmlStream from 'xml-stream';
import * as http from 'http';
import { PerformanceHorizon } from '../../constants/performance-horizon';
import { EventEmitter } from 'events';
import {FeedStream} from "../../interfaces/feed-stream";

export class PerformanceHorizonXmlStream extends EventEmitter implements FeedStream{
  httpOptions: Object;
  xml: XmlStream;
  req: http.ClientRequest;

  constructor (uri) {
    super();
    this.httpOptions = {
      host: PerformanceHorizon.HOST,
      path: uri,
      port: 80
    };
  }

  pause() {
    if(!this._throwErrorIfNoStream()) {
      return this.xml.pause();
    }
  }

  resume() {
    if(!this._throwErrorIfNoStream()) {
      return this.xml.resume();
    }
  }

  abort() {
    this.req.abort();
    this.xml.pause();
    this.emit('abort');
  }

  _throwErrorIfNoStream() {
    if(!this.xml) {
      this.emit('No xml stream initialized');
      return true;
    }
    return false;
  }

  start() {
    this.req = http.get(this.httpOptions);
    this.req.on('response', response => {
      // Pass the response as UTF-8 to XmlStream
      response.setEncoding('utf8');

      this.xml = new XmlStream(response);

      this.xml.preserve('product');

      this.xml.on('endElement: product', item => this.emit('product', item));

      response.on('end', () => this.emit('end'));
    });

    this.req.on('error', e => {
      this.emit('Problem with request: ' + e.message);
    });
  }
}
