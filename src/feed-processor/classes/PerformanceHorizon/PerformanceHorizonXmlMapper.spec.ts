import 'mocha';
import { expect } from 'chai';
import { Vendor } from '../../enums/vendor';
import { Product } from '../Product/Product';
import { PerformanceHorizonXmlMapper } from './PerformanceHorizonXmlMapper';
import {Category} from "../../enums/category";
import {PerformanceHorizon} from "../../constants/performance-horizon";
import COOLBLUE_CATEGORY_ALIAS_MAP = PerformanceHorizon.COOLBLUE_CATEGORY_ALIAS_MAP;

xdescribe('PerformanceHorizonMapper', () => {

  it('should correctly map an xml product to a listi product', () => {
    const mockXmlProduct = getMockXmlProduct();
    const productFromXml: Product = new PerformanceHorizonXmlMapper(Vendor.COOLBLUE).mapProduct(mockXmlProduct, () => {}, COOLBLUE_CATEGORY_ALIAS_MAP);
    expect(productFromXml.ean).to.equal(8710103404750);
    expect(productFromXml.title).to.equal('Philips FC6148 Kruimelzuiger');
    expect(productFromXml.images).to.deep.equal(['https://image.coolblue.nl/1024x1024/products/643319.jpg']);
    expect(productFromXml.description).to.equal(mockXmlProduct.product_summary.$text);
    expect(productFromXml.categories.length).to.equal(1);
    expect(productFromXml.categories[0]).to.equal(Category.ELEKTRONICA);
    expect(productFromXml.price).to.equal(65.00);
    expect(productFromXml.vendor).to.equal(Vendor.COOLBLUE);
    expect(productFromXml.link).to.equal('https://prf.hn/click/camref:1011l35Bf/creativeref:1101l7234/destination:https://www.coolblue.nl/product/100206/philips-fc6148-kruimelzuiger.html');
  });

});

function getMockXmlProduct() {
  return {
    sku: {
      $children: ['100206'],
      $text: '100206',
      $name: 'sku'
    },
    upc: {
      $children: ['8710103404750'],
      $text: '8710103404750',
      $name: 'upc'
    },
    product_name: {
      $children: ['Philips FC6148 Kruimelzuiger'],
      $text: 'Philips FC6148 Kruimelzuiger',
      $name: 'product_name'
    },
    product_summary: {
      $children: [
        "Klaar kleine schoonmaakklussen in een handomdraai en bespaar gelijktijdig tot 70 procent energie met de Philips FC6148 Kruimelzuiger. Met zijn lichte, krachtige en duurzame 10,8 Volt Lithium-Ion accu zuig je vuil gemakkelijk op. Zo'n lichte accu is wel zo fijn, want zo wordt het nooit te zwaar om de kruimelzuiger vast te houden. En dat is niet het enige voordeel aan de Lithium-Ion accu. Deze accu zorgt namelijk ook voor een aanzienlijke energiebesparing. Want zodra de accu is opgeladen, schakelt het oplaadsysteem zichzelf volledig automatisch uit. Goed voor het milieu en voor je portemonnee! Wanneer het 0,5 liter grote stofreservoir vol is, leeg je deze in een handomdraai. Je verwijdert het mondstuk letterlijk met 1 druk op de knop. Dan is het nog slechts een kwestie van de kruimeldief boven de vuilnisbak houden en je kunt direct weer met de Philips FC6148 Kruimelzuiger aan de slag."
      ],
      $text:
        "Klaar kleine schoonmaakklussen in een handomdraai en bespaar gelijktijdig tot 70 procent energie met de Philips FC6148 Kruimelzuiger. Met zijn lichte, krachtige en duurzame 10,8 Volt Lithium-Ion accu zuig je vuil gemakkelijk op. Zo'n lichte accu is wel zo fijn, want zo wordt het nooit te zwaar om de kruimelzuiger vast te houden. En dat is niet het enige voordeel aan de Lithium-Ion accu. Deze accu zorgt namelijk ook voor een aanzienlijke energiebesparing. Want zodra de accu is opgeladen, schakelt het oplaadsysteem zichzelf volledig automatisch uit. Goed voor het milieu en voor je portemonnee! Wanneer het 0,5 liter grote stofreservoir vol is, leeg je deze in een handomdraai. Je verwijdert het mondstuk letterlijk met 1 druk op de knop. Dan is het nog slechts een kwestie van de kruimeldief boven de vuilnisbak houden en je kunt direct weer met de Philips FC6148 Kruimelzuiger aan de slag.",
      $name: 'product_summary'
    },
    product_features: {
      $children: [
        '10,8 volt Lithium accu  Gebruiksduur: 9 minuten  Oplaadtijd: 8 uur  Met wandhouder  Gewicht: 0,8 kilo'
      ],
      $text:
        '10,8 volt Lithium accu  Gebruiksduur: 9 minuten  Oplaadtijd: 8 uur  Met wandhouder  Gewicht: 0,8 kilo',
      $name: 'product_features'
    },
    brand: {
      $children: ['Philips'],
      $text: 'Philips',
      $name: 'brand'
    },
    currency: {
      $children: ['EUR'],
      $text: 'EUR',
      $name: 'currency'
    },
    price: {
      $children: ['65.00'],
      $text: '65.00',
      $name: 'price'
    },
    condition: {
      $children: ['new'],
      $text: 'new',
      $name: 'condition'
    },
    product_url: {
      $children: [
        'https://prf.hn/click/camref:1011l35Bf/creativeref:1101l7234/destination:https://www.coolblue.nl/product/100206/philips-fc6148-kruimelzuiger.html'
      ],
      $text:
        'https://prf.hn/click/camref:1011l35Bf/creativeref:1101l7234/destination:https://www.coolblue.nl/product/100206/philips-fc6148-kruimelzuiger.html',
      $name: 'product_url'
    },
    image_url: {
      $children: ['https://image.coolblue.nl/1024x1024/products/643319.jpg'],
      $text: 'https://image.coolblue.nl/1024x1024/products/643319.jpg',
      $name: 'image_url'
    },
    free_shipping: {
      $children: ['yes'],
      $text: 'yes',
      $name: 'free_shipping'
    },
    category: {
      $children: ['Toetsenbord en muis sets'],
      $text: 'Toetsenbord en muis sets',
      $name: 'category'
    },
    product_type: {
      $children: ['Kruimeldieven'],
      $text: 'Kruimeldieven',
      $name: 'product_type'
    },
    availability: {
      $children: ['2'],
      $text: '2',
      $name: 'availability'
    },
    shipping_cost: {
      $children: ['0.00'],
      $text: '0.00',
      $name: 'shipping_cost'
    },
    group: {
      $children: ['yes'],
      $text: 'yes',
      $name: 'group'
    },
    reviewsaveragescore: {
      $children: ['8.8'],
      $text: '8.8',
      $name: 'reviewsaveragescore'
    },
    reviewscount: {
      $children: ['312'],
      $text: '312',
      $name: 'reviewscount'
    },
    delivery_time: {
      $children: ['Voor 23.59 uur besteld, morgen in huis.'],
      $text: 'Voor 23.59 uur besteld, morgen in huis.',
      $name: 'delivery_time'
    },
    coolblueskeuze: {
      $children: ['0'],
      $text: '0',
      $name: 'coolblueskeuze'
    },
    $name: 'product'
  };
}
