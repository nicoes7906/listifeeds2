import {Logger} from 'winston';
import {Timer} from "../Timer";
import {Promise} from "core-js";
import {Pool} from "mysql";
import {Runnable} from "../../interfaces/runnable";
import XmlStreamParser from "../XmlStreamParser/XmlStreamParser";
import {RunLimitHandler} from "../RunLimitHandler";
import {ProductStreamHelpers} from "../Product/ProductStreamHelpers";
import {PerformanceHorizonXmlMapper} from "./PerformanceHorizonXmlMapper";
import {ProductBatchWriterMysql} from "../Product/ProductBatchWriterMysql";
import * as pump from 'pump';
import {Vendor} from "../../enums/vendor";
import {S3} from "aws-sdk";
import {PerformanceHorizonTargetFile} from "../../enums/PerformanceHorizonTargetFile";
import {createS3DownloadStream} from "../../lib/S3Download";
import {PerformanceHorizon} from "../../constants/performance-horizon";

export class PerformanceHorizonXmlFetcherStream {
  static createPhXmlFetcher(vendor: Vendor, logTag: string, sqlClient: Pool, s3Client: S3, logger: Logger, fileName: PerformanceHorizonTargetFile): Runnable {
    return {
      run
    };

    function run(): Promise<void> {
      return new Promise((resolve, reject) => {
          const timer = new Timer(logTag, logger.info.bind(logger));

          timer.start();

          const productMapper = new PerformanceHorizonXmlMapper(vendor);

          const phMapper = ProductStreamHelpers.createProductMapStream(
            (product, count) => productMapper.mapProduct(product, logger.error.bind(logger), PerformanceHorizon.COOLBLUE_CATEGORY_ALIAS_MAP),
            logger,
            reject
          );

          const xmlStreamParser = XmlStreamParser.createXmlStreamParser({startElement: 'product'});

          const requestStream = createS3DownloadStream(s3Client, fileName, logger);

          const productWriter = ProductBatchWriterMysql.createProductWriteStream(
            sqlClient,
            logTag,
            logger,
            timer
          );

          const runLimitHandler = RunLimitHandler.createRunLimitHandlerStream(
            requestStream.destroy.bind(requestStream),
            logger.error.bind(logger)
          );

          pump(
            requestStream,
            xmlStreamParser,
            phMapper,
            runLimitHandler,
            productWriter,
            (err) => {
              if (err) {
                if (err.toString().indexOf('premature close') > -1) {
                  logger.info('Pipeline ended prematurely');
                  resolve();
                } else {
                  logger.error(`Pipeline ${logTag} failed.`);
                  reject(err);
                }
              } else {
                logger.info(`Pipeline ${logTag} succeeded.`);
                logger.info(`${logTag} categories: ${productMapper.getAllCategories()}`)
                resolve();
              }
            }
          )
        }
      );
    }
  }
}
