import {ProductMappable} from "../../interfaces/product-mappable";
import {Product} from "../Product/Product";
import {Vendor} from "../../enums/vendor";
import {CategoryHelper} from "../CategoryHelper/CategoryHelper";
import {ProductValidator} from "../ProductValidator/ProductValidator";
import * as _uniq from 'lodash/uniq';
import {CategoryAliasMap} from "../../types/category-alias-map";
import {sanitizePhImage} from "../../lib/SanitizePhImage";
import {ProductLinks} from "../../types/link";

export class PerformanceHorizonXmlMapper implements ProductMappable {
  vendor: Vendor;
  allCategories: string[] = [];

  constructor(vendor) {
    this.vendor = vendor;
  }

  getAllCategories() {
    return _uniq(this.allCategories);
  }

  mapProduct(phXmlObject: any, logError: Function, categoryAliasMap: CategoryAliasMap): Product | null {
    if (
      PerformanceHorizonXmlMapper.isValidProduct(phXmlObject)
    ) {
      this.allCategories.push(phXmlObject?.category);
      const categoriesFromAliasMap = CategoryHelper.getCategoriesFromAliasMap(phXmlObject?.category, categoryAliasMap);

      if(categoriesFromAliasMap.length < 1) {
        return null;
      }

      const {upc, image_url, price, product_url, product_name, category} = phXmlObject;
      const description = PerformanceHorizonXmlMapper.getDescription(phXmlObject);
      const title = product_name;
      const links: ProductLinks = {
        mobileUrl: product_url,
        desktopUrl: product_url
      };
      const ean = upc;
      const images = [sanitizePhImage(image_url)];
      const priceInt = parseFloat(price);
      const categoriesFromTitle = CategoryHelper.getCategoriesFromText(title);
      const categoriesFromDescription = CategoryHelper.getCategoriesFromText(description);
      const categories = _uniq([].concat(categoriesFromAliasMap).concat(categoriesFromTitle).concat(categoriesFromDescription));

      return new Product(
        ean,
        title,
        images,
        description,
        priceInt,
        this.vendor,
        null,
        '',
        links,
        categories,
        -1,
        -1,
        -1,
        null,
        null
      );
    }

    return null;
  }

  static isValidProduct(product) {
    return product &&
      product.upc &&
      product.product_name &&
      ProductValidator.isValidEAN(product.upc) &&
      ProductValidator.isValidTitle(product.product_name);
  }

  static getDescription(phXmlObject) {
    const {product_summary, product_name} = phXmlObject;
    if (product_summary) {
      return product_summary;
    } else if (product_name) {
      return product_name;
    } else {
      return '';
    }
  }
}
