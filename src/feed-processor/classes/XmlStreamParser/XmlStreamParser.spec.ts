import 'mocha';
import {expect} from 'chai';
import * as sinon from 'sinon';
import * as _get from 'lodash/get';
import XmlStreamParser from "./XmlStreamParser";

describe('XmlStreamParser', () => {

  it('should write a product to the transform stream when this has completed as single text entries', () => {
    const mockName1 = 'Midj Eden H65 Barkruk';
    const mockDescription1 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget dolor nisi.';
    const mockName2 = 'Hallo';
    const mockName3 = 'Jooo';
    const mockXmlReads = getMockXmlReads(mockName1, mockDescription1, mockName2, mockName3);
    const parser = XmlStreamParser.createXmlStreamParser({collections: ['category']});
    const mockCallback = sinon.spy();
    mockXmlReads.forEach((value) => {
      parser.write(value, 'utf-8');
    });
    const read1 = parser.read();
    const read2 = parser.read();
    const read3 = parser.read();
    parser.read();
    const read5 = parser.read();
    assertProductValueEqual(read1, 'name', mockName1);
    assertProductValueEqual(read1, 'description', mockDescription1);
    assertProductValueEqual(read1, 'sellerType', 'P');
    assertProductValueEqual(read1, 'sellerRating', undefined);
    assertProductValueDeepEqual(read1, 'categories', ['Koken & tafelen', 'Koken & tafelen 2', 'Koken & tafelen 3']);
    assertProductValueEqual(read2, 'name', mockName2);
    assertProductValueEqual(read3, 'name', mockName3);
    expect(read5).to.equal(null);
  });
});

function assertProductValueEqual(product, key, expectedValue) {
  expect(product).to.be.an('object');
  expect(_get(product, key)).to.equal(expectedValue);
}

function assertProductValueDeepEqual(product, key, expectedValue) {
  expect(product).to.be.an('object');
  expect(_get(product, key)).to.deep.equal(expectedValue);
}

function getMockXmlReads(mockName1, mockDescription1, mockName2, mockName3) {
  return [
    getXmlOutput1(mockName1, mockDescription1, mockName2),
    getXmlOutput2(mockName3)
  ];
}

function getXmlOutput1(mockName1, mockDescription1, mockName2) {
  return '<?xml version="1.0" encoding="utf-8"?>' +
    '<!-- Generated on 12/16/18 at 06:43:53 (http://pf.tradetracker.net) -->\n' +
    '<products>\n' +
    '<product ID="247771">\n' +
    '<name>' + mockName1 + '</name>\n' +
    '<price currency="EUR">199.00</price>\n' +
    '<URL>http://tc.tradetracker.net/?c=2625&amp;m=584064&amp;a=287017&amp;u=https%3A%2F%2Fwww.fonq.nl%2Fproduct%2Fle-creuset-signature-ronde-braad_stoofpan-24-cm%2F277450%2F%3Fchannel_code%3D34%26s2m_product_id%3D247771%26utm_source%3Dtt%26osadcampaign%3Dtradetracker%26utm_medium%3Dcps</URL>\n' +
    '<images>\n' +
    '<image>https://mb.fcdn.nl/square340/1462221/le-creuset-signature-ronde-braad_stoofpan-24-cm.jpg</image>\n' +
    '</images>\n' +
    '<description><![CDATA[' + mockDescription1 + ']]></description>\n' +
    '<sellerType>P</sellerType>\n' +
    '<sellerRating></sellerRating>\n' +
    '<categories>\n' +
    '<category path="Koken &amp; tafelen">Koken &amp; tafelen</category>\n' +
    '<category path="Koken &amp; tafelen">Koken &amp; tafelen 2</category>\n' +
    '<category path="Koken &amp; tafelen">Koken &amp; tafelen 3</category>\n' +
    '</categories>\n' +
    '<properties>\n' +
    '<property name="brand">\n' +
    '<value>Le Creuset</value>\n' +
    '</property>\n' +
    '<property name="EAN">\n' +
    '<value>0024147260820</value>\n' +
    '</property>\n' +
    '<property name="fromPrice">\n' +
    '<value>255.00</value>\n' +
    '</property>\n' +
    '<property name="deliveryCosts">\n' +
    '<value>0.00</value>\n' +
    '</property>\n' +
    '<property name="subcategories">\n' +
    '<value>Pannen</value>\n' +
    '</property>\n' +
    '<property name="color">\n' +
    '<value>Blauw</value>\n' +
    '</property>\n' +
    '<property name="material">\n' +
    '<value>Gietijzer</value>\n' +
    '</property>\n' +
    '<property name="currency">\n' +
    '<value>EUR</value>\n' +
    '</property>\n' +
    '<property name="length">\n' +
    '<value></value>\n' +
    '</property>\n' +
    '<property name="width">\n' +
    '<value></value>\n' +
    '</property>\n' +
    '<property name="height">\n' +
    '<value></value>\n' +
    '</property>\n' +
    '<property name="deliveryTime">\n' +
    '<value>Voor 23:00 besteld, morgen in huis</value>\n' +
    '</property>\n' +
    '<property name="stock">\n' +
    '<value>yes</value>\n' +
    '</property>\n' +
    '<property name="categoryPath">\n' +
    '<value>Koken &amp; tafelen&gt;Pannen&gt;Braadpan</value>\n' +
    '</property>\n' +
    '<property name="subsubcategories">\n' +
    '<value>Braadpan</value>\n' +
    '</property>\n' +
    '</properties>\n' +
    '<variations/>\n' +
    '</product>\n' +
    '<product ID="247772">\n' +
    '<name>' + mockName2 + '</name>\n' +
    '<price currency="EUR">249.00</price>\n' +
    '<URL>http://tc.tradetracker.net/?c=2625&amp;m=584064&amp;a=287017&amp;u=https%3A%2F%2Fwww.fonq.nl%2Fproduct%2Fle-creuset-signature-ronde-braad_stoofpan-26-cm%2F277451%2F%3Fchannel_code%3D34%26s2m_product_id%3D247772%26utm_source%3Dtt%26osadcampaign%3Dtradetracker%26utm_medium%3Dcps</URL>\n' +
    '<images>\n' +
    '<image>https://mb.fcdn.nl/square340/1462222/le-creuset-signature-ronde-braad_stoofpan-26-cm.jpg</image>\n' +
    '</images>\n' +
    '<description><![CDATA[De Signature braadpan van Le Creuset is onmisbaar voor het bereiden van heerlijke stoof- en suddergerechten! De pan heeft een duurzame kras- en vlekbestendige email coating. De ultragladde bodem zorgt voor een optimale en zeer snelle warmteverdeling. Bestel Le Creuset Signature Ronde Braad/Stoofpan Ø 26 cm online bij fonQ. Alle Le Creuset producten uit voorraad leverbaar. Voor 23:00 besteld, morgen in huis.]]></description>\n' +
    '<categories>\n' +
    '<category path="Koken &amp; tafelen">Koken &amp; tafelen</category>\n' +
    '</categories>\n' +
    '<properties>\n' +
    '<property name="brand">\n' +
    '<value>Le Creuset</value>\n' +
    '</property>\n' +
    '<property name="EAN">\n' +
    '<value>0024147260967</value>\n' +
    '</property>\n' +
    '<property name="fromPrice">\n' +
    '<value>289.00</value>\n' +
    '</property>\n' +
    '<property name="deliveryCosts">\n' +
    '<value>0.00</value>\n' +
    '</property>\n' +
    '<property name="subcategories">\n' +
    '<value>Pannen</value>\n' +
    '</property>\n' +
    '<property name="color">\n' +
    '<value>Blauw</value>\n' +
    '</property>\n' +
    '<property name="material">\n' +
    '<value>Gietijzer</value>\n' +
    '</property>\n' +
    '<property name="currency">\n' +
    '<value>EUR</value>\n' +
    '</property>\n' +
    '<property name="length">\n' +
    '<value></value>\n' +
    '</property>\n' +
    '<property name="width">\n' +
    '<value></value>\n' +
    '</property>\n' +
    '<property name="height">\n' +
    '<value></value>\n' +
    '</property>\n' +
    '<property name="deliveryTime">\n' +
    '<v';
}

function getXmlOutput2(mockName1) {
    return 'alue>Voor 23:00 besteld, morgen in huis</value>\n' +
      '</property>\n' +
      '<property name="stock">\n' +
      '<value>yes</value>\n' +
      '</property>\n' +
      '<property name="categoryPath">\n' +
      '<value>Koken &amp; tafelen&gt;Pannen&gt;Braadpan</value>\n' +
      '</property>\n' +
      '<property name="subsubcategories">\n' +
      '<value>Braadpan</value>\n' +
      '</property>\n' +
      '</properties>\n' +
      '<variations/>\n' +
      '</product>\n' +
      '<product ID="247773">\n' +
      '<name>' + mockName1 + '</name>\n' +
      '<price currency="EUR">199.00</price>\n' +
      '<URL>http://tc.tradetracker.net/?c=2625&amp;m=584064&amp;a=287017&amp;u=https%3A%2F%2Fwww.fonq.nl%2Fproduct%2Fle-creuset-signature-ovale-braad_stoofpan-27-cm%2F278070%2F%3Fchannel_code%3D34%26s2m_product_id%3D247773%26utm_source%3Dtt%26osadcampaign%3Dtradetracker%26utm_medium%3Dcps</URL>\n' +
      '<images>\n' +
      '<image>https://mb.fcdn.nl/square340/1462223/le-creuset-signature-ovale-braad_stoofpan-27-cm.jpg</image>\n' +
      '</images>\n' +
      '<description><![CDATA[Een alleskunner! Deze pan van Le Creuset is naast braadpan te gebruiken als hapjespan of ovenschaal. Dit gaat niet ten koste van de smaak: door de emaillaag neemt het ijzer geen geur of smaak op en blijft iedere maaltijd de unieke smaak behouden. Bestel Le Creuset Signature Ovale Braad/Stoofpan Ø 27 cm online bij fonQ. Alle Le Creuset producten uit voorraad leverbaar. Voor 23:00 besteld, morgen in huis.]]></description>\n' +
      '<categories>\n' +
      '<category path="Koken &amp; tafelen">Koken &amp; tafelen</category>\n' +
      '</categories>\n' +
      '<properties>\n' +
      '<property name="brand">\n' +
      '<value>Le Creuset</value>\n' +
      '</property>\n' +
      '<property name="EAN">\n' +
      '<value>0024147261841</value>\n' +
      '</property>\n' +
      '<property name="fromPrice">\n' +
      '<value>255.00</value>\n' +
      '</property>\n' +
      '<property name="deliveryCosts">\n' +
      '<value>0.00</value>\n' +
      '</property>\n' +
      '<property name="subcategories">\n' +
      '<value>Pannen</value>\n' +
      '</property>\n' +
      '<property name="color">\n' +
      '<value>Blauw</value>\n' +
      '</property>\n' +
      '<property name="material">\n' +
      '<value>Gietijzer</value>\n' +
      '</property>\n' +
      '<property name="currency">\n' +
      '<value>EUR</value>\n' +
      '</property>\n' +
      '<property name="length">\n' +
      '<value></value>\n' +
      '</property>\n' +
      '<property name="width">\n' +
      '<value></value>\n' +
      '</property>\n' +
      '<property name="height">\n' +
      '<value></value>\n' +
      '</property>\n' +
      '<property name="deliveryTime">\n' +
      '<value>Voor 23:00 besteld, morgen in huis</value>\n' +
      '</property>\n' +
      '<property name="stock">\n' +
      '<value>yes</value>\n' +
      '</property>\n' +
      '<property name="categoryPath">\n' +
      '<value>Koken &amp; tafelen&gt;Pannen&gt;Braadpan</value>\n' +
      '</property>\n' +
      '<property name="subsubcategories">\n' +
      '<value>Braadpan</value>\n' +
      '</property>\n' +
      '</properties>\n' +
      '<variations/>\n' +
      '</product>\n' +
      '<product ID="247787">\n' +
      '<name>Stelton Original Kurkentrekker</name>\n' +
      '<price currency="EUR">39.99</price>\n' +
      '<URL>http://tc.tradetracker.net/?c=2625&amp;m=584064&amp;a=287017&amp;u=https%3A%2F%2Fwww.fonq.nl%2Fproduct%2Fstelton-original-kurkentrekker%2F278079%2F%3Fchannel_code%3D34%26s2m_product_id%3D247787%26utm_source%3Dtt%26osadcampaign%3Dtradetracker%26utm_medium%3Dcps</URL>\n' +
      '<images>\n' +
      '<image>https://mb.fcdn.nl/square340/1341455/stelton-original-kurkentrekker.jpg</image>\n' +
      '</images>\n' +
      '<description><![CDATA[Plop! Met de Original kurkentrekker van Stelton maak je in een mum van tijd een heerlijke fles wijn open. De stalen kurkentrekker is stevig en heeft een simpele maar stijlvolle design, wat hem een unieke uitstraling geeft. Bestel Stelton Original Kurkentrekker online bij fonQ. Alle Stelton producten uit voorraad leverbaar. Voor 23:00 besteld, morgen in huis.]]></description>\n' +
      '<categories>\n' +
      '<category path="Koken &amp; tafelen">Koken &amp; tafelen</category>\n' +
      '</categories>\n' +
      '<properties>\n' +
      '<property name="brand">\n' +
      '<value>Stelton</value>\n' +
      '</property>\n' +
      '<property name="EAN">\n' +
      '<value>5709846000780</value>\n' +
      '</property>\n' +
      '<property name="fromPrice">\n' +
      '<value>44.95</value>\n' +
      '</property>\n' +
      '<property name="deliveryCosts">\n' +
      '<value>0.00</value>\n' +
      '</property>\n' +
      '<property name="subcategories">\n' +
      '<value>Wijn &amp; bar</value>\n' +
      '</property>\n' +
      '<property name="color">\n' +
      '<value>Zilver</value>\n' +
      '</property>\n' +
      '<property name="material">\n' +
      '<value>Staal</value>\n' +
      '</property>\n' +
      '<property name="currency">\n' +
      '<value>EUR</value>\n' +
      '</property>\n' +
      '<property name="length">\n' +
      '<value></value>\n' +
      '</property>' +
      '</properties>\n' +
      '</product>\n' +
      '</products>';
}


