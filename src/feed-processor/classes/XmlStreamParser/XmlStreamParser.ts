import * as stream from "stream";
import * as Sax from 'sax';
import * as _set from 'lodash/set';
import * as _get from 'lodash/get';

interface XmlStreamParserOptions {
  collections?: string[],
  startElement?: string
}

const defaultXmlStreamParserOptions: XmlStreamParserOptions = {
  collections: [],
  startElement: 'product'
};

class XmlStreamParser {
  static createXmlStreamParser(options: XmlStreamParserOptions) {
    const saxStream: stream.Duplex = Sax.createStream(true, {});

    const xmlTransformer = new stream.Transform({
      objectMode: true,
      transform(chunk: any, enc, callback) {
        saxStream.write(chunk);
        callback();
      }
    });

    const onError = (e) => xmlTransformer.emit("error", e);
    const onEnd = () => xmlTransformer.emit("end");

    const {onStartElement, onTextElement, onEndElement} = getParserEvents(
      {
        ...defaultXmlStreamParserOptions,
        ...options
      },
      xmlTransformer.push.bind(xmlTransformer),
      onError,
      onEnd
    );

    saxStream.on('opentag', onStartElement);
    saxStream.on('text', onTextElement);
    saxStream.on('cdata', onTextElement);
    saxStream.on('closetag', onEndElement);
    saxStream.on('error', onError);

    return xmlTransformer;
  }
}

export default XmlStreamParser;

function getParserEvents(
  options: XmlStreamParserOptions,
  pushToStream: Function,
  onError: Function,
  onEnd: Function
) {
  const parserState = {
    level: 0,
    startParseLevel: -1,
    stack: []
  };

  return {
    onStartElement,
    onTextElement,
    onEndElement,
    onError
  };

  function onStartElement(node) {
    const {name, attributes} = node;
    addParseLevel();

    if (name === options.startElement) {
      parserState.startParseLevel = parserState.level;
    } else if (isInElement()) {
      const finalName = attributes.name ? attributes.name : name;
      parserState.stack.push({level: parserState.level, isOpening: true, name: finalName, attributes});
    }
  }

  function onTextElement(text) {
    if (isInElement() && !isNullOrWhitespace(text)) {
      parserState.stack.push({level: getParseLevelDepth(), isText: true, value: text});
    }
  }

  function isNullOrWhitespace( input ) {
    return !input || !input.trim();
  }

  function onEndElement(name) {
    if (isInElement()) {
      parserState.stack.push({level: parserState.level, isClosing: true, name});
    }

    if (parserState.level === getStartParseLevel()) {
      const objectToPush = reduceStackToObject();
      pushToStream(objectToPush);
      parserState.stack = [];
    }
    if (parserState.level === 0) {
      onEnd();
    }

    removeLastParseLevel();
  }

  function reduceStackToObject() {
    let lastPath = '';
    let inArray = false;
    return parserState.stack.reduce((acc, stackEntry) => {
      if (stackEntry.isOpening) {
        if (options.collections.indexOf(stackEntry.name) > -1) {
          inArray = true;
        } else {
          lastPath = lastPath.length > 0 ? lastPath + '.' + stackEntry.name : stackEntry.name;
        }
      }
      if (stackEntry.isClosing) {
        if (inArray) {
          inArray = false;
        } else {
          lastPath = lastPath.substring(0, lastPath.lastIndexOf('.'));
        }
      }
      if (stackEntry.isText) {
        if (inArray) {
          const lastArrayValue = _get(acc, lastPath) || [];
          _set(acc, lastPath, [...lastArrayValue, stackEntry.value]);
        } else {
          _set(acc, lastPath, stackEntry.value)
        }
      }
      return acc;
    }, {})
  }

  function isInElement() {
    return parserState.startParseLevel > 0 && getParseLevelDepth() > parserState.startParseLevel;
  }

  function addParseLevel() {
    parserState.level++;
    return getParseLevelDepth();
  }

  function removeLastParseLevel() {
    parserState.level--;
    return getParseLevelDepth();
  }

  function getStartParseLevel() {
    return parserState.startParseLevel;
  }

  function getParseLevelDepth() {
    return parserState.level;
  }
}
