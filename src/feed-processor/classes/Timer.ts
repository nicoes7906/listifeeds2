import {differenceInSeconds, format} from "date-fns";

export class Timer {
  logFn: Function;
  logTag: String;
  startDate: number;

  constructor(logTag: String, logFn: Function) {
    this.logFn = logFn;
    this.logTag = logTag;
  }

  start() {
    this.startDate = Date.now();
    this.logFn(`Starting logging ${this.logTag} at ${Timer._formatDate(this.startDate)}`)
  }

  logDifference() {
    this.logFn(`Difference of ${this.logTag} is +${this.getSecondsSinceStart()}`)
  }

  getSecondsSinceStart() {
    return differenceInSeconds(Date.now(), this.startDate)
  }

  static _formatDate(rawDate: number) {
    return format(rawDate, 'YYYY-MM-DDTHH:mm:ss')
  }
}
