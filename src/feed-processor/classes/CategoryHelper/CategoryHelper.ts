import * as _ from 'lodash';
import {CategoryAliasMap} from "../../types/category-alias-map";
import {Category} from "../../enums/category";

const CATEGORIES_BY_WORDS = {
  'Nerf': Category.NERF,
  'Lego': Category.LEGO,
  'Playmobil': Category.PLAYMOBIL,
  'VTech': Category.VTECH,
  'Playstation': Category.GAMES_CONSOLES,
  'Xbox': Category.GAMES_CONSOLES,
  'Princess': Category.PRINCESS,
  'Frozen': Category.FROZEN,
  'Paw patrol': Category.PAW_PATROL,
  'Star wars': Category.STARWARS,
  'Cars': Category.CARS,
  'Brandweerman Sam': Category.BRANDWEERMAN_SAM,
  'knutsel': Category.KNUTSELEN,
  'l.o.l': Category.LOL,
  'Pokemon': Category.POKEMON,
  'Baby born': Category.BABY_BORN,
  'Bing': Category.BING,
  'Fisher Price': Category.FISHER_PRICE,
  'Barbie': Category.BARBIE
};

export class CategoryHelper {
  static getCategoriesFromAliasMap(categoryString, aliasMap: CategoryAliasMap): Array<Category> {
    if(_.isEmpty(categoryString)) return [];
    return aliasMap
      .filter(categoryGroup => categoryGroup.aliases.map(alias => alias.toLowerCase()).indexOf(categoryString.toLowerCase()) > -1)
      .map(categoryGroup => categoryGroup.category);
  }

  static getCategoriesFromText(textToSearch): Array<Category> {
    if(_.isEmpty(textToSearch)) return [];
    const textToLower = textToSearch.toLowerCase();
    const foundCategories = Object.keys(CATEGORIES_BY_WORDS)
      .filter(key => textToLower.indexOf(key.toLowerCase()) > -1)
      .map(key => CATEGORIES_BY_WORDS[key]);
    return _.uniq(foundCategories);
  }
}
