import { expect } from 'chai';
import {CategoryHelper} from "./CategoryHelper";
import {Category} from "../../enums/category";

describe('Category Helper', () => {

  describe('Categories from alias map', () => {
    it('should get a category by an alias map', () => {
      const aliasMap = [{category: Category.BABY_PEUTER, aliases: ['hoi']}];
      const result = CategoryHelper.getCategoriesFromAliasMap('hoi', aliasMap);
      expect(result.length).to.equal(1);
      expect(result[0]).to.equal(Category.BABY_PEUTER);
    });
    it('should not get a category when it\'s not in the alias map', () => {
      const aliasMap = [{category: Category.BABY_PEUTER, aliases: ['hoi']}];
      const result = CategoryHelper.getCategoriesFromAliasMap('hoi2', aliasMap);
      expect(result.length).to.equal(0);
    });
    it('should get a category when it matches on lower casing', () => {
      const aliasMap = [{category: Category.BABY_PEUTER, aliases: ['hoi']}];
      const result = CategoryHelper.getCategoriesFromAliasMap('HoI', aliasMap);
      expect(result.length).to.equal(1);
    });
    it('should return an empty array if the categpry string is empty', () => {
      const aliasMap = [{category: Category.BABY_PEUTER, aliases: ['hoi']}];
      const result = CategoryHelper.getCategoriesFromAliasMap('', aliasMap);
      expect(result.length).to.equal(0);
    });
  });

  describe('Categories by words', () => {
    it('should derive categories from product descriptions', () => {
      const text = 'Hallo Nerf, hallo Xbox, vandaag gaan we de playstation aanzetten. Een beetje knutselen. Je kent het.';
      const result = CategoryHelper.getCategoriesFromText(text);
      expect(result.length).to.equal(3);
      expect(result[0]).to.equal(Category.NERF);
      expect(result[1]).to.equal(Category.GAMES_CONSOLES);
      expect(result[2]).to.equal(Category.KNUTSELEN);
    })
  });

});
