import * as winston from "winston";
import * as Transport from "winston-transport";

export class LoggerFactory {
  public static createLogger (level) {
    // TODO: make volume in docker compose for log files
    const transports: Transport[] = [
      new winston.transports.File({ filename: 'error.log', level: 'error' }),
      new winston.transports.File({ filename: 'combined.log' })
    ];

    if(process.env.IS_LAMBDA) {
      transports.push(new AWSTransport());
    } else {
      transports.push(new winston.transports.Console());
    }

    return winston.createLogger({
      level,
      format: winston.format.simple(),
      transports
    });
  }
}

class AWSTransport extends Transport {
  log({ message }, cb) {
    setTimeout(() => {
      console.log(`aws: ${message}`);
      cb(null);
    }, 1);
  }
}
