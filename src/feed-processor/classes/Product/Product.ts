import { Vendor } from '../../enums/vendor';
import { ProductInfo } from '../../interfaces/product-info';
import {Category} from "../../enums/category";
import {ProductLinks} from "../../types/link";

export class Product implements ProductInfo {
  public id : string;
  public ean: number;
  public title : string;
  public images: Array<string>;
  public description: string;
  public price: number;
  public vendor: Vendor;
  public vendorRating: number;
  public link: string;
  public links: ProductLinks;
  public categories: Array<Category>;
  public fromAge: number;
  public toAge: number;
  public feedIndex: number;
  public video: string;
  public boostCategories: string

  constructor(
    ean: number,
    title : string,
    images: Array<string>,
    description: string,
    price: number,
    vendor: Vendor,
    vendorRating: number,
    link: string,
    links: ProductLinks,
    categories: Array<Category>,
    fromAge: number,
    toAge: number,
    feedIndex: number,
    video: string,
    boostCategories: string
  ) {
    this.id = ean + '_' + vendor;
    this.ean = +ean;
    this.title = title;
    this.images = images;
    this.description = description;
    this.price = +price;
    this.vendor = vendor;
    this.vendorRating = vendorRating > 0 ? vendorRating : -1;
    this.link = link;
    this.links = links;
    this.categories = categories || [];
    this.fromAge = fromAge;
    this.toAge = toAge;
    this.feedIndex = feedIndex;
    this.video = video
    this.boostCategories = boostCategories
  }

  public getValueOf(propName): any {
    return this[propName];
  }
}
