import { escape } from 'mysql2';
import { Promise } from 'core-js';
import { BaseEscapedProduct } from '../../interfaces/base-escaped-product';

export class ProductDatabaseMysqlHelper {
  maxBatchProducts: number;
  tableName: String;
  fields: Array<string>;
  todayEscaped: string;
  queryFn: Function;
  queryStack: Array<Array<any>>;
  stringParts: {insertFields: string, updateValues: string};

  constructor(tableName: String, fields: Array<string>, today: Date, queryFn: Function, maxBatchProducts?: number) {
    this.maxBatchProducts = maxBatchProducts;
    this.todayEscaped = escape(today);
    this.tableName = tableName;
    this.fields = fields;
    this.queryFn = queryFn;
    this.queryStack = [];

    this.stringParts = {
      insertFields: this.fields.reduce((acc, fieldKey) => {
        return acc + `${fieldKey}, `;
      }, '') + 'created_date, modified_date',
      updateValues: this.fields.reduce((acc, fieldKey) => {
        return acc + `${fieldKey}=VALUES(${fieldKey}),`;
      }, '') + `modified_date=${this.todayEscaped}`
    }
  }

  insertOrUpdateAsBatch(escapedProduct: BaseEscapedProduct): Promise<any> {
    if(this.maxBatchProducts < 1) {
      return Promise.reject('Max batch products should be set');
    }
    const createdDate = this.todayEscaped;
    const modifiedDate = this.todayEscaped;
    const stackEntry = this.fields.reduce((acc, fieldKey) => {
      return [...acc, escapedProduct[fieldKey]];
    }, []);
    this.queryStack.push([...stackEntry, createdDate, modifiedDate]);
    if(this.queryStack.length >= this.maxBatchProducts) {
      return this.commitBatch();
    } else {
      return Promise.resolve();
    }
  }

  flushBatch(): Promise<string> {
    if(this.queryStack.length < 1) {
      return Promise.resolve('Nothing to flush');
    } else {
      return this.commitBatch();
    }
  }

  commitBatch(): Promise<string> {
    const stackToCommit = [...this.queryStack];
    this.queryStack = [];
    return new Promise((resolve, reject) => {
      try {
        const query = this._getInsertOrUpdateMultipleQuery(stackToCommit);

        this.queryFn(query,
          (err) => {
            if (err) {
              console.log(stackToCommit);
              return reject('Error upserting multiple products: ' + err.message);
            }
            resolve(`Batch upserted ${stackToCommit.length} products to ${this.tableName}`);
          });
      } catch (e) {
        reject(e);
      }
    });
  }

  _getInsertOrUpdateMultipleQuery(values: string[][]): string {
    const valuesToString =values.reduce((acc, value) => {
      const valueToString = value.reduce((acc2, value2) => acc2 + `${value2},`, '');
      const valueToStringWithoutTrailingComma = valueToString.substring(0, valueToString.length - 1);
      return acc + `(${valueToStringWithoutTrailingComma}),`
    },`INSERT INTO ${this.tableName} (${this.stringParts.insertFields}) VALUES `);
    const valuesToStringWithoutTrailingComma = valuesToString.substring(0, valuesToString.length - 1);
    return valuesToStringWithoutTrailingComma + ` ON DUPLICATE KEY UPDATE ${this.stringParts.updateValues}`;
  }

  _getInsertOrUpdateQuery(valuesString): string {
    return `INSERT INTO ${this.tableName} (${this.stringParts.insertFields}) VALUES (${valuesString}) ON DUPLICATE KEY UPDATE ${this.stringParts.updateValues}`;
  }

  insertOrUpdate(escapedProduct: BaseEscapedProduct): Promise<String> {
    const createdDate = this.todayEscaped;
    const modifiedDate = this.todayEscaped;

    const insertValuesString = this.fields.reduce((acc, fieldKey) => {
      return acc + `${escapedProduct[fieldKey]}, `;
    }, '') + `${createdDate}, ${modifiedDate}`;

    return new Promise((resolve, reject) => {
      try {
        const query = this._getInsertOrUpdateQuery(insertValuesString);

        this.queryFn(query,
          (err, results) => {
            if (err) return reject('Error upserting product: ' + err.message);
            resolve(`Upsert product to ${this.tableName} with EAN ${escapedProduct.ean}`);
          });
      } catch (e) {
        reject(e);
      }
    });
  }
}
