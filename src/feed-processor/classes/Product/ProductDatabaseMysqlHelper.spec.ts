import 'mocha';
import {expect} from 'chai';
import * as sinon from 'sinon';
import {ProductDatabaseMysqlHelper} from './ProductDatabaseMysqlHelper';

describe('ProductDatabaseMysqlHelper', () => {
  const product: any = {
    ean: '\'123\'',
    images: '\'img1#img2\'',
    description: 'hoi',
    price: '\'1\'',
    vendor: '\'barts\''
  };
  const product2: any = {
    ean: '\'456\'',
    images: '\'img1#img2#img3\'',
    description: 'hoi2',
    price: '\'2\'',
    vendor: '\'inter\''
  };
  const randomDate = new Date(2018, 3, 3);

  describe('on insert or update', () => {
    it('should correctly format a query string that inserts or updates when a product already exists', () => {
      const querySpy = sinon.spy();
      const mockQueryFn = (query, cb) => {
        querySpy(query);
        cb();
      };
      const writeHelper = new ProductDatabaseMysqlHelper('products', ['ean', 'images'], randomDate, mockQueryFn);
      return writeHelper.insertOrUpdate(product)
        .then(() => {
          sinon.assert.called(querySpy);
          expect(querySpy.firstCall.args[0].toString()).to.be.equal('INSERT INTO products (ean, images, created_date, modified_date) VALUES (\'123\', \'img1#img2\', \'2018-04-03 00:00:00.000\', \'2018-04-03 00:00:00.000\') ON DUPLICATE KEY UPDATE ean=VALUES(ean),images=VALUES(images),modified_date=\'2018-04-03 00:00:00.000\'');
        });
    })
  });

  describe('when committing in batches', () => {
    let writeHelper, querySpy;

    beforeEach(() => {
      querySpy = sinon.spy();
      const mockQueryFn = (query, cb) => {
        querySpy(query);
        cb();
      };
      writeHelper = new ProductDatabaseMysqlHelper('products', ['ean', 'images', 'description', 'price', 'vendor'], randomDate, mockQueryFn, 2);
    });

    it('should insert or update when the max number of batch products is added to the write helper', () => {
      return writeHelper.insertOrUpdateAsBatch(product)
        .then(() => writeHelper.insertOrUpdateAsBatch(product2))
        .then(() => {
          sinon.assert.calledOnce(querySpy);
          expect(querySpy.firstCall.args[0].toString()).to.be.equal("INSERT INTO products (ean, images, description, price, vendor, created_date, modified_date) VALUES ('123','img1#img2',hoi,'1','barts','2018-04-03 00:00:00.000','2018-04-03 00:00:00.000'),('456','img1#img2#img3',hoi2,'2','inter','2018-04-03 00:00:00.000','2018-04-03 00:00:00.000') ON DUPLICATE KEY UPDATE ean=VALUES(ean),images=VALUES(images),description=VALUES(description),price=VALUES(price),vendor=VALUES(vendor),modified_date='2018-04-03 00:00:00.000'");
        })
        .then(() => writeHelper.insertOrUpdateAsBatch(product))
        .then(() => {
          sinon.assert.calledOnce(querySpy);
        });
    });

    it('should flush remaining products in the stack when called', () => {
      return writeHelper.insertOrUpdateAsBatch(product)
        .then(() => {
          sinon.assert.notCalled(querySpy);
        })
        .then(() => writeHelper.flushBatch())
        .then(() => {
          sinon.assert.calledOnce(querySpy);
          expect(querySpy.firstCall.args[0].toString()).to.be.equal("INSERT INTO products (ean, images, description, price, vendor, created_date, modified_date) VALUES ('123','img1#img2',hoi,'1','barts','2018-04-03 00:00:00.000','2018-04-03 00:00:00.000') ON DUPLICATE KEY UPDATE ean=VALUES(ean),images=VALUES(images),description=VALUES(description),price=VALUES(price),vendor=VALUES(vendor),modified_date='2018-04-03 00:00:00.000'");
        })
    });
  })

});
