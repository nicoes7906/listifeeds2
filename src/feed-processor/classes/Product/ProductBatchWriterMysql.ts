import {Writable} from 'stream';
import {Pool} from "mysql";
import {ProductDatabaseMysqlHelper} from "./ProductDatabaseMysqlHelper";
import {EscapedProduct} from "./EscapedProduct";
import {Timer} from "../Timer";
import {Logger} from "winston";
import {EscapedAggregatedProduct} from "../AggregatedProduct/EscapedAggregatedProduct";

require('dotenv').load();

export class ProductBatchWriterMysql {

  static MAX_BATCH_PRODUCTS = 500;

  static createAggregatedProductWriteStream(sqlClient: Pool, tag: string, logger: Logger, timer: Timer): Writable {
    const dbHelper = new ProductDatabaseMysqlHelper(
      process.env.AGGREGATED_PRODUCTS_TABLE,
      ['ean', 'title', 'images', 'description', 'price', 'vendors', 'number_of_prices', 'ranking', 'categories', 'toAge', 'fromAge', 'video', 'boostCategories'],
      new Date(Date.now()),
      sqlClient.query.bind(sqlClient),
      ProductBatchWriterMysql.MAX_BATCH_PRODUCTS
    );

    return createWritable(dbHelper, tag, logger, timer, (chunk) =>  new EscapedAggregatedProduct(chunk));
  }

  static createProductWriteStream(sqlClient: Pool, tag: string, logger: Logger, timer: Timer): Writable {
    const dbHelper = new ProductDatabaseMysqlHelper(
      process.env.MYSQL_PRODUCTS_TABLE,
      ['ean', 'title', 'images', 'description', 'price', 'vendor', 'vendor_rank', 'link', 'links', 'categories', 'fromAge', 'toAge', 'feedIndex', 'video', 'boostCategories'],
      new Date(Date.now()),
      sqlClient.query.bind(sqlClient),
      ProductBatchWriterMysql.MAX_BATCH_PRODUCTS
    );

    return createWritable(dbHelper, tag, logger, timer, (chunk) =>  new EscapedProduct(chunk));
  }
}

function createWritable(dbHelper, tag: string, logger: Logger, timer: Timer, escapeFn: Function) {
  let saveIndex = 0;

  return new Writable({
    objectMode: true,
    write(chunk, encoding, callback) {

      const escapedProduct = escapeFn(chunk);

      dbHelper.insertOrUpdateAsBatch(escapedProduct)
        .then(msg => {
          saveIndex++;
          if(msg) logInfo(msg);
          callback(null)
        })
        .catch(e => {
          logError(e);
          callback(e);
        });
    },
    final(cb) {
      return dbHelper.flushBatch()
        .then(() => {
          logInfo('Flushed final batch');
          cb();
        })
        .catch((e) => {
          logError(e)
        });
    }
  });

  function logError(e) {
    logger.error(`${tag} : ${e}`);
  }

  function logInfo(msg) {
    logger.info(`${tag} : ${saveIndex} : +${timer.getSecondsSinceStart()} : ${msg}`);
  }
}
