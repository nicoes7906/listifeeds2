import { Product } from './Product';
import { escape } from 'mysql2';
import { BaseEscapedProduct } from '../../interfaces/base-escaped-product';

export class EscapedProduct implements BaseEscapedProduct {
  public id : string;
  public ean: String;
  public title: String;
  public images: String;
  public description: String;
  public price: String;
  public vendor: String;
  public vendor_rank: String;
  public link: String;
  public links: String;
  public categories: String;
  public fromAge: String;
  public toAge: String;
  public feedIndex: String;
  public video: String;
  public boostCategories: String;

  constructor(product: Product) {
    this.id = escape(product.id);
    this.ean = escape(product.ean);
    this.title = escape(product.title);
    this.images = escape(product.images.join('#'));
    this.description = escape(product.description);
    this.price = escape(product.price);
    this.vendor = escape(product.vendor);
    this.vendor_rank = escape(product.vendorRating);
    this.link = escape(product.link);
    this.links = escape(JSON.stringify(product.links));
    this.categories = escape(product.categories.join('#'));
    this.fromAge = escape(product.fromAge);
    this.toAge = escape(product.toAge);
    this.feedIndex = escape(product.feedIndex);
    this.video = escape(product.video);
    this.boostCategories = escape(product.boostCategories);
  }
}
