import {Timer} from "../Timer";
import {Logger} from "winston";
import {S3} from "aws-sdk";
import * as s3StreamFactory from 's3-upload-stream';

export class ProductStreamWriter {
  static createProductWriteStreamS3(s3Client: S3, key: string, logTag: string, logger: Logger, timer: Timer) {
    let parts = 0;
    const s3UploadStream = s3StreamFactory(s3Client).upload({
      "Bucket": "listifeeds-bucket",
      "Key": `normalized/${key}.csv.gzip`
    });

    // Handle errors.
    s3UploadStream.on('error', function (error) {
      logger.error(error);
      throw new Error('Something wen\'t wrong streaming into S3');
    });

    // Handle progress.
    s3UploadStream.on('part', function (details) {
      parts++;
      logger.info(`${logTag} - Part ${parts} uploaded for ${key} at ${timer.getSecondsSinceStart()}`);
    });

    // Handle upload completion.
    s3UploadStream.on('uploaded', function (details) {
      logger.info(`Finished s3 normalized upload to ${key}`);
    });

    return s3UploadStream;
  }
}
