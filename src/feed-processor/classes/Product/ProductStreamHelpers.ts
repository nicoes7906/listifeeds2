import {Transform} from "stream";
import {Logger} from "winston";

export class ProductStreamHelpers {
  static createProductMapStream(mapProductFn: Function, logger: Logger, onError) {
    let indexCount = 0;
    return new Transform({
      objectMode: true,
      transform(chunk: any, enc, callback) {
        try {
          const newProduct = mapProductFn(chunk, indexCount);

          if(newProduct) {
            this.push(newProduct);
            indexCount++;
          }

          callback();
        } catch (e) {
          callback(e);
        }
      }
    }).once('error', e => {
      logger.error('Error writing product: ' + e);
      onError(e);
    })
  }
}
