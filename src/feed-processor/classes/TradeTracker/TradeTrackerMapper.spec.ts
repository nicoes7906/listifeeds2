import 'mocha';
import {expect} from 'chai';
import {TradeTrackerMapper} from './TradeTrackerMapper';
import {Product} from '../Product/Product';
import {Category} from "../../enums/category";
import {Vendor} from "../../enums/vendor";

describe('TradeTrackerMapper', () => {
  const mockXmlStreamFonqObject = getMockXmlStreamObject();

  it('should map the feed result to the product', () => {
    const mapper = new TradeTrackerMapper(Vendor.INTERTOYS);
    const product: Product = mapper.mapProduct(mockXmlStreamFonqObject);
    expect(product.ean).to.equal(8718469460304);
    expect(product.title).to.equal('EXIT Loft 500 Speelhuisje met glijbaan hoog');
    expect(product.description.indexOf('Het EXIT Loft')).to.be.above(-1);
    expect(product.price).to.equal(636.00);
    expect(product.images).to.deep.equal(["//mb.fcdn.nl/square340/353590/exit-loft-500-speelhuisje-met-glijbaan-hoog.jpg"]);
    expect(product.link).to.deep.equal("http://tc.tradetracker.net/?c=2625&amp;m=584064&amp;a=287017&amp;u=https%3A%2F%2Fwww.fonq.nl%2Fproduct%2Fexit-loft-500-speelhuisje-met-glijbaan-hoog%2F94154%2F%3Fchannel_code%3D34%26s2m_product_id%3D101895%26utm_source%3Dtt%26osadcampaign%3Dtradetracker%26utm_medium%3Dcps");
    expect(product.categories.length).to.equal(1);
    expect(product.categories[0]).to.equal(Category.BOUWEN_CONSTRUCTIE);
  });

  function getMockXmlStreamObject() {
    return {
      "name": "EXIT Loft 500 Speelhuisje met glijbaan hoog",
      "price": "636.00",
      "URL": "http://tc.tradetracker.net/?c=2625&amp;m=584064&amp;a=287017&amp;u=https%3A%2F%2Fwww.fonq.nl%2Fproduct%2Fexit-loft-500-speelhuisje-met-glijbaan-hoog%2F94154%2F%3Fchannel_code%3D34%26s2m_product_id%3D101895%26utm_source%3Dtt%26osadcampaign%3Dtradetracker%26utm_medium%3Dcps",
      "images": ["https://mb.fcdn.nl/square340/353590/exit-loft-500-speelhuisje-met-glijbaan-hoog.jpg"],
      "description": "Het EXIT Loft Speelhuisje 500 is een droomhuis voor de jongsten. In dit verhoogde huisje kunnen ze heerlijk spelen, van de hoge glijbaan afglijden of zandgebakjes maken in de zandbak eronder. Het dak is ook nog waterdicht, zodat je altijd droog zit! Bestel EXIT Loft 500 Speelhuisje met glijbaan hoog online bij fonQ. Alle EXIT producten uit voorraad leverbaar. Voor 23:00 besteld, morgen in huis.",
      "categories": ["Bouwen & Constructie"],
      "properties": {
        "brand": {"value": "EXIT"},
        "EAN": {"value": "8718469460304"},
        "fromPrice": {"value": "799.00"},
        "deliveryCosts": {"value": "0.00"},
        "subcategories": {"value": "Buitenspeelgoed"},
        "color": {"value": "Groen"},
        "material": {"value": "Hout"},
        "currency": {"value": "EUR"},
        "width": {"value": "328 cm"},
        "height": {"value": "255 cm"},
        "deliveryTime": {"value": "Voor 23:00 besteld, morgen in huis"},
        "stock": {"value": "yes"},
        "categoryPath": {"value": "Tuin &amp; balkon&gt;Buitenspeelgoed&gt;Speelhuisje"},
        "subsubcategories": {"value": "Speelhuisje"}
      }
    }
  }


});
