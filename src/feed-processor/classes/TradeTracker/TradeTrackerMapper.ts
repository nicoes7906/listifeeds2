import * as HTMLEntities from 'he';
import {ProductMappable} from '../../interfaces/product-mappable';
import {Product} from '../Product/Product';
import {Vendor} from '../../enums/vendor';
import {uniq} from 'lodash';
import {CategoryHelper} from "../CategoryHelper/CategoryHelper";
import {TradeTrackerConstants} from "../../constants/tradetracker";
import {TradeTrackerMapperHelper} from "./TradeTrackerMapperHelper";
import {ProductValidator} from "../ProductValidator/ProductValidator";

export class TradeTrackerMapper implements ProductMappable {
  vendor: Vendor;

  constructor(vendor: Vendor) {
    this.vendor = vendor;
  }

  mapProduct (tradeTrackerObject: object) {
    const product: any = {...tradeTrackerObject};

    if(TradeTrackerMapper.isValidProduct(product)) {
      const link = product.URL;
      const productName = product.name;
      const productDescription = product.description;
      const productCategories = product.categories || [];
      const productSubCategories = product.properties.subcategories;

      if(productSubCategories) {
        const subcategories = productSubCategories.value.split('/');
        productCategories.concat(subcategories)
      }

      const {fromAge, toAge}: { fromAge: number, toAge: number } = TradeTrackerMapperHelper.getFromToAge(product.extraInfo);

      const categoriesByAliases = TradeTrackerMapper.getCategoriesByAliases(productCategories);
      const categoriesByTitle = CategoryHelper.getCategoriesFromText(productName);
      const categoriesByDescription = CategoryHelper.getCategoriesFromText(productDescription);
      const categories = uniq([]
        .concat(categoriesByAliases)
        .concat(categoriesByTitle)
        .concat(categoriesByDescription));

      const productImages = product.images ? product.images : [];

      return new Product(
        product.properties.EAN.value,
        productName,
        productImages.map(image => TradeTrackerMapper.stripProtocol(image)),
        productDescription,
        product.price,
        this.vendor,
        null,
        link,
        { desktopUrl: '', mobileUrl: ''},
        categories,
        fromAge,
        toAge,
        -1,
        null,
        null
      )
    }

    return null;
  }

  static stripProtocol(imageUrl) {
    return '//' + imageUrl.replace(/(^\w+:|^)\/\//, '');
  }

  static isValidProduct(product) {
    return product &&
      product.properties.EAN &&
      ProductValidator.isValidEAN(product.properties.EAN.value) &&
      ProductValidator.isValidTitle(product.name);
  }

  static getCategoriesByAliases(categories) {
    if(!categories) return [];
    return categories.reduce((acc, catName) => {
      return acc.concat(CategoryHelper.getCategoriesFromAliasMap(catName, TradeTrackerConstants.CATEGORY_ALIAS_MAP));
    }, []);
  }
}
