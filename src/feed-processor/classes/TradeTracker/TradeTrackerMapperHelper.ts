const fromAgeRegex = /Van ([0-9]+) tot ([0-9]+) jaar/;

export class TradeTrackerMapperHelper {
  static getFromToAge(extraInfoString): {fromAge: number, toAge: number} {
    if(typeof extraInfoString === 'string') {
      const matches = extraInfoString.match(fromAgeRegex);
      if(matches && matches.length > 0) {
        return {fromAge: <number> +matches[1], toAge: <number> +matches[2]};
      }
    }
    return {fromAge: -1, toAge: -1};
  }
}
