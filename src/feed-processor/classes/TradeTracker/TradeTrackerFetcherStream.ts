import {Logger} from 'winston';
import {Timer} from "../Timer";
import {Vendor} from "../../enums/vendor";
import {ProductStreamHelpers} from "../Product/ProductStreamHelpers";
import * as request from "hyperquest";
import {Promise} from "core-js";
import * as pump from 'pump';
import {Pool} from "mysql";
import {Runnable} from "../../interfaces/runnable";
import {RunLimitHandler} from "../RunLimitHandler";
import {TradeTrackerConstants} from "../../constants/tradetracker";
import {TradeTrackerMapper} from "./TradeTrackerMapper";
import XmlStreamParser from "../XmlStreamParser/XmlStreamParser";
import {ProductBatchWriterMysql} from "../Product/ProductBatchWriterMysql";
import {DynamoDB, S3} from "aws-sdk";

export class TradeTrackerFetcherStream {
  static createTradeTrackerFetcher(vendor: Vendor, uri: string, logTag: string, sqlClient: Pool, logger: Logger): Runnable {
    const logInfo = (msg) => {
      logger.info(msg);
    };

    const logError = (msg) => {
      logger.error(msg);
    };

    return {
      run
    };

    function run(): Promise<void> {
      return new Promise((resolve, reject) => {
        const url = `https://${TradeTrackerConstants.HOST}${uri}`;
        const productMapper = new TradeTrackerMapper(vendor);

        const ttMapper = ProductStreamHelpers.createProductMapStream(
          productMapper.mapProduct.bind(productMapper),
          logger,
          reject
        );
        const timer = new Timer(logTag, logger.info.bind(logger));

        const productWriter = ProductBatchWriterMysql.createProductWriteStream(
          sqlClient,
          logTag,
          logger,
          timer
        );

        timer.start();

        const xmlStreamParser = XmlStreamParser.createXmlStreamParser({collections: ['category', 'image']});

        const requestStream = request(url);

        const runLimitHandler = RunLimitHandler.createRunLimitHandlerStream(
          requestStream.destroy.bind(requestStream),
          logError
        );

        pump(
          requestStream,
          xmlStreamParser,
          runLimitHandler,
          ttMapper,
          productWriter,
          (err) => {
            if (err) {
              if(err.toString().indexOf('premature close') > -1) {
                logInfo('Pipeline ended prematurely');
                resolve();
              } else {
                logError(`Pipeline ${logTag} failed.`);
                reject(err);
              }
            } else {
              logInfo(`Pipeline ${logTag} succeeded.`);
              resolve();
            }
          }
        )
      });
    }
  }
}
