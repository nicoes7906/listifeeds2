import {Logger} from 'winston';
import {LoggerFactory} from './LoggerFactory';
import {SlackWebhook} from "./SlackWebhook";
import {S3} from "aws-sdk";
import {createPool, Pool, PoolOptions} from "mysql2";

require('dotenv').load();
const {LOG_LEVEL, MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD} = process.env;

const AWS = require('aws-sdk');

const s3Client: S3 = new AWS.S3();

const mysqlConfig: PoolOptions = {
  host: MYSQL_HOST,
  port: parseInt(MYSQL_PORT),
  user: MYSQL_USER,
  password: MYSQL_PASSWORD,
  database: 'listifeeds',
  connectionLimit: 100,
  insecureAuth: true
};

export class ScriptExecutor {

  static executeRunner(name: string, runFn: (sqlClient: Pool, s3Client: S3, logger: Logger) => Promise<any>): Promise<any> {
      return new Promise((resolve, reject) => {

        const logger: Logger = LoggerFactory.createLogger(LOG_LEVEL || 'info');

        const slackLogger = SlackWebhook.createSlackLogger(logger);

        const connectionPool: Pool = createPool(mysqlConfig);

        logger.info(`Starting executor ${name}`);

        return runFn(connectionPool, s3Client, logger)
          .then(() => {
            connectionPool.end(() => {
              logger.info(`Finished executor`);
              slackLogger.log(`Updating ${name.toLowerCase()} succeeded`);
              resolve(null);
            });
          })
          .catch(e => {
              connectionPool.end(() => {
                slackLogger.log(`Updating ${name.toLowerCase()} failed with error: ` + e.toString());
                reject(e);
              })
          });
      })
    }
}
