const csv = require('csv');

export function createCsvReaderTransform() {
  let indexCount = 0;
  let csvKeys = [];

  return csv.transform(function(row){
    if(indexCount === 0) {
      csvKeys = row;
      indexCount++;
      return null;
    } else {
      indexCount++;
      return mapCsvLineToObject(row, csvKeys);
    }
  });

  function mapCsvLineToObject(row, csvKeys) {
    return csvKeys.reduce((acc, key, index) => {
      acc[key] = row[index]
      return acc;
    }, {});
  }
}
