import {Logger} from "winston";
import {S3} from "aws-sdk";
import * as JSONStream from 'JSONStream';
import {Promise} from "core-js";
import {Runnable} from "../../interfaces/runnable";
import {ProductStreamHelpers} from "../Product/ProductStreamHelpers";
import {Timer} from "../Timer";
import {BolMapper} from "./BolMapper";
import {createS3DownloadStream} from "../../lib/S3Download";
import {Vendor} from "../../enums/vendor";
import {Pool} from "mysql";
import {ProductBatchWriterMysql} from "../Product/ProductBatchWriterMysql";

export class BolJsonFetcherStream {
  static createJsonFetcher(fileName: string, vendor: Vendor, logTag: string, sqlClient: Pool, s3Client: S3, logger: Logger): Runnable {
    return {
      run
    };

    function run(): Promise<string> {
      return new Promise((resolve, reject) => {
        const bolReadStream = createS3DownloadStream(s3Client, fileName, logger);
        const mapProductFn = BolJsonFetcherStream.createMapProductFn();
        const bolOaMapper = ProductStreamHelpers.createProductMapStream(mapProductFn, logger, reject);
        const timer = new Timer(logTag, logger.info.bind(logger));

        timer.start();

        const productWriter = ProductBatchWriterMysql.createProductWriteStream(sqlClient, 'bol-api', logger, timer);

        bolReadStream
          .pipe(JSONStream.parse('products.*'))
          .pipe(bolOaMapper)
          .pipe(productWriter)
          .on('finish', () => resolve('Ok'))
      });
    }
  }

  static createMapProductFn() {
    const bolMapper = new BolMapper();
    let productCount = 0;
    return product => {
      productCount++;
      return bolMapper.mapProduct(product, 'json', productCount);
    };
  }
}
