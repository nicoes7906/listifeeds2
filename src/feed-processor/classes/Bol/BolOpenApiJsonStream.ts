import {Readable, ReadableOptions} from 'stream';
import * as request from 'request';
import {BolConstants} from "../../constants/bol";
import {Logger} from "winston";

export class BolOpenApiJsonStream {

  static createReadableStream(bolCategoryNumber, logger: Logger) {
    const NUMBER_OF_CALLS = 12;
    let startCallCount = 0;
    let finishedCallCount = 0;
    const readStack = [];

    const readStream = new ReadableBolJson({
      objectMode: true,
      read() {
        const self = this;
        tryPush();

        function tryPush() {
          if (readStack.length) {
            const product = readStack.shift();
            self.push(product);
          } else if(finishedCallCount === NUMBER_OF_CALLS) {
            self.push(null);
          } else {
            setTimeout(tryPush, 100);
          }
        }
      }
    });

    while(startCallCount < NUMBER_OF_CALLS) {
      const url = BolOpenApiJsonStream.getBolUrl(bolCategoryNumber, startCallCount);
      request(url, function (err, response, body) {
        if (err) readStream.emit('error', err);
        try {
          const products = JSON.parse(body).products;
          products.forEach(product => readStack.push(product));
          finishedCallCount++;
        } catch (e) {
          logger.error(e);
        }
      });
      startCallCount++;
    }

    return readStream;
  }

  static getBolUrl(bolCategoryNumber, offsetIndex) {
    return `https://api.bol.com/catalog/v4/lists/?ids=${bolCategoryNumber}&apikey=${BolConstants.API_KEY}&offset=${offsetIndex * 100}&sort=rankasc&limit=100&includeattributes=true`;
  }
}

class ReadableBolJson extends Readable {
  constructor(readableOptions: ReadableOptions){
    super(readableOptions);
    this.readable = false;
  }
  abort() {
    this.pause();
    this.emit('end');
  }
}
