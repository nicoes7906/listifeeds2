import {BolMapper} from "./BolMapper";
import {expect} from 'chai';
import {Vendor} from "../../enums/vendor";
import {Category} from "../../enums/category";

describe('BolMapper', () => {

  describe('Xml type', () => {
    it('should correctly map a product', () => {
      const mapper = new BolMapper();
      const mockProduct = _getMockXmlProductStream();
      const product = mapper.mapProduct(mockProduct, 'xml');
      expect(product.ean).to.equal(8717255005019);
      expect(product.title).to.equal('Houten puzzel locomotief 5 stukken');
      expect(product.description.indexOf('houten')).to.equal(5);
      expect(product.price).to.equal(6.95);
      expect(product.vendor).to.equal(Vendor.BOLFEED);
      expect(product.images[0]).to.equal("https://s.s-bol.com/imgbase0/imagebase3/extralarge/FC/0/0/0/0/9200000098100000.jpg");
      expect(product.links.mobileUrl).to.equal(`http://partner.bol.com/click/click?p=1&t=url&s=47511&url=https://www.bol.com/nl/p/houten-puzzel-locomotief-5-stukken/9200000098100000/&f=TXL`)
      expect(product.links.desktopUrl).to.equal(`http://partner.bol.com/click/click?p=1&t=url&s=47511&url=https://www.bol.com/nl/p/houten-puzzel-locomotief-5-stukken/9200000098100000/&f=TXL`)
      expect(product.categories.length).to.equal(2);
      expect(product.categories[0]).to.equal(Category.PUZZELS);
      expect(product.categories[1]).to.equal(Category.FROZEN);
    });

    it('should not map products that are not available in NL', () => {
      const mapper = new BolMapper();
      const mockProduct = _getMockXmlProductStream();
      delete mockProduct.OfferNL;
      const product = mapper.mapProduct(mockProduct, 'xml');
      expect(product).to.equal(null);
    });

    it('should only map products offered by Bol (B) and not by the plaza (P)', () => {
      const mapper = new BolMapper();
      const mockProduct = _getMockXmlProductStream();
      mockProduct.OfferNL.sellerType = 'P';
      const product = mapper.mapProduct(mockProduct, 'xml');
      expect(product).to.equal(null);
    });

    it('should correctly parse large prices', () => {
      const mapper = new BolMapper();
      const mockProduct = _getMockXmlProductStream();
      mockProduct.OfferNL.sellingPrice = "1,799.00";
      const product = mapper.mapProduct(mockProduct, 'xml');
      expect(product.price).to.equal(1799.00);
    });

  });

  describe('JSON type', () => {
    it('should correctly map a product', () => {
      const mapper = new BolMapper();
      const mockProduct = _getMockJsonProduct();
      const product = mapper.mapProduct(mockProduct, 'json');
      expect(product).to.not.equal(null);
      expect(product.ean).to.equal(5702016077452);
      expect(product.title).to.equal('LEGO Friends Mia\'s Boomhut - 41335');
      expect(product.description.indexOf('Inclusief de minipoppetjes Mia en Daniel')).to.equal(2);
      expect(product.price).to.equal(28.95);
      expect(product.vendor).to.equal(Vendor.BOLOPENAPI);
      expect(product.images.length).to.equal(4);
      expect(product.images[0]).to.equal("https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279.jpg");
      expect(product.links.mobileUrl).to.equal(`http://partner.bol.com/click/click?p=1&t=url&s=47511&url=https://www.bol.com/nl/p/lego-friends-mia-s-boomhut-41335/9200000080799279&f=TXL`)
      expect(product.links.desktopUrl).to.equal(`http://partner.bol.com/click/click?p=1&t=url&s=47511&url=https://www.bol.com/nl/p/lego-friends-mia-s-boomhut-41335/9200000080799279&f=TXL`)
      expect(product.categories.length).to.equal(3);
      expect(product.categories[0]).to.equal(Category.BOUWEN_CONSTRUCTIE);
      expect(product.categories[1]).to.equal(Category.SPELLEN);
      expect(product.categories[2]).to.equal(Category.LEGO);
      expect(product.fromAge).to.equal(6);
      expect(product.toAge).to.equal(12);
    });

    it('should only map products that are offered by bol.com', () => {
      const mapper = new BolMapper();
      const mockProduct = _getMockJsonProduct();
      mockProduct.offerData.offers[0] = {
        ...mockProduct.offerData.offers[0],
        seller: {
          ...mockProduct.offerData.offers[0].seller,
          sellerType: 'grootzakelijke verkoper'
        }
      };
      const product = mapper.mapProduct(mockProduct, 'json');
      expect(product).to.equal(null);
    });

  })

});

function _getMockXmlProductStream() {
  return {
    "productId": "9200000098100000",
    "ean": "8717255005019",
    "title": "Houten puzzel locomotief 5 stukken",
    "productPageUrlNL": "https://www.bol.com/nl/p/houten-puzzel-locomotief-5-stukken/9200000098100000/",
    "productUrlBE": "https://www.bol.com/be/p/houten-puzzel-locomotief-5-stukken/9200000098100000/",
    "imageUrl": "https://s.s-bol.com/imgbase0/imagebase3/extralarge/FC/0/0/0/0/9200000098100000.jpg",
    "brand": "roel",
    "familyName": "Houten puzzel locomotief 5 stukken",
    "OfferNL": {
      "sellingPrice": "6.95",
      "shippingCost": "0.00",
      "condition": "new",
      "isDeliverable": "true",
      "ultimateOrderTime": "00:00:00.000+01:00",
      "maximalDeliveryDay": "7",
      "sellerType": "B"
    },
    "Gpc": {
      "segmentName": "Toys/Games",
      "familyName": "Toys/Games",
      "className": "Bordspelen, kaarten en puzzels",
      "brickName": "Puzzels (Niet-elektrisch)",
      "chunkName": "Puzzel"
    },
    "Category": {
      "unit": "Speelgoed en Entertainment",
      "category": "Hobby Spellen en Buitenspeelgoed",
      "productgroup": "Toys",
      "productsubgroup": "Puzzels",
      "subsubgroup": "Puzzels"
    },
    "description": "Deze houten puzzel frozen locomotief in een houten frame heeft 5 houtenstukken en is geschikt voor jonge kinderen die voor het eerst gaan puzzelen.<br /><br />Puzzelen is goed voor de creatieve ontwikkeling van uw kind en de fijne motoriek.",
    "Properties": {"Property": {"key": "Material", "value": "Hout"}}
  }
}

function _getMockJsonProduct() {
  return {
    "id": "9200000080799279",
    "ean": "5702016077452",
    "gpc": "toys",
    "title": "LEGO Friends Mia's Boomhut - 41335",
    "subtitle": "Klim omhoog in de boomhut en zorg ervoor dat Mia zich helemaal op haar plek voelt!",
    "specsTag": "LEGO",
    "summary": "6 - 12 jaar | 351 onderdelen | 2 minifiguren | 2018",
    "rating": 47,
    "shortDescription": "• Inclusief de minipoppetjes Mia en Daniel, het konijntje Cinnamon en de vogel Mimi. • Inclusief een boomhut die toegankelijk is via een ladder of klauternet, een zolder met een dak dat open kan en een opbergkist, een boomstam met opbergkist en een konijnenhuisje. • Accessoire-elementen: een skateboard, een waterpistool, een zaklamp, een boek, een pizza, een mok, een bordspel, een kaart van Heartlake City Park, een boemerang, een tegel met een liefdesbrief, een vogelnestje, een ei en een wortel. • Glijd naar beneden langs de tokkelbaan en klauter weer omhoog de boomhut in via het net. • Maak je klaar voor een spannend logeerpartijtje met Mia’s vriendinnen met spookverhalen bij het licht van de fakkel, bordspelen en pizza. Help Mia om haar broers boomhut om te vormen tot de perfecte afspreekplek in het park voor al haar vriendinnen. Stop Daniels spullen in een doos onder het dak voor hij naar de universiteit vertrekt en help Mia alles te versieren door foto's van haar vriendinnen en familie op te hangen. Klauter langs het net omhoog naar de tweede verdieping en gebruik het waterpistool om ongewenste bezoekers op een afstand te houden! Glijd daarna omlaag langs de tokkelbaan om op bezoek te gaan bij Mia's konijntje Mimì in haar huisje en geef haar een lekkere wortel.",
    "longDescription": "• Inclusief de minipoppetjes Mia en Daniel, het konijntje Cinnamon en de vogel Mimi.<br />• Inclusief een boomhut die toegankelijk is via een ladder of klauternet, een zolder met een dak dat open kan en een opbergkist, een boomstam met opbergkist en een konijnenhuisje.<br />• Accessoire-elementen: een skateboard, een waterpistool, een zaklamp, een boek, een pizza, een mok, een bordspel, een kaart van Heartlake City Park, een boemerang, een tegel met een liefdesbrief, een vogelnestje, een ei en een wortel.<br />• Glijd naar beneden langs de tokkelbaan en klauter weer omhoog de boomhut in via het net.<br />• Maak je klaar voor een spannend logeerpartijtje met Mia’s vriendinnen met spookverhalen bij het licht van de fakkel, bordspelen en pizza.<br /><br />Help Mia om haar broers boomhut om te vormen tot de perfecte afspreekplek in het park voor al haar vriendinnen. Stop Daniels spullen in een doos onder het dak voor hij naar de universiteit vertrekt en help Mia alles te versieren door foto's van haar vriendinnen en familie op te hangen. <br /><br />Klauter langs het net omhoog naar de tweede verdieping en gebruik het waterpistool om ongewenste bezoekers op een afstand te houden! Glijd daarna omlaag langs de tokkelbaan om op bezoek te gaan bij Mia's konijntje Mimì in haar huisje en geef haar een lekkere wortel.<br /><br /><a href=\"https://www.bol.com/nl/b/lego/4148554/?lastId=4279587071&promo=LEGOFriends_PDP\"><img src=\"https://www.bol.com/nl/cms/images/LEGOFriends2018.jpg\" style=\"max-width: 99%; height: auto;\" /></a>",
    "attributeGroups": [
      {
        "title": "Producteigenschappen",
        "attributes": [
          {
            "label": "Aantal onderdelen",
            "value": "351"
          },
          {
            "label": "Aantal minifiguren",
            "value": "2"
          },
          {
            "key": "COLOUR",
            "label": "Kleur",
            "value": "Paars"
          },
          {
            "key": "MATERIAL",
            "label": "Materiaal",
            "value": "Kunststof"
          }
        ]
      },
      {
        "title": "Gebruiksinformatie",
        "attributes": [
          {
            "key": "RECOMMENDED_AGE",
            "label": "Aanbevolen leeftijd",
            "value": "6 - 12 jaar"
          },
          {
            "label": "Leeftijdswaarschuwing",
            "value": "Niet geschikt voor kinderen onder de 3 jaar"
          }
        ]
      },
      {
        "title": "Technische specificaties",
        "attributes": [
          {
            "key": "POWER_TYPE",
            "label": "Voedingstype",
            "value": "Niet elektrisch"
          },
          {
            "key": "NUMBER_OF_BATTERIES_REQUIRED",
            "label": "Benodigd aantal batterijen",
            "value": "0"
          },
          {
            "label": "Aantal meegeleverde batterijen",
            "value": "Geen"
          }
        ]
      },
      {
        "title": "Afmetingen & Gewicht",
        "attributes": [
          {
            "label": "Afmetingen product",
            "value": "26 x 28 x 7 cm (lxbxh)"
          },
          {
            "label": "Afmetingen verpakking",
            "value": "27,7 x 26,3 x 7,9 cm (lxbxh)"
          },
          {
            "key": "WEIGHT",
            "label": "Gewicht",
            "value": "0,6 kg"
          }
        ]
      },
      {
        "title": "Productinformatie",
        "attributes": [
          {
            "label": "Merk",
            "value": "LEGO"
          },
          {
            "key": "MPN",
            "label": "Artikelnummer",
            "value": "41335"
          },
          {
            "label": "Jaar van uitgave",
            "value": "2018"
          },
          {
            "key": "CERTIFICATION_MARK",
            "label": "Keurmerk",
            "value": "CE"
          },
          {
            "key": "AWARD",
            "label": "Gewonnen prijzen",
            "value": "Geen"
          }
        ]
      },
      {
        "title": "Overige kenmerken",
        "attributes": [
          {
            "key": "Toys Theme",
            "label": "Speelgoedthema",
            "value": "Stadsleven"
          },
          {
            "key": "Kind of Construction Toy",
            "label": "Type constructiespeelgoed",
            "value": "Bouwset"
          }
        ]
      }
    ],
    "entityGroups": [
      {
        "title": "Acteurs"
      },
      {
        "title": "Artiesten"
      },
      {
        "title": "Auteurs"
      },
      {
        "title": "Regisseur"
      },
      {
        "title": "Regisseur"
      },
      {
        "title": "Uitgever",
        "entities": [
          {
            "id": "4148554",
            "value": "LEGO"
          }
        ]
      }
    ],
    "urls": [
      {
        "key": "DESKTOP",
        "value": "https://www.bol.com/nl/p/lego-friends-mia-s-boomhut-41335/9200000080799279"
      },
      {
        "key": "MOBILE",
        "value": "https://www.bol.com/nl/p/lego-friends-mia-s-boomhut-41335/9200000080799279"
      }
    ],
    "images": [
      {
        "type": "IMAGE",
        "key": "XS",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/mini/FC/9/7/2/9/9200000080799279.jpg"
      },
      {
        "type": "IMAGE",
        "key": "S",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/tout/FC/9/7/2/9/9200000080799279.jpg"
      },
      {
        "type": "IMAGE",
        "key": "M",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/thumb/FC/9/7/2/9/9200000080799279.jpg"
      },
      {
        "type": "IMAGE",
        "key": "L",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/regular/FC/9/7/2/9/9200000080799279.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XXL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/extralarge/FC/9/7/2/9/9200000080799279.jpg"
      }
    ],
    "media": [
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_1.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_10.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_11.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_12.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_13.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_14.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_15.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_16.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_17.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_19.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_2.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_3.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_4.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_5.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_6.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_7.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_8.jpg"
      },
      {
        "type": "IMAGE",
        "key": "XL",
        "url": "https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/7/2/9/9200000080799279_9.jpg"
      }
    ],
    "offerData": {
      "bolCom": 1,
      "nonProfessionalSellers": 0,
      "professionalSellers": 0,
      "offers": [
        {
          "id": "9200000080799279",
          "condition": "Nieuw",
          "price": 28.95,
          "availabilityCode": "-1",
          "availabilityDescription": "Op voorraad. Voor 23:59 uur besteld, morgen in huis",
          "comment": "Op voorraad. Voor 23:59 uur besteld, morgen in huis",
          "seller": {
            "id": "0",
            "sellerType": "bol.com",
            "displayName": "bol.com",
            "url": "www.bol.com",
            "topSeller": false,
            "useWarrantyRepairConditions": false
          },
          "bestOffer": true
        }
      ]
    },
    "parentCategoryPaths": [
      {
        "parentCategories": [
          {
            "id": "7934",
            "name": "Speelgoed"
          },
          {
            "id": "10462",
            "name": "Bouwen & Constructie"
          },
          {
            "id": "20000",
            "name": "Bouwsets & Onderdelen"
          },
          {
            "id": "20001",
            "name": "Bouwsets"
          }
        ]
      },
      {
        "parentCategories":
          [
            {
              "id": "7934",
              "name": "Speelgoed"
            },
            {
              "id": "10596",
              "name": "Spellen"
            }
        ]
      }
    ]
  }
}
