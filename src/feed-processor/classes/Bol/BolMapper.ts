import {uniq, isString} from 'lodash';
import {ProductMappable} from "../../interfaces/product-mappable";
import {Product} from "../Product/Product";
import {Vendor} from "../../enums/vendor";
import {BolConstants} from "../../constants/bol";
import {CategoryHelper} from "../CategoryHelper/CategoryHelper";
import {AgeHelper} from "../AgeHelper/AgeHelper";
import {ProductValidator} from "../ProductValidator/ProductValidator";
import {ProductLinks} from "../../types/link";


export class BolMapper implements ProductMappable {
  unmappedCategories = [];

  mapProduct(bolDataProduct, type: 'xml' | 'json', feedIndex?: number | -1): Product | null {
    return type === 'xml' ? BolMapper._mapXmlProduct(bolDataProduct, feedIndex, (category) => this.unmappedCategories.push(category)) : BolMapper._mapJsonProduct(bolDataProduct, feedIndex);
  }

  getUnmappedCategories() {
    return this.unmappedCategories;
  }

  static _mapJsonProduct(bolJsonProduct, feedIndex: number): Product | null {
    try {
      const bolOffer = bolJsonProduct.offerData.offers?.find(bolOffer => {
        return bolOffer?.seller?.sellerType === 'bol.com';
      });
      if(bolOffer && BolMapper.isValidProduct(bolJsonProduct.ean, bolJsonProduct.title)) {
        const categoriesByAliases = BolMapper.getCategoriesByAliasFromParentCategoryPaths(bolJsonProduct.parentCategoryPaths);
        const categoriesByTitle = bolJsonProduct.title ? CategoryHelper.getCategoriesFromText(bolJsonProduct.title) : [];
        const categoriesByDescription = bolJsonProduct.longDescription ? CategoryHelper.getCategoriesFromText(bolJsonProduct.longDescription) : [];
        const categories = uniq([]
          .concat(categoriesByAliases)
          .concat(categoriesByTitle)
          .concat(categoriesByDescription));

        const {fromAge, toAge}: {fromAge: number, toAge: number} = BolMapper.getFromAndToAge(bolJsonProduct.attributeGroups);

        const images = BolMapper.getImagesFromMedia(bolJsonProduct.media);
        const mainImage = bolJsonProduct?.images?.find(image => image.key === "XL")?.url || bolJsonProduct?.images?.find(image => image.key === "L")?.url;

        // If we have a main image then we want to use that as the first of max 4 images.
        if(mainImage) {
          images.unshift(mainImage);
          images.pop();
        }

        return new Product(
          bolJsonProduct.ean,
          bolJsonProduct.title,
          images,
          bolJsonProduct.longDescription,
          parseFloat(bolOffer.price),
          Vendor.BOLOPENAPI,
          +bolJsonProduct.rating,
          '',
          BolMapper.makeBolOpenApiLink(bolJsonProduct),
          categories,
          fromAge,
          toAge,
          feedIndex,
          null,
          null
        )
      }

      return null;
    } catch (e) {
      throw new Error(e);
    }
  }

  static _mapXmlProduct(bolXmlProduct, feedIndex, onUnmapped): Product | null {
    if (bolXmlProduct.OfferNL && bolXmlProduct.OfferNL.sellerType.toLowerCase() === 'b' && BolMapper.isValidProduct(bolXmlProduct.ean, bolXmlProduct.title)) {
      const categoriesByAliasesProductSubGroup = CategoryHelper.getCategoriesFromAliasMap(bolXmlProduct.Category.productsubgroup, BolConstants.CATEGORY_ALIAS_MAP);
      const categoriesByAliasesProductGroup = CategoryHelper.getCategoriesFromAliasMap(bolXmlProduct.Category.productgroup, BolConstants.CATEGORY_ALIAS_MAP);
      const categoriesByTitle = bolXmlProduct.title ? CategoryHelper.getCategoriesFromText(bolXmlProduct.title) : [];
      const categoriesByDescription = bolXmlProduct.description ? CategoryHelper.getCategoriesFromText(bolXmlProduct.description) : [];
      const categories = uniq([]
        .concat(categoriesByAliasesProductSubGroup)
        .concat(categoriesByAliasesProductGroup)
        .concat(categoriesByTitle)
        .concat(categoriesByDescription));

      if(categories.length === 0) {
        onUnmapped(bolXmlProduct.Category);
      }

      return new Product(
        bolXmlProduct.ean,
        bolXmlProduct.title,
        [bolXmlProduct.imageUrl],
        bolXmlProduct.description,
        BolMapper.parseXmlPrice(bolXmlProduct.OfferNL.sellingPrice),
        Vendor.BOLFEED,
        null,
        '',
        BolMapper.makeBolFeedLink(bolXmlProduct),
        categories,
        -1,
        -1,
        feedIndex,
        null,
        null
      )
    }
    return null;
  }

  static _mapCsvProduct(bolXmlProduct, feedIndex, onUnmapped): Product | null {
    if (bolXmlProduct?.OfferNL?.sellingPrice && BolMapper.isValidProduct(bolXmlProduct.ean, bolXmlProduct.title)) {
      const categoriesByAliasesProductSubGroup = CategoryHelper.getCategoriesFromAliasMap(bolXmlProduct.Category.subgroup, BolConstants.CATEGORY_ALIAS_MAP);
      const categoriesByAliasesProductGroup = CategoryHelper.getCategoriesFromAliasMap(bolXmlProduct.Category.productgroup, BolConstants.CATEGORY_ALIAS_MAP);
      const categoriesByTitle = bolXmlProduct.title ? CategoryHelper.getCategoriesFromText(bolXmlProduct.title) : [];
      const categoriesByDescription = bolXmlProduct.description ? CategoryHelper.getCategoriesFromText(bolXmlProduct.description) : [];
      const categories = uniq([]
        .concat(categoriesByAliasesProductSubGroup)
        .concat(categoriesByAliasesProductGroup)
        .concat(categoriesByTitle)
        .concat(categoriesByDescription));

      if(categories.length === 0) {
        onUnmapped(bolXmlProduct.Category);
      }

      return new Product(
        bolXmlProduct.ean,
        bolXmlProduct.title,
        [bolXmlProduct.imageUrl],
        bolXmlProduct.description,
        BolMapper.parseXmlPrice(bolXmlProduct.OfferNL.sellingPrice),
        Vendor.BOLFEED,
        null,
        '',
        BolMapper.makeBolFeedLink(bolXmlProduct),
        categories,
        -1,
        -1,
        feedIndex,
        null,
        null
      )
    }
    return null;
  }

  static isValidProduct(ean, title) {
    return ProductValidator.isValidEAN(ean) &&
      ProductValidator.isValidTitle(title);
  }

  static parseXmlPrice(price) {
    return isString(price) && parseFloat(price.replace(',', ''))
  }

  static getCategoriesByAliasFromParentCategoryPaths(parentCategoryPaths) {
    const flattenedCategoryPaths = parentCategoryPaths.reduce((acc, parentCategoryPath) => [].concat(acc).concat(parentCategoryPath.parentCategories), []);
    if(!flattenedCategoryPaths) return [];
    return flattenedCategoryPaths.reduce((acc, category) => {
      return acc.concat(CategoryHelper.getCategoriesFromAliasMap(category.name, BolConstants.CATEGORY_ALIAS_MAP));
    }, []);
  }

  /**
   * Get max 4 for images for media collection
   * @param mediaCollection
   */
  static getImagesFromMedia(mediaCollection: Array<{url: string, type: string}>) {
      return mediaCollection && mediaCollection.length && mediaCollection.map(mediaItem => {
        if (mediaItem.type === 'IMAGE') {
          return mediaItem.url;
        }
      }).slice(0, 4) || [];
  }

  static makeBolFeedLink(bolXmlProduct: any): ProductLinks {
    return {
      desktopUrl: `http://partner.bol.com/click/click?p=1&t=url&s=${BolConstants.SITE_ID}&url=${bolXmlProduct.productPageUrlNL}&f=TXL`,
      mobileUrl: `http://partner.bol.com/click/click?p=1&t=url&s=${BolConstants.SITE_ID}&url=${bolXmlProduct.productPageUrlNL}&f=TXL`
    }
  }

  static makeBolOpenApiLink(bolJsonProduct: any): ProductLinks {
    const bolDesktopLink = bolJsonProduct?.urls?.find(url => url.key === 'DESKTOP')?.value;
    const bolMobileLink = bolJsonProduct?.urls?.find(url => url.key === 'MOBILE')?.value;
    return {
        desktopUrl: `http://partner.bol.com/click/click?p=1&t=url&s=${BolConstants.SITE_ID}&url=${bolDesktopLink}&f=TXL`,
        mobileUrl: `http://partner.bol.com/click/click?p=1&t=url&s=${BolConstants.SITE_ID}&url=${bolMobileLink}&f=TXL`
      }
  }

  static getFromAndToAge(attributeGroups) {
    const flattenedAttributes = BolMapper.flattenAttributes(attributeGroups);
    const recommendedAge = flattenedAttributes.filter(attr => attr.key && attr.key === 'RECOMMENDED_AGE')[0] || null;
    const recommendAgeArray = recommendedAge && recommendedAge.value.split(' ').length > 1 ? recommendedAge.value.split(' ') : null;
    const fromAgeRaw = recommendAgeArray ? +recommendAgeArray[0] : -1;
    const toAgeRaw = recommendAgeArray ? +recommendAgeArray[2] : -1;
    return AgeHelper.getFromAndToAge(fromAgeRaw, toAgeRaw);
  }

  static flattenAttributes(attributeGroups: Array<{attributes}>) {
    if(!attributeGroups) return [];
    return attributeGroups && attributeGroups.reduce((sum, value) => {
      return value.attributes && sum.concat(value.attributes) || sum
    }, []);
  }
}
