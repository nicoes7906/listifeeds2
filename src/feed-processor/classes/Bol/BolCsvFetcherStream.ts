import {Logger} from 'winston';
import {Timer} from "../Timer";
import {Vendor} from "../../enums/vendor";
import {ProductStreamHelpers} from "../Product/ProductStreamHelpers";
import {Promise} from "core-js";
import * as csv from 'csv';
import {Runnable} from "../../interfaces/runnable";
import {RunLimitHandler} from "../RunLimitHandler";
import {ProductBatchWriterMysql} from "../Product/ProductBatchWriterMysql";
import {S3} from "aws-sdk";
import {Pool} from "mysql";
import {createS3DownloadStream} from "../../lib/S3Download";
import {BolFeedsTargetFile} from "../../enums/BolFeedsTargetFile";
import {BolFeedsCsvMapper} from "./BolFeedsCsvMapper";

export class BolFeedCsvFetcherStream {
  static createCsvFetcher(logTag: string, sqlClient: Pool, s3Client: S3, logger: Logger, fileName: BolFeedsTargetFile): Runnable {
    return {
      run
    };

    function run(): Promise<string> {
      return new Promise((resolve, reject) => {
        const productMapper = new BolFeedsCsvMapper(Vendor.BOLFEED);
        const bolMapper = ProductStreamHelpers.createProductMapStream(
          productMapper.mapProduct.bind(productMapper),
          logger,
          reject
        );
        const timer = new Timer(logTag, logger.info.bind(logger));

        timer.start();

        const productWriter = ProductBatchWriterMysql.createProductWriteStream(
          sqlClient,
          logTag,
          logger,
          timer
        );

        const requestStream = createS3DownloadStream(s3Client, fileName, logger);

        const runLimitHandler = RunLimitHandler.createRunLimitHandlerStream(
          requestStream.destroy.bind(requestStream),
          logger.error.bind(logger)
        );

        requestStream
          .pipe(csv.parse({columns: true, delimiter: "|", relax: true}))
          .pipe(runLimitHandler)
          .pipe(bolMapper)
          .pipe(productWriter)
          .on('finish', () => resolve('Ok'))
      });
    }
  }
}
