import { ProductMappable } from "../../interfaces/product-mappable";
import { Product } from "../Product/Product";
import { Vendor } from "../../enums/vendor";
import {set} from "lodash"
import {BolMapper} from "./BolMapper";

export class BolFeedsCsvMapper implements ProductMappable {
  vendor: Vendor;
  categories: Array<string>;

  constructor(vendor) {
    this.vendor = vendor;
    this.categories = [];
  }

  mapProduct(bolCsvObject: any, feedIndex:number, logError: Function): Product | null {
    const bolObject = unflatten(bolCsvObject);
    const product = BolMapper._mapCsvProduct(bolObject, feedIndex, () => {});
    return product;
  }
}

function unflatten(flattened) {
  let object = {};

  for (let key in flattened) {
    let path = key.split('.');
    set(object, path, flattened[key]);
  }

  return object;
}
