import {Logger} from 'winston';
import {Timer} from "../Timer";
import {Promise} from "core-js";
import {Runnable} from "../../interfaces/runnable";
import XmlStreamParser from "../XmlStreamParser/XmlStreamParser";
import {ProductStreamHelpers} from "../Product/ProductStreamHelpers";
import {BolMapper} from "./BolMapper";
import * as pump from 'pump';
import {S3} from "aws-sdk";
import {createS3DownloadStream} from "../../lib/S3Download";
import {ProductBatchWriterMysql} from "../Product/ProductBatchWriterMysql";
import {Pool} from "mysql";
import {BolFeedsTargetFile} from "../../enums/BolFeedsTargetFile";

export class BolXmlFetcherStream {
  static createBolXmlFetcher(logTag: string, sqlClient: Pool, s3Client: S3, logger: Logger, fileName: BolFeedsTargetFile): Runnable {
    return {
      run
    };

    function run(): Promise<void> {
      return new Promise((resolve, reject) => {
          const timer = new Timer(logTag, logger.info.bind(logger));
          const bolReadStream = createS3DownloadStream(s3Client, fileName, logger);

          timer.start();

          const productMapper = new BolMapper();

          const bolMapper = ProductStreamHelpers.createProductMapStream(
            (product, count) => productMapper.mapProduct(product, 'xml', count),
            logger,
            reject
          );

          const xmlStreamParser = XmlStreamParser.createXmlStreamParser({startElement: 'Product'});

          const productWriter = ProductBatchWriterMysql.createProductWriteStream(sqlClient, 'bol-feed', logger, timer);

          pump(
            bolReadStream,
            xmlStreamParser,
            bolMapper,
            productWriter,
            (err) => {
              if (err) {
                if (err.toString().indexOf('premature close') > -1) {
                  logger.info('Pipeline ended prematurely');
                  resolve();
                } else {
                  logger.error(`Pipeline ${logTag} failed.`);
                  reject(err);
                }
              } else {
                logger.info(`Pipeline ${logTag} succeeded.`);
                console.log('unmapped', productMapper.getUnmappedCategories())
                resolve();
              }
            }
          )
        }
      );
    }
  }
}
