import {VENDOR_DISPLAY_NAMES} from "../../constants/vendor-names";
import {uniqBy} from 'lodash';
import {VendorGroup} from "../../types/vendor-group";
import {PriceObject} from "../../types/PriceObject";
import {BoostableCategories} from "../../constants/BoostableCategories";

export class ElasticSearchMapperHelper {

  static mapCategories(categories: string) {
    return categories.split('#')
  }

  static mapBoostCategories(serializedBoostCategories: string) {
    const boostCategories = serializedBoostCategories && serializedBoostCategories.length > 0 ? serializedBoostCategories.split('|') : null;
    if(boostCategories) {
      return BoostableCategories.filter(cat => boostCategories.indexOf(cat) > -1).reduce((acc, key) => {
        acc[key] = boostCategories && boostCategories.indexOf(key) > -1 ? 1 : 0
        return acc;
      }, {})
    } else {
      return {};
    }
  }

  static mapVendors(vendors: string): VendorGroup[] {
    return uniqBy(JSON.parse(vendors).map(vendor => ({
      ...vendor,
      name: VENDOR_DISPLAY_NAMES[vendor.name]
    })), 'name');
  }

  static mapImages(images: string) {
    return images.split('#');
  }

  static mapToPriceObject(price: number): PriceObject {
    const floatPrice = parseFloat('' + price);
    return {
      actual: floatPrice.toFixed(2),
      euros:  floatPrice.toFixed(2).split('.')[0],
      cents:  floatPrice.toFixed(2).split('.')[1]
    }
  }
}
