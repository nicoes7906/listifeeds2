import * as stream from "stream";
import {AggregatedProduct} from "../AggregatedProduct/AggregatedProduct";
import {RunLimitHandler} from "../RunLimitHandler";
import {AggregatedProductRecord} from "../../interfaces/aggregated-product-record";
import {ElasticSearchMapperHelper} from "./ElasticSearchMapperHelper";
import {Category} from "../../enums/category";
import { Client } from '@elastic/elasticsearch'
import {ElasticSearchDocument} from "../../types/ElasticSearchDocument";

export class ElasticSearchUploader {

  static run(
    productStream: stream.Readable,
    suspendUploadTreshold: number,
    esClient: Client,
    esIndexName: string,
    logInfo: Function,
    logError: Function
  ) {
    let writeQueue: Promise<string> = Promise.resolve('Start');
    let productQueue: Array<AggregatedProduct> = [];
    const uploadCounter = ElasticSearchUploader.createCounter();
    const productCounter = ElasticSearchUploader.createCounter();
    const popularCounter = ElasticSearchUploader.createCounter();
    const runLimit = RunLimitHandler.getRunLimit();

    return new Promise((resolve, reject) => {
      productStream.on('readable', function () {
        let product;
        productCounter.increment();

        while (product = productStream.read()) {
          scheduleProductForUpload(product);
        }

        if(runLimit) {
          if(productCounter.getCount() === runLimit) {
            productStream.pause();
            onEnd(resolve, reject);
          }
        }
      });

      productStream.on('end', () => onEnd(resolve, reject))
    });

    /////

    function onEnd(resolve, reject) {
      if (productQueue.length > 0) {
        uploadAndVoidProductQueue();
      }

      writeQueue
        .then(() => {
          logInfo('Success uploading to Elastic Search');
          resolve('Ok');
        })
        .catch(() => {
          logInfo('Error uploading to Elastic Search');
          reject('Err');
        });
    }

    function scheduleProductForUpload(product: AggregatedProductRecord) {
      productQueue.push(product);
      if (productQueue.length === suspendUploadTreshold) {
        uploadAndVoidProductQueue();
      }
    }

    function uploadAndVoidProductQueue() {
      const productsToUpload = productQueue;
      productQueue = [];
      writeQueue = writeQueue
        .then(() => upsertProductBatch(productsToUpload))
        .then(() => logInfo(`Uploaded batch ${uploadCounter.getCount() * suspendUploadTreshold} `))
        .catch(e => logError(`An error occured: ${e.message}`));
    }

    function mapDbItemsForES(dbItems): ElasticSearchDocument[] {
      return dbItems.map(dbItem => {
        const categories = ElasticSearchMapperHelper.mapCategories(dbItem.categories);
        if(popularCounter.getCount() < 100) {
          categories.push(Category.POPULAR);
          popularCounter.increment();
        }
        const boostCategories = ElasticSearchMapperHelper.mapBoostCategories(dbItem.boostCategories)
        return {
          objectID: dbItem.ean,
          ean: dbItem.ean,
          title: dbItem.title,
          shortDescription: dbItem.description?.length > 100 ? dbItem.description.substring(0,97) + '...' : dbItem.description,
          longDescription: dbItem.description,
          ranking: dbItem.ranking,
          facetPrice: dbItem.price,
          categories,
          vendors: ElasticSearchMapperHelper.mapVendors(dbItem.vendors),
          media: ElasticSearchMapperHelper.mapImages(dbItem.images),
          price: ElasticSearchMapperHelper.mapToPriceObject(dbItem.price),
          fromAge: dbItem.fromAge,
          toAge: dbItem.toAge,
          modifiedDate: dbItem.modified_date,
          createdDate: dbItem.created_date,
          video: dbItem.video || null,
          boostCategories
        }
      })
    }

    function convertDocumentsToEsBulkEntries(docs: ElasticSearchDocument[]) {
      return docs.reduce((acc: any[], doc: ElasticSearchDocument) => {
        acc.push({ index: { _index: esIndexName, _id: doc.objectID } } );
        acc.push(doc);
        return acc;
      }, [])
    }

    async function upsertProductBatch(productBatch: Array<AggregatedProduct>): Promise<void> {
      const esDocuments = mapDbItemsForES(productBatch);
      const bulkBody = convertDocumentsToEsBulkEntries(esDocuments);
      try {
        const {body: bulkResponse} = await esClient.bulk({body: bulkBody});
        if (bulkResponse.errors) {
          const erroredDocuments = []
          // The items array has the same order of the dataset we just indexed.
          // The presence of the `error` key indicates that the operation
          // that we did for the document has failed.
          bulkResponse.items.forEach((action, i) => {
            const operation = Object.keys(action)[0]
            if (action[operation].error) {
              erroredDocuments.push({
                // If the status is 429 it means that you can retry the document,
                // otherwise it's very likely a mapping error, and you should
                // fix the document before to try it again.
                status: action[operation].status,
                error: action[operation].error
              })
            }
          })
          console.error(`Error indexing following documents: ${erroredDocuments}`);
          throw new Error('Error indexing some documents')
        }
      } catch(e) {
          throw new Error('There was an error indexing documents in Elastic Search')
      }
    }
  }

  static createCounter() {
    let count = 0;
    return {
      increment: () => {
        count = count + 1;
      },
      getCount: () => count
    }
  }
}
