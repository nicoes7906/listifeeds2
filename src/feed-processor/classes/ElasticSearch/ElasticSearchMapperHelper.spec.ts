import {expect} from "chai";
import {ElasticSearchMapperHelper} from "./ElasticSearchMapperHelper";

describe('ElasticSearchMapperHelper', () => {
  describe('mapBoostCategories', () => {
    it('should return the right boost categories map', () => {
      const result1 = ElasticSearchMapperHelper.mapBoostCategories(null);
      expect(result1).to.deep.equal({})
      const result2 = ElasticSearchMapperHelper.mapBoostCategories('');
      expect(result2).to.deep.equal({})
      const result3 = ElasticSearchMapperHelper.mapBoostCategories('NOCAT');
      expect(result3).to.deep.equal({})
    })
    it('should return the right boost categories map', () => {
      const result = ElasticSearchMapperHelper.mapBoostCategories('SPELLEN');
      expect(result).to.deep.equal({
        SPELLEN: 1
      })
    })
  })

})
