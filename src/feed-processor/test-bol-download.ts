import * as fs from "fs";
import {BolConstants} from "./constants/bol";
import * as http from "http";
import * as zlib from "zlib";

require('dotenv').load();

function downloadToys() {
  const file = fs.createWriteStream(BolConstants.PATH_COMPUTER_GAMES_XML);
  const unzipTransform = zlib.createGunzip();
  const {LISTIFEEDS_HOST, LISTIFEEDS_PORT, LISTIFEEDS_AUTH_USER, LISTIFEEDS_AUTH_PASS} = process.env;
  const request = http.get(`http://${LISTIFEEDS_HOST}:${LISTIFEEDS_PORT}/refreshcomputergames`, {
    'auth': `${LISTIFEEDS_AUTH_USER}:${LISTIFEEDS_AUTH_PASS}`
  }, function(response) {
    response
      .pipe(unzipTransform)
      .pipe(file);
  });
}

downloadToys();

