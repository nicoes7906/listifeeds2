export type ProductLinks = {
  desktopUrl: string,
  mobileUrl: string
}
