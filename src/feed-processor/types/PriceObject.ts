export type PriceObject = {
  actual: string,
  euros: string,
  cents: string
}
