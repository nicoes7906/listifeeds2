import {Vendor} from "../enums/vendor";

export type VendorGroup = {
  price: number,
  name: Vendor,
  mobileUrl: string,
  desktopUrl: string
}
