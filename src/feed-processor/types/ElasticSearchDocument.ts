import {VendorGroup} from "./vendor-group";
import {PriceObject} from "./PriceObject";

export type ElasticSearchDocument = {
    objectID: string,
    ean: string,
    title: string,
    shortDescription: string,
    longDescription: string,
    ranking: number
    facetPrice: number
    categories: string[]
    vendors: VendorGroup[]
    media: string[]
    price: PriceObject
    fromAge: number
    toAge: number
    modifiedDate: Date,
    createdDate: Date,
}
