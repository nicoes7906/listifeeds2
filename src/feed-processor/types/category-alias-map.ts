import {Category} from "../enums/category";

export type CategoryAliasGroup = {
  category: Category,
  aliases: Array<String>
}

export type CategoryAliasMap = Array<CategoryAliasGroup>;
