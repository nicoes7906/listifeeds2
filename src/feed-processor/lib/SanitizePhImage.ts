export function sanitizePhImage(imageUrl) {
  return imageUrl?.indexOf('prf.hn') > -1 ? imageUrl.split('/destination:')[1] : imageUrl;
}
