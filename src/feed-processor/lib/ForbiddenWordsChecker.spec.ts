import {checkForForbiddenWords} from "./ForbiddenWordsChecker";
import {expect} from 'chai';

describe('ForbiddenWordsChecker', () => {

  describe('Check forbidden', () => {
    [
      'Deze pieMel is',
      'Deze Sex is',
      'Deze kut heeft',
      'Deze sex,',
      'Deze piemel.',
      'sex<p>',
      'Met louter een seksspeeltje,',
      'grappig, sexy of intelligent',
      'sexy ontdekkingen'
    ].forEach((text) => {
      it('should throw an error', () => {
        expect(() => checkForForbiddenWords(text)).to.throw('Forbidden word')
      })
    })
  })

    describe('Check allowed', () => {
      [
        'Kutn',
        'Unisex',
        'Pornografisch'
      ].forEach((text) => {
        it('should throw an error', () => {
          expect(() => checkForForbiddenWords(text)).not.to.throw()
        })
    })
  })
})
