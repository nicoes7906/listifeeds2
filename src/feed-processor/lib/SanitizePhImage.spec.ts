import {sanitizePhImage} from "./SanitizePhImage";
import {expect} from 'chai';

describe('Sanitize PH image', () => {
  it('should remove the ph prefix', () => {
    const phImageUrl = 'https://prf.hn/click/camref:1101l3crA/creativeref:1101l50254/destination:https://images.wehkamp.nl/i/wehkamp/16568828_pb_01?w=500&h=500';
    const result = sanitizePhImage(phImageUrl);
    expect(result).to.equal('https://images.wehkamp.nl/i/wehkamp/16568828_pb_01?w=500&h=500');
  })
})
