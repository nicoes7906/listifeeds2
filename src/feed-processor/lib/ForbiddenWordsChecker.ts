import {forbiddenWords} from "../constants/forbiddenWords";

const forbiddenWordsRegex = forbiddenWords.map(word => new RegExp(`\\b${word}\\b`, 'gi'))

export function checkForForbiddenWords(text) {
  forbiddenWordsRegex.forEach(wordRegex => {
    if(text?.toLowerCase().match(wordRegex)){
      throw new Error('Forbidden word')
    }
  })
}
