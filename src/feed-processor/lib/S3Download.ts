import {bucketName} from "../constants/S3";
import * as zlib from "zlib";
import {S3} from "aws-sdk";
import {Logger} from "winston";

export function createS3DownloadStream(s3Client: S3, filePath, logger: Logger, isGzipped = true) {
  const file = `${filePath}${isGzipped ? '.gzip' : ''}`;

  logger.info(`Starting S3 download stream for ${filePath}`);

  const params = {
    Key: file,
    Bucket: bucketName
  };

  try {
    const chain = s3Client.getObject(params).createReadStream();
    if(isGzipped) {
      return chain.pipe(zlib.createGunzip());
    }
    return chain;
  } catch (e) {
    console.error(e);
    throw new Error(`Error occurred for downloading stream for file ${filePath}`)
  }
}
