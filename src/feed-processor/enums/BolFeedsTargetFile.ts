import {S3FileNames} from "../constants/S3";

export enum BolFeedsTargetFile {
  TOYS = S3FileNames.BOL_TOYS_FEED,
  COMPUTER_GAMES = S3FileNames.BOL_COMPUTER_GAMES_FEED
}
