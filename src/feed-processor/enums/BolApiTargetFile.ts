import {S3FileNames} from "../constants/S3";

export enum BolApiTargetFile {
  TOYS = S3FileNames.BOL_TOYS_API,
  GAMES = S3FileNames.BOL_GAMES_API
}
