import {S3FileNames} from "../constants/S3";

export enum PerformanceHorizonTargetFile {
  WEHKAMP_TOYS = S3FileNames.WEHKAMP,
  COOOLBLUE_COMPUTER_GAMES = S3FileNames.COOLBLUE
}
