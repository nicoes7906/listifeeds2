export enum Vendor {
  BOLFEED = "bolco",
  BOLOPENAPI = "boloa",
  INTERTOYS = "inter",
  BARTSMIT = "barts",
  MEDIAMARKT = "media",
  WEHKAMP = 'wehka',
  COOLBLUE = "coolb",
  GROTE_SPEELGOED_WINKEL = 'grosw',
  FF_SHOPPEN = 'ffsho',
  FONQ = 'fonqs',
  SPEELGOED_DIRECT = 'sdire',
  FUN_EN_FEEST = 'funen',
  COOLSHOP = 'cools',
  BELLATIO = 'bellatio',
  ICECAT = 'icecat',
  LISTI = 'listi'
}
