export enum VendorS3Keys {
  BOL_TOYS_API = 'bol-api-toys.json',
  BOL_GAMES_API = 'bol-api-games.json',
  BOL_FEED = 'bol-feed.xml',
  COOLBLUE = 'coolblue.xml',
  WEHKAMP = 'wehkamp.csv',
  ICECAT = 'icecat-pcf.csv'
}
