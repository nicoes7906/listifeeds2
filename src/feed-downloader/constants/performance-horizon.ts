import {CategoryAliasMap} from "../types/category-alias-map";
import {Category} from "../enums/category";

export namespace PerformanceHorizon {
  export const HOST = 'feeds.performancehorizon.com';
  // Coolblue full feed
  export const COOLB_FULL_FEED = '/listi/1101l93/8ddf3afab68def588d8405d1e8524c88';

  // Wehkamp entertainment
  export const WEHKA_ENTER_FEED = '/listi/1011l255/19ee34268269d9768442bd6b080bd1d2.csv';

  export const CATEGORY_ALIAS_MAP: CategoryAliasMap = [
    {category: Category.ELEKTRONICA, aliases: [
        'Elektronica',
        'Toetsenbord en muis sets',
        'Muizen', 'pc speakers',
        'Laptop sleeves',
        'Opladers voor laptops',
        'cradles & docks voor laptops'
      ]},
    {category: Category.GAMES_CONSOLES, aliases: ['Gaming muizen', 'Controllers', 'racepedalen', "Gaming headsets", 'Games', 'Game captures', 'Gaming stoelen']},
  ]
}
