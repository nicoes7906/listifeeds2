import * as path from 'path';

export namespace BolConstants {
  export const PATH_TOYS_XML = '/tmp/bol-toys-feed.xml';
  export const PATH_COMPUTER_GAMES_XML = '/tmp/bol-computer-games-feed.xml';
  export const PATH_DEV_TOYS_XML = path.join(__dirname, '../../feeds/bol-toys-feed.xml');
  export const PATH_DEV_COMPUTER_GAMES_XML = path.join(__dirname, '../../feeds/bol-computer-games-feed.xml');
  export const SITE_ID = 47511; //To be found at partnerprogramma.bol.com
  export const API_KEY = '18834F82BD67430BBB9B8FF4CCF369C3';
}
