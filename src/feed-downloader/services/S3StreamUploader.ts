const s3StreamFactory = require('s3-upload-stream');
const zlib = require('zlib');

const compress = zlib.createGzip();

export function S3StreamUploader(s3Client) {
  const s3Stream = s3StreamFactory(s3Client);

  return {
    writeRetailerFileAsGzipStream
  }

  async function writeFileAsStream(key, readStream) {
    return await new Promise((resolve, reject) => {
      const upload = s3Stream.upload({
        "Bucket": "listifeeds-bucket",
        "Key": key
      });

      // Handle errors.
      upload.on('error', function (error) {
        console.log(error);
        reject(error)
      });

      // Handle progress.
      upload.on('part', function (details) {
        console.log(details);
      });

      // Handle upload completion.
      upload.on('uploaded', function (details) {
        console.log(details);
        resolve("Success");
      });

      readStream.pipe(upload)
    })
  }

  async function writeRetailerFileAsGzipStream(keyBase, readStream) {
      await writeFileAsStream(`${keyBase}.gzip`, readStream.pipe(compress))
  }
}
