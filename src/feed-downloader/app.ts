import {BolOpenApiJsonStream} from "./classes/bol/BolOpenApiJsonStream";

const AWS = require('aws-sdk')
import {Logger} from "winston";
import * as request from "request";
import {LoggerFactory} from "./helpers/LoggerFactory";
import {S3StreamUploader} from "./services/S3StreamUploader";
import {PerformanceHorizon} from "./constants/performance-horizon";
import {VendorS3Keys} from "./constants/vendorS3Keys";

const s3Client = new AWS.S3();
const streamUploader = S3StreamUploader(s3Client);

async function runner(event) {
  try {
    const logger: Logger = LoggerFactory.createLogger('debug');

    console.log("Starting process for executor", event.Executor);
    switch (event.Executor) {
      case 'BOLTOYSAPI':
        const bolToysReadStream = BolOpenApiJsonStream.createReadableStream(7934, 10000, logger);
        await streamUploader.writeRetailerFileAsGzipStream(VendorS3Keys.BOL_TOYS_API, bolToysReadStream);
        return;
      case 'BOLGAMESAPI':
        const bolGamesReadStream = BolOpenApiJsonStream.createReadableStream(3135, 2000, logger);
        await streamUploader.writeRetailerFileAsGzipStream(VendorS3Keys.BOL_GAMES_API, bolGamesReadStream);
        return;
      case 'BOLFEEDS':
        await new Promise((resolve, reject) => {
          const executionId = Date.now();
          request('http://3.11.61.244/execute?id=' + executionId, function (err, response, body) {
            if(err) {
              console.error(err);
              reject('Unable to perform bol feeds task')
            }
            console.log('Execution started for executionId', executionId);
          })
          const statusInterval = setInterval(() => {
            request('http://3.11.61.244/status?id=' + executionId, function (err, response, body) {
              if(err) {
                console.error(err);
                throw new Error('Unable to perform bol feeds task')
              }
              if(response.statusCode === 200) {
                console.log('Script processed for executionId', executionId);
                resolve('Ok');
                clearInterval(statusInterval)
              } else if (response.statusCode === 500) {
                console.log('Script error for executionId', executionId);
                reject('An error occurred on the script')
                clearInterval(statusInterval)
              } else if (response.statusCode === 202) {
                console.log('Still pending for executionId', executionId);
              } else {
                reject('Unknown status code')
              }
            })
          }, 5000)
        });
        return
      case 'COOLBLUE':
        const cbRequestStream = request(`https://${PerformanceHorizon.HOST}${PerformanceHorizon.COOLB_FULL_FEED}`);
        await streamUploader.writeRetailerFileAsGzipStream(VendorS3Keys.COOLBLUE, cbRequestStream);
        return;
      case 'WEHKAMP':
        const wkRequestStream = request(`https://${PerformanceHorizon.HOST}${PerformanceHorizon.WEHKA_ENTER_FEED}`);
        await streamUploader.writeRetailerFileAsGzipStream(VendorS3Keys.WEHKAMP, wkRequestStream);
        return;
      case "ICECAT":
        const icecatRequestStreamUrl = 'https://icecat.biz/index.cgi?login_form=csv_content;csv_file=/products_csv_lists_new/104728_NL.csv;login=nicoeshuis;password=RjPRbGxnDC8'
        const icecatRequestStream = request({ method: 'GET', uri: icecatRequestStreamUrl, encoding: 'utf8'});

        await new Promise((resolve, reject) => {
          icecatRequestStream.pipe(process.stdout)
            .on('finish', () => resolve('Ok'))
            .on('error', () => reject('Error'))
        })
        return
      default:
        throw new Error(`Executor ${event.Executor} is not defined`)
    }
  } catch (e) {
    console.error(e);
    throw new Error('Couldn\'t do a stream upload');
  }
}

exports.lambdaHandler = runner;
