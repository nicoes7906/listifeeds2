import {Readable, ReadableOptions} from 'stream';
import * as request from 'request';
import {BolConstants} from "../../constants/bol";
import {Logger} from "winston";

const PRODUCTS_FETCH_LIMIT = 100;

export class BolOpenApiJsonStream {

  static createReadableStream(bolCategoryNumber, targetNumProducts, logger: Logger) {
    const NUMBER_OF_CALLS = Math.ceil(targetNumProducts / PRODUCTS_FETCH_LIMIT);
    let done = false;

    const readStream = new ReadableBolJson({
      read() {
        if(done) {
          this.push(null)
          return;
        }
        const result = new Array(NUMBER_OF_CALLS).fill(null).reduce((acc, _, i) => {
          return acc.then((stack) => fetchCallCount(stack, i))
        }, Promise.resolve({ products: [] }));

        result.then((stack) => {
          this.push(Buffer.from(JSON.stringify(stack)));
          done = true;
        }).catch(e => {
          console.error(e);
          throw new Error('Couldn\'t fetch Bol Open API products');
        })
      }
    });

    function fetchCallCount(stack, callCount) {
      return new Promise((resolve, reject) => {
        const url = BolOpenApiJsonStream.getBolUrl(bolCategoryNumber, callCount);
        request(url, function (err, response, body) {
          if (err) readStream.emit('error', err);
          try {
            logger.info(`Fetched from BOL API: ${url}`);
            const {products} = JSON.parse(body);
            stack.products = stack.products.concat(products);
            resolve(stack);
          } catch (e) {
            logger.error(e);
            reject('An error occured while fetch and parsing: ' + url);
          }
        });
      })
    }

    return readStream;
  }

  static getBolUrl(bolCategoryNumber, offsetIndex) {
    return `https://api.bol.com/catalog/v4/lists/?ids=${bolCategoryNumber}&offers=bolcom&format=json&apikey=${BolConstants.API_KEY}&offset=${offsetIndex * PRODUCTS_FETCH_LIMIT}&sort=rankasc&limit=${PRODUCTS_FETCH_LIMIT}&includeattributes=true`;
  }
}

class ReadableBolJson extends Readable {
  constructor(readableOptions: ReadableOptions){
    super(readableOptions);
    this.readable = false;
  }
  abort() {
    this.pause();
    this.emit('end');
  }
}
